<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

<h1>CSV TEST</h1>
<?php
if (($handle = fopen("users.csv", "r"))) {
    $length = 1000;
    $separator = ';';
    $enclosure = "\n";
    $escape = "'";
    $row = 0;
    while (($data = fgetcsv($handle, $length, $separator, $enclosure, $escape)) !== FALSE) {
        if ($row === 0) { //если это первая запись, значит это поля
            $fields = $data; //первая запись является полями
        } else {
            foreach ($data as $key => $item) { //проход по каждому элементу записи
                $array[$row][$fields[$key]] = $item; //ключ - название поля, значение - значение
            }
        }
        $row++;
    }
    fclose($handle);
}
?>

<table>
    <tr>
        <?php foreach ($fields as $field) { ?>
            <th><?= $field ?></th>
        <?php } ?>
    </tr>
    <?php foreach ($array as $item) { ?>
        <tr>
            <?php foreach ($item as $el) { ?>
                <td><?= $el ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
</table>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>