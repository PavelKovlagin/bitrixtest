<?php $currentDate = date('H') ?>

<div class="main-phone-block">
    <?php if ($currentDate >= 9 && $currentDate <= 18) { ?>
        <a href="tel:84952128506" class="phone">8 (495) 212-85-06</a>
    <?php } else { ?>
        <a href="mailto:store@store.ru" class="phone">store@store.ru</a>
    <?php } ?>
    <div class="shedule">время работы с 9-00 до 18-00</div>
</div>