<?php

use App\Exceptions\MyException;
use App\Patterns\Models\DogFacts;
use App\Patterns\Models\ElementFromArray;
use App\Patterns\Models\ElementsContainer;
use App\Patterns\Models\ReverseFactAdapter;
use App\Patterns\Models\SimpleElement;
use App\Patterns\Models\SingleUser;
use App\Patterns\Models\SqlExecutor;
use App\Patterns\Models\SqlViewer;
use App\Patterns\Models\StringFactAdapter;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("ИПР");
//SINGLETON
$user1 = SingleUser::getInstance();
$user2 = SingleUser::getInstance();
$user1->setName('Иван');
echo $user1->getName() . "<br>";
$user2->setName('Игорь'); //устанавливаем имя у второго пользователя
echo $user1->getName() . "<br>"; //но возвращаем имя первого

//FACTORY
$container = new ElementsContainer();
$id = 0;
$container->addElement(new ElementFromArray([
    'ID' => $id++,
    'NAME' => 'Элемент адин',
    'DESCRIPTION' => 'Описание элемента адин'
]));

$container->addElement(new SimpleElement($id++, 'Имя', 'Описание'));

$container->addElement(new ElementFromArray([
    'ID' => $id++,
    'NAME' => 'Элемент дыва',
    'DESCRIPTION' => 'Описание элемента дыва'
]));

$container->addElement(new SimpleElement($id++, 'Элемент', 'Элемент простой'));

$container->addElement(new ElementFromArray([
    'ID' => $id++,
    'NAME' => 'Элемент тры',
    'DESCRIPTION' => 'Описание элемента тры'
]));

$container->addElement(new SimpleElement($id++, 'Просто', 'Простой элемент'));

$container->addElement(new ElementFromArray([
    'ID' => $id++,
    'NAME' => 'Элемент чатыре',
    'DESCRIPTION' => 'Описание элемента адин'
]));

foreach ($container->getElements() as $item) { ?>
    <p><?= $item ?> </p>
<?php }

//ADAPTER
try {
    $fact = new DogFacts();
?>
    <pre>
    <? print_r($fact->get()) ?>
</pre>
    <?php
    $stringAdapter = new StringFactAdapter($fact);
    ?>
    <h1>Факт: <?= $stringAdapter->get() ?></h1>
    <?
    $reverseAdapter = new ReverseFactAdapter($fact);
    ?>
    <h1>Обратный факт: <?= $reverseAdapter->get() ?></h1>
<? } catch (MyException $e) {?>
    <h1><?= $e->getMessage() ?></h1>
<? }

//OBSERVER
$sqlExecutor = new SqlExecutor();
$sqlViewer = new SqlViewer();

$sqlExecutor->attach($sqlViewer, '*');

$sqlExecutor->select('first execution');
$sqlExecutor->delete('first execution');
$sqlExecutor->insert('first execution');
$sqlExecutor->update('first execution');

$sqlExecutor->detach($sqlViewer, '*');

$sqlExecutor->select('second execution');
$sqlExecutor->delete('second execution');
$sqlExecutor->insert('second execution');
$sqlExecutor->update('second execution');

$sqlExecutor->attach($sqlViewer, 'select');
$sqlExecutor->attach($sqlViewer, 'update');

$sqlExecutor->select('third execution');
$sqlExecutor->delete('third execution');
$sqlExecutor->insert('third execution');
$sqlExecutor->update('third execution');

//EXCEPTION
try {
} catch (MyException $e) {
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>