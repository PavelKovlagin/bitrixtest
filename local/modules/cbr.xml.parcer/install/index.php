<?php

use Bitrix\Main\Loader;

Loader::IncludeModule('highloadblock');

use Bitrix\Highloadblock as HL;

IncludeModuleLangFile(__FILE__);

class cbr_xml_parcer extends CModule
{
    var $MODULE_ID = "cbr.xml.parcer";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $errors;

    function __construct()
    {
        $this->MODULE_VERSION = "1.0.0";
        $this->MODULE_VERSION_DATE = "2021-11-28";
        $this->MODULE_NAME = "Парсер курсов валют";
        $this->MODULE_DESCRIPTION = "Парсер курсов валют";
    }

    function DoInstall()
    {
        global $APPLICATION;

        \Bitrix\Main\ModuleManager::RegisterModule($this->MODULE_ID);

        /*---Установка HL блока с курсами--- */
        $arLangs = array(
            'ru' => 'Курсы валют',
            'en' => 'Exchange rates'
        );

        //создание HL-блока
        $result = HL\HighloadBlockTable::add(
            [
                'NAME' => 'ExchangeRates',
                'TABLE_NAME' => 'exchange_rates',
            ]
        );

        if ($result->isSuccess()) {
            $id = $result->getId();
            foreach ($arLangs as $lang_key => $lang_val) {
                HL\HighloadBlockLangTable::add(
                    [
                        'ID' => $id,
                        'LID' => $lang_key,
                        'NAME' => $lang_val
                    ]
                );
            }
        } else {
            $errors = $result->getErrorMessages();
            var_dump($errors);
        }

        $arCartFields = [
            'UF_DATE' => [
                'ENTITY_ID' => 'HLBLOCK_' . $id,
                'FIELD_NAME' => 'UF_DATE',
                'USER_TYPE_ID' => 'datetime',
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => [
                    'ru' => 'Дата заполнения',
                    'en' => 'Date'
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => 'Дата заполнения',
                    'en' => 'Date'
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => 'Дата заполнения',
                    'en' => 'Date'
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
            ],
            'UF_NUM_CODE' => [
                'ENTITY_ID' => 'HLBLOCK_' . $id,
                'FIELD_NAME' => 'UF_NUM_CODE',
                'USER_TYPE_ID' => 'integer',
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => [
                    'ru' => 'Числовой код',
                    'en' => 'Num code'
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => 'Числовой код',
                    'en' => 'Num code'
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => 'Числовой код',
                    'en' => 'Num code'
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
            ],
            'UF_CHAR_CODE' => [
                'ENTITY_ID' => 'HLBLOCK_' . $id,
                'FIELD_NAME' => 'UF_CHAR_CODE',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => [
                    'ru' => 'Символьный код',
                    'en' => 'Char code'
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => 'Символьный код',
                    'en' => 'Char code'
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => 'Символьный код',
                    'en' => 'Char code'
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
            ],
            'UF_NOMINAL' => [
                'ENTITY_ID' => 'HLBLOCK_' . $id,
                'FIELD_NAME' => 'UF_NOMINAL',
                'USER_TYPE_ID' => 'integer',
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => [
                    'ru' => 'Номинал',
                    'en' => 'Nominal'
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => 'Номинал',
                    'en' => 'Nominal'
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => 'Номинал',
                    'en' => 'Nominal'
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
            ],
            'UF_NAME' => [
                'ENTITY_ID' => 'HLBLOCK_' . $id,
                'FIELD_NAME' => 'UF_NAME',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => [
                    'ru' => 'Название',
                    'en' => 'Name'
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => 'Название',
                    'en' => 'Name'
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => 'Название',
                    'en' => 'Name'
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
            ],
            'UF_VALUE' => [
                'ENTITY_ID' => 'HLBLOCK_' . $id,
                'FIELD_NAME' => 'UF_VALUE',
                'USER_TYPE_ID' => 'double',
                'SETTINGS' => [
                    'PRECISION' => 2
                ],
                'MANDATORY' => 'Y',
                "EDIT_FORM_LABEL" => [
                    'ru' => 'Значение',
                    'en' => 'Value'
                ],
                "LIST_COLUMN_LABEL" => [
                    'ru' => 'Значение',
                    'en' => 'Value'
                ],
                "LIST_FILTER_LABEL" => [
                    'ru' => 'Значение',
                    'en' => 'Value'
                ],
                "ERROR_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
                "HELP_MESSAGE" => [
                    'ru' => '',
                    'en' => ''
                ],
            ]
        ];

        $arSavedFieldsRes = [];
        foreach ($arCartFields as $arCartField) {
            $obUserField  = new CUserTypeEntity;
            $ID = $obUserField->Add($arCartField);
            $arSavedFieldsRes[] = $ID;
        }

        CAgent::AddAgent(
            "Parcer\Agent\UpdateCbr::update();",  // имя функции
            "cbr.xml.parcer",                // идентификатор модуля
            "N",                      // агент не критичен к кол-ву запусков
            3600,                    // интервал запуска - 1 сутки
            "",                       // дата первой проверки - текущее
            "Y",                      // агент активен
            "",                       // дата первого запуска - текущее
            30
        );

        $APPLICATION->IncludeAdminFile("Установка модуля ", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/step.php");
        return true;
    }

    function DoUninstall()
    {
        global $APPLICATION;

        \Bitrix\Main\ModuleManager::UnRegisterModule($this->MODULE_ID);

        /*---Удаление HL блока с курсами валют---*/
        $hlblock = HL\HighloadBlockTable::getList([
            'filter' => ['=NAME' => 'ExchangeRates']
        ])->fetch();
        if ($hlblock) {
            HL\HighloadBlockTable::delete($hlblock['ID']);
        }

        CAgent::RemoveAgent("Parcer\Agent\UpdateCbr::update();", "cbr.xml.parcer");

        $APPLICATION->IncludeAdminFile("Деинсталляция модуля " . $this->MODULE_ID . "", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/unstep.php");
        return true;
    }
}
