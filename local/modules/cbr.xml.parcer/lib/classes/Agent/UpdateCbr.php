<?php

namespace Parcer\Agent;

use Parcer\Cbr\Cbr;

class UpdateCbr
{
    /**
     * Выполняет запись валют в HL блок
     * Выполняется агентом и возвращает название фунцкии, для того установить выполнение агента через какое-то время
     * 
     * @return [type]
     */
    function update()
    {
        $cbb = new Cbr();
        $cbb->insert();
        return 'Parcer\Agent\UpdateCbr::update();';
    }
}
