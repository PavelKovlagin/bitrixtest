<?php

namespace Parcer\Cbr;

use Parcer\Hlblock\Entity;
use Parcer\Parcer\Xml;
use SimpleXMLElement;

class Cbr
{
    private ?SimpleXMLElement $xml = null;
    private $entity = null;

    public function __construct()
    {
        $this->xml = Xml::get('https://www.cbr-xml-daily.ru/daily.xml');
        $this->entity = new Entity();
    }

    /**
     * Добавляет записи в HL блок
     * 
     * @return [type]
     */
    public function insert()
    {
        foreach ($this->xml as $key => $item) {
            $res = $this->entity->insert([
                'UF_DATE' => date('d.m.Y H:i:s'),
                'UF_NUM_CODE' => $item->NumCode,
                'UF_CHAR_CODE' => $item->CharCode,
                'UF_NOMINAL' => $item->Nominal,
                'UF_NAME' => $item->Name,
                'UF_VALUE' => str_replace(',', '.', $item->Value)
            ]);
        }
    }

    /**
     * Возвращает сформированный массив
     * 
     * @param mixed $res
     * 
     * @return array
     */
    public function get($res): array
    {
        $array = [];
        foreach ($res as $item) {
            $array[$item['UF_CHAR_CODE']][$item['UF_DATE']->format('Y.m.d H:i:s')] = [
                'UF_DATE' => $item['UF_DATE'],
                'UF_NUM_CODE' => $item['UF_NUM_CODE'],
                'UF_CHAR_CODE' => $item['UF_CHAR_CODE'],
                'UF_NOMINAL' => $item['UF_NOMINAL'],
                'UF_NAME' =>$item['UF_NAME'],
                'UF_VALUE' => $item['UF_VALUE']
            ];
        }
        return $array;
    }

    /**
     * Возвращает все записи
     * 
     * @return [type]
     */
    public function getAll()
    {
        $res = $this->entity->select();
        return $this->get($res);
    }

    /**
     * Возвращает записи по символьному коду курса
     * 
     * @param string $code
     * 
     * @return [type]
     */
    public function getByCharCode(string $code)
    {
        $res = $this->entity->select([
            'filter' => [
                'UF_CHAR_CODE' => $code
            ]            
        ]);
        return $this->get($res);
    }
}
