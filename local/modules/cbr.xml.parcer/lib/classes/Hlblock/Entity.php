<?php

namespace Parcer\Hlblock;

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL; 
Loader::includeModule("highloadblock"); 

class Entity
{
    private $entity_data_class = null;

    public function __construct()
    {
        $hlblock = HL\HighloadBlockTable::getList([
            'filter' => ['=NAME' => 'ExchangeRates']
        ])->fetch();
        if($hlblock){
            HL\HighloadBlockTable::getById($hlblock['ID'])->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock); 
            $this->entity_data_class = $entity->getDataClass(); 
        }
    }

    /**
     * Получение записей из HL блока
     * 
     * @param array $params
     * 
     * @return [type]
     */
    public function select(array $params = [])
    {
        return $this->entity_data_class::getList($params);
    }

    /**
     * Добавление записи в HL блок
     * 
     * @param array $params
     * 
     * @return [type]
     */
    public function insert(array $params)
    {
        return $this->entity_data_class::add($params);
    }
}