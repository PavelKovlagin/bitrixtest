<?php

namespace Parcer\Parcer;

use SimpleXMLElement;

class Xml
{
    public static function get(string $url): ?SimpleXMLElement
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $html = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($html, "SimpleXMLElement", LIBXML_NOCDATA);

        return $xml;
    }
}
