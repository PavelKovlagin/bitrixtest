<?php

use Bitrix\Main\Loader;

if (!$USER->IsAdmin())
    return;

preg_match("/modules\/([a-zA-Z0-9_\.]+)\//", __FILE__, $find_module_id);
$moduleId = $find_module_id[1];


Loader::IncludeModule('cbr.xml.parcer');
$parcer = new Parcer\Cbr\Cbr();
$allRates = $parcer->getAll();
if (isset($_REQUEST['char_code']) && $_REQUEST['char_code'] !== 'all') {
    $rates = $parcer->getByCharCode($_REQUEST['char_code']);
} else {
    $rates = $parcer->getAll();
}

?>
<style type="text/css">
    /* внешние границы таблицы серого цвета толщиной 1px */
    table .parcer {
        border: 1px solid grey;
    }

    /* границы ячеек первого ряда таблицы */
    th .parcer {
        border: 1px solid grey;
    }

    /* границы ячеек тела таблицы */
    td .parcer {
        border: 1px solid grey;
    }
</style>
<form>
    <input type='hidden' name='mid_menu' value='<?= $_REQUEST['mid_menu'] ?>'>
    <input type='hidden' name='mid' value='<?= $_REQUEST['mid'] ?>'>
    <input type='hidden' name='lang' value='<?= $_REQUEST['lang'] ?>'>
    <select name='char_code'>
        <option value='all'>Все</option>
        <?php foreach ($allRates as $key => $item) { ?>
        <option <?= $_REQUEST['char_code'] === $key ? 'selected' : '' ?> value='<?= $key ?>'><?= current($item)['UF_NAME'] ?></option>
        <?php } ?>
    </select>
    <input type='submit' value='ОК'>
</form>

<?php foreach ($rates as $key => $rate) { ?>
    <h1><?= current($rate)['UF_NAME'] ?> - <?= $key ?></h1>
    <table class='parcer'>
        <tr class='parcer'>
            <th class='parcer'>Дата и время</th>
            <th class='parcer'>Числовой код курса</th>
            <th class='parcer'>Символьный код курса</th>
            <th class='parcer'>Номинал</th>
            <th class='parcer'>Название</th>
            <th class='parcer'>Значение</th>
        </tr>
        <!--ряд с ячейками заголовков-->
        <?php foreach ($rate as $item) { ?>
            <tr class='parcer'>
                <td class='parcer'><?= $item['UF_DATE']->format('Y-m-d H:i:s') ?></td>
                <td class='parcer'><?= $item['UF_NUM_CODE'] ?></td>
                <td class='parcer'><?= $item['UF_CHAR_CODE'] ?></td>
                <td class='parcer'><?= $item['UF_NOMINAL'] ?></td>
                <td class='parcer'><?= $item['UF_NAME'] ?></td>
                <td class='parcer'><?= $item['UF_VALUE'] ?></td>
            </tr>
            <!--ряд с ячейками тела таблицы-->
        <?php } ?>
    </table>
<?php } ?>