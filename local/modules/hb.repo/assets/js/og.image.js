BX.addCustomEvent('OnEditorInitedBefore', function(editor) {       
    var _this=this;
    var _class = 'hb_html_edit';

   /*Описание кнопки добавляемой в визуальный редактор*/    
    var btn = this.AddButton({    
        iconClassName: 'hb-og-add-img',
        /*Путь до иконки кнопки*/        
        src: '/local/modules/hb.site/assets/images/og-image-file-add-20.png',        
        id: 'hb-add-img',
        /*Названия кнопки из языкового файла*/    
        name: "Добавить изображение для og:image",
        /*Событие по нажатию на кнопку*/       
        handler: function(e){
            /*Описание модального окна */    
            var DialogAddImg = new BX.CDialog({    
                /*Заголовок модального окна из языкового файла*/    
                title: "Добавить изображение для og:image",    
                /*Путь до файла контента отображаемого в модальном окне*/    
                content_url: '/local/modules/hb.site/lib/og/Image.php',    
                /*Запрос к файлу с контентом модального окна*/    
                content_post: 'ajax=yes&action=openWindow&class='+_class,    
                min_width: 400,    
                min_height: 400,    
                /*Описание кнопок модального окна*/    
                buttons: [    
                   /*Описание кнопки загрузку изображений в визуальный редактор*/    
                    {       
                        /*Заголовок кнопки из языкового файла*/                            
                        title: "Сохранить",    
                        name: 'loadImg',    
                        id: 'loadImg',    
                        /*Действие по нажатию на кнопку*/        
                        action: function () {    
                            var arData = $("#" + _class + "_form").serialize();
                            var _thisBtn = this;
                            
                            BX.ajax({
                                url: '/local/modules/hb.site/lib/og/Image.php',
                                method: 'POST',
                                dataType: 'json',
                                data: 'ajax=yes&action=saveImg&class='+ _class + '&' + arData,
                                onsuccess: function(data){  
                                    var content = _this.GetContent();

                                    if (data.error) {
                                        content = content.replace(/<\?\s\$APPLICATION->SetPageProperty\(\'OG_PICTURE\',.*?\);/, "");
                                        content = content.replace(/\$APPLICATION->SetPageProperty\(\'OG_PICTURE_WIDTH\',.*?\);/, "");
                                        content = content.replace(/\$APPLICATION->SetPageProperty\(\'OG_PICTURE_HEIGHT\',.*?\);\s\?>/, "");
                                    } else {
                                        content += "<? $APPLICATION->SetPageProperty('OG_PICTURE', '" + data.SRC + "');";
                                        content += "$APPLICATION->SetPageProperty('OG_PICTURE_WIDTH', '" + data.HEIGHT + "');";
                                        content += "$APPLICATION->SetPageProperty('OG_PICTURE_HEIGHT', '" + data.WIDTH + "'); ?>";
                                    }

                                    _this.SetContent(content, true);
                                    _this.ReInitIframe();
                                    _thisBtn.parentWindow.Close();               
                                },
                            });
                        }
                    },    
    
                    /*Вывод кнопки отмены в модальном окне*/    
                    BX.CDialog.prototype.btnCancel,    
                ]    
            });    
               
            /*Вызов модального окна описанного выше*/     
            DialogAddImg.Show();          
        }    
    });    

});