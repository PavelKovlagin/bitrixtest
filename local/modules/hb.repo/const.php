<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

define('HB_REPO_LOG_DIR', __DIR__ . '/logs');
define('HB_REPO_CONFIG_DIR', __DIR__ . '/lib/config');