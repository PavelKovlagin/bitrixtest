<?
use \Bitrix\Main\Loader;
use Repo\DI\Container;

include 'const.php';

if (file_exists(__DIR__ . '/lib/vendor/autoload.php')) {
    require_once(__DIR__ . '/lib/vendor/autoload.php');
}

Container::build();

Loader::includeModule('iblock');