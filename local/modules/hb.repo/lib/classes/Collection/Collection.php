<?php

declare(strict_types=1);

namespace Repo\Collection;

use Repo\Helper\Formatting\CamelCaseFormatting;

class Collection implements CollectionInterface
{
    protected array $container = [];
    private ?int $currentPage = null; //текущая страница
    private ?int $totalRecords = null; //всего записей
    private ?int $pageLimit = null; //количество записей на странице

    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->container[$offset] : null;
    }

    public function offsetSet($offset, $value): void
    {
        if ($offset === null) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    public function count(): int
    {
        return count($this->container);
    }

    public function jsonSerialize(): array
    {
        return CamelCaseFormatting::getFormattedArray($this->toArray());
    }

    public function first()
    {
        return $this->container[array_key_first($this->container)];
    }

    public function last()
    {
        return $this->container[array_key_last($this->container)];
    }

    public function current()
    {
        return current($this->container);
    }

    public function next(): void
    {
        next($this->container);
    }

    public function key()
    {
        return key($this->container);
    }

    public function valid(): bool
    {
        return $this->key() !== null;
    }

    public function rewind()
    {
        return reset($this->container);
    }

    public function toArray(): array
    {
        return $this->container;
    }

    public function push($value): self
    {
        $this->offsetSet(null, $value);

        return $this;
    }

    public function get($key)
    {
        return $this->offsetGet($key);
    }

    /**
     * Получение элемента коллекции по идентификатору этого элемента в БД
     * 
     * @param mixed $id
     * 
     * @return [type]
     */
    public function getById($id)
    {
        foreach ($this->container as $item) {
            if ((int)$item->getId() === (int)$id) {
                return $item;
            }
        }
        return null;
    }

    /**
     * Получение элемента по внешнему коду
     * 
     * @param string $xml
     * 
     * @return [type]
     */
    public function getByXmlId(string $xml)
    {
        foreach ($this->container as $item) {
            if ($item->getXmlId() === $xml) {
                return $item;
            }
        }
    }

    /**
     * Возвращает случаный элемент из коллекции
     * 
     * @return [type]
     */
    public function rand()
    {
        $key = array_rand($this->container);
        return $this->offsetGet($key);
    }

    /**
     * Возвращает список из случайных элементов
     * 
     * @param int $count - количество случайных элементов
     * 
     * @return [type]
     */
    public function randList(int $count = 2)
    {
        if ($count < 2 || count($this->container) <= $count) {
            return $this->container;
        }
        $rand = array_rand($this->container, $count);
        foreach ($rand as $r) {
            $array[] = $this->container[$r];
        }
        return $array;
    }

    /**
     * Удаляет элемент из коллекци по идентификатору
     * 
     * @param int $id
     * 
     * @return [type]
     */
    public function unsetById(int $id)
    {
        foreach ($this->container as $key => $item) {
            if ($item->getId() === $id) {
                unset($this->container[$key]);
                return;
            }
        }
    }

    /**
     * удаляет элемент из коллекции по символьному коду
     * 
     * @param string $code
     * 
     * @return [type]
     */
    public function unsetByCode(string $code)
    {
        foreach ($this->container as $key => $item) {
            if ($item->getCode() === $code) {
                unset($this->container[$key]);
                return;
            }
        }
    }

    /**
     * Возвращает идентификаторы элементов коллекции
     * 
     * @return [type]
     */
    public function getIds()
    {
        foreach ($this->container as $item) {
            $array[] = $item->getId();
        }
        return $array;
    }

    /**
     * Устанавливает текущую страницу
     * 
     * @param int $currentPage
     * 
     * @return self
     */
    public function setCurrentPage(int $currentPage): self
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    /**
     * Возвращает текущую страницу
     * 
     * @return int|null
     */
    public function getCurrentPage(): ?int
    {
        return $this->currentPage;
    }

    /**
     * Устанавливает общее количество записей
     * 
     * @param int $totalRecords
     * 
     * @return self
     */
    public function setTotalRecords(int $totalRecords): self
    {
        $this->totalRecords = $totalRecords;
        return $this;
    }

    /**
     * Возращает общее количество записей
     * 
     * @return int|null
     */
    public function getTotalRecords(): ?int
    {
        return $this->totalRecords;
    }

    /**
     * Устанавливает число записей на странице
     * 
     * @param int $pageLimit
     * 
     * @return self
     */
    public function setPageLimit(int $pageLimit): self
    {
        $this->pageLimit = $pageLimit;
        return $this;
    }

    /**
     * Возвращает число записей на странице
     * 
     * @return int|null
     */
    public function getPageLimit(): ?int
    {
        return $this->pageLimit;
    }

    /**
     * Возвращает значение для кнопки Показать ещё
     * 
     * @return int|null
     */
    public function getPageMore(): ?int
    {
        $page = $this->getCurrentPage();
        $totalPages = $this->getTotalPages();
        $limit = $this->getPageLimit();
        $totalRecords = $this->getTotalRecords();
        if ($totalRecords === 0) {
            return 0;
        } elseif ((int)$page === (int)$totalPages) { //если последняя страница
            return 0;
        } elseif ((int)$page === ((int)$totalPages - 1)) { //если предпоследняя страница
            return $totalRecords - ($page * $limit);
        } else {
            return $limit;
        }
        //return $this->pageMore;
    }

    /**
     * Врзвращает количество страниц
     * 
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return (int)ceil($this->getTotalRecords() / $this->getPageLimit());
    }
}
