<?php

declare(strict_types=1);

namespace Repo\Collection;

interface CollectionInterface extends \ArrayAccess, \Countable, \JsonSerializable, \Iterator
{
    public function offsetExists($offset): bool;

    public function offsetGet($offset);

    public function offsetSet($offset, $value): void;

    public function offsetUnset($offset): void;

    public function count(): int;

    public function jsonSerialize(): array;

    public function current();

    public function next(): void;

    public function key();

    public function valid(): bool;

    public function rewind();

    public function toArray(): array;

    public function push($value): self;
    
    public function get($key);
}
