<?php

declare(strict_types=1);

namespace Repo\DI;

use DI\Container as DIContainer;
use DI\ContainerBuilder;
use DI\DependencyException;
use DI\NotFoundException;

class Container
{
    /**
     * @var DIContainer Контейнер зависимостей.
     */
    private static DIContainer $container;

    /**
     * Метод формирует контейнер зависимостей.
     *
     * @throws \Exception
     */
    public static function build(): void
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->useAnnotations(false);
        $containerBuilder->addDefinitions(HB_REPO_CONFIG_DIR . '/definitions.php');

        self::$container = $containerBuilder->build();
    }

    /**
     * Метод возвращает контейнер зависимостей.
     *
     * @return DIContainer
     */
    public static function getContainer(): DIContainer
    {
        return self::$container;
    }

    /**
     * Метод возвращает сервис из контейнера зависимостей.
     *
     * @param $name
     * @return mixed
     * @throws DependencyException
     * @throws NotFoundException
     */
    public static function get($name)
    {
        return self::$container->get($name);
    }

    /**
     * Метод проверяет наличие сервиса в контейнере зависимостей.
     *
     * @param $name
     * @return bool
     */
    public static function has($name): bool
    {
        return self::$container->has($name);
    }

    /**
     * Метод передает аргументы в конструктор сервиса и возвращает созданный объект из контейнера зависимостей.
     *
     * @param $name
     * @param array $parameters
     * @return mixed
     * @throws DependencyException
     * @throws NotFoundException
     */
    public static function make($name, array $parameters)
    {
        return self::$container->make($name, $parameters);
    }
}
