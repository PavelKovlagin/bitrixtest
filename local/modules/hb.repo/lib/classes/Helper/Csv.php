<?php 

namespace Repo\Helper;

class Csv
{
    /**
     * Возвращает массив из CSV файла
     * 
     * @param string $file
     * 
     * @return [type]
     */
    public static function getArrayFromCsv(string $file)
    {
        if (($handle = fopen($_SERVER['DOCUMENT_ROOT'] . $file, "r"))) {
            $length = 1000;
            $separator = ';';
            $enclosure = "\n";
            $escape = "'";
            $row = 0;
            while (($data = fgetcsv($handle, $length, $separator, $enclosure, $escape)) !== FALSE) {
                if ($row === 0) { //если это первая запись, значит это поля
                    $fields = $data; //первая запись является полями
                } else {
                    foreach ($data as $key => $item) { //проход по каждому элементу записи
                        $array[$row][$fields[$key]] = $item; //ключ - название поля, значение - значение
                    }
                }
                $row++;
            }
            fclose($handle);
            return $array;
        }
        return null;
    }
}