<?php

declare(strict_types=1);

namespace Repo\Helper\Formatting;

/**
 * Класс для преобразования данных в camelCase.
 *
 * @package App\Helper\Formatting
 */
class CamelCaseFormatting
{
    /**
     * Метод проходит по ключам многомерного массива с преобразовывает их из стиля UNDERSCORE_CASE в camelCase.
     *
     * @param array $array
     * @return array
     */
    public static function getFormattedArray(array $array): array
    {
        $keys = array_map(static function ($item) use (&$array) {
            if (is_array($array[$item])) {
                $array[$item] = self::getFormattedArray($array[$item]);
            }

            if (!is_string($item)) {
                return $item;
            }

            $item = strtolower($item);
            $parts = explode('_', $item);

            return array_shift($parts) . implode('', array_map('ucfirst', $parts));
        }, array_keys($array));

        return array_combine($keys, $array);
    }
}
