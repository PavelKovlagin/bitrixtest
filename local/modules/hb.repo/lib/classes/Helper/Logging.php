<?php

namespace Repo\Helper;

use \Bitrix\Main\Diag\Debug;

class Logging
{
    public static function log(array $params, string $name, string $file)
    {
        $path = str_replace($_SERVER['DOCUMENT_ROOT'], '', HB_REPO_LOG_DIR);
        Debug::writeToFile(
            $params,
            date('Y.m.d H:i:s') . ' | ' .  $file,
            $path . '/log_'. $name . '_' . date('Y.m.d') . '.txt'
        ); 
    }
}