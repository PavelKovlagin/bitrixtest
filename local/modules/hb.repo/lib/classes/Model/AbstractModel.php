<?php

declare(strict_types=1);

namespace Repo\Model;

use Repo\Helper\Formatting\CamelCaseFormatting;

abstract class AbstractModel implements \JsonSerializable
{
    /**
     * @param array $data
     * @return mixed
     */
    abstract public static function fromArray(array $data);

    /**
     * @return array
     */
    abstract public function toArray(): array;

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return CamelCaseFormatting::getFormattedArray($this->toArray());
    }
}
