<?php

declare(strict_types=1);

namespace Repo\Model\File\Entity\File;

use Bitrix\Main;

class FileTable extends Main\FileTable
{
    public static function getMap(): array
    {
        $map = parent::getMap();
        $path = 'http://' . $_SERVER['SERVER_NAME'] . '/upload';

        $map['SRC']=  new Main\Entity\ExpressionField(
            'SRC',
            'IF (%3$s > 0, CONCAT_WS("/", "' . $path . '", %1$s, %2$s), NULL)',
            ['SUBDIR', 'FILE_NAME', 'ID']
        );

        return $map;
    }
}
