<?php

declare(strict_types=1);

namespace Repo\Model\File\Entity\File;

use Repo\DI\Container;
use Repo\Helper\Option;
use Repo\Model\Iblock\Entity\Value\FileValue;
use Bitrix\Main\Context;

class Image extends FileValue
{
    public const SIZE_FULL = [1280, 1024];
    public const SIZE_STOCK_PREVIEW = [475, 300];
    public const SIZE_CATEGORY_PREVIEW = [475, 325];
    public const SIZE_INTERIOR_PREVIEW = [475, 325];
    public const SIZE_INSTAGRAM_INTERIOR = [225, 225];
    public const SIZE_REVIEW_PICTURES_PREVIEW = [100, 100];
    public const SIZE_PRODUCT_SMALL = [80, 32];
    public const SIZE_PRODUCT_CARD = [280, 190];

    private ?int $id = null;

    public static function fromArray(array $data): self
    {
        /** @var Image $image */
        $image = parent::fromArray($data);

        if ($id = $data['ID']) {
            $image->setId((int)$id);
        }

        return $image;
    }

    /**
     * Метод возвращает измененное изображение.
     * Ресайз выполняется средствами bitrix.
     *
     * @param array $sizes [int width, int height]
     * @param int $resizeType
     * @param bool $initSizes
     * @return array|null|bool
     */
    public function getResizedImage(
        array $sizes,
        int $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
        bool $initSizes = false,
        bool $waterMarkExec = true
    )
    {
        $preparedSizes = [];

        if ($waterMarkExec) {
            $arFilter = [
                [
                    "name" => "watermark",
                    "type" => "image",
                    "position" => "center",
                    "size" => "real",
                    "alpha_level" => "98",
                    "file" => $_SERVER["DOCUMENT_ROOT"] ."/uploads/". WATERMARK
                ]            
            ];
        }
        

        if ($width = (int)$sizes[0]) {
            $preparedSizes['width'] = $width;

            if ($height = (int)$sizes[1]) {
                $preparedSizes['height'] = $height;
            }
        }

        $image = \CFile::ResizeImageGet(
            $this->getId() ?? $this->toArray(),
            $preparedSizes,
            $resizeType,
            $initSizes,
            $arFilter
        );

        if ($server = Context::getCurrent()->getServer()->get('SERVER_NAME')) {
            $image['src'] = 'http://' . $server . $image['src'];
        } elseif ($server = Container::get(Option::class)->get('siteUrl')) {
            $image['src'] = 'http://' . $server . $image['src'];
        } else {
            $image['src'] = $image['src'];
        }
        
        return $image;
    }

    /**
     * Метод возвращает идентификатор файла.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Метод устанавливает идентификатор файла.
     *
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
}
