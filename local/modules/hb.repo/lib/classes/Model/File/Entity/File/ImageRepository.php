<?php

declare(strict_types=1);

namespace Repo\Model\File\Entity\File;

use Bitrix\Main\ArgumentException;

class ImageRepository
{
    /**
     * @param array $parameters
     * @return array
     * @throws ArgumentException
     */
    public function get(array $parameters): array
    {
        $files = FileTable::getList($parameters);
        $filesArray = [];

        while ($file = $files->fetch()) {
            $filesArray[$file['ID']] = Image::fromArray($file);
        }

        return $filesArray;
    }

    /**
     * @param array $ids
     * @return array
     * @throws ArgumentException
     */
    public function getByIds(array $ids): array
    {
        if (!$ids) {
            return [];
        }

        $result = [];
        $filesArray = $this->get([
            'filter' => [
                '=ID' => $ids,
            ],
            'select' => [
                '*',
                'SRC',
            ],
        ]);

        foreach ($ids as $elementId => $fileId) {
            $result[$elementId] = $filesArray[$fileId];
        }

        return $result;
    }
}
