<?php

declare(strict_types=1);

namespace Repo\Model\File\Entity\File;

class NullImage extends Image
{
    public function getSrc(): ?string
    {
        return null;
    }
}
