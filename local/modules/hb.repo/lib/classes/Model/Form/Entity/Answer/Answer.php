<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Answer;

use Repo\Model\AbstractModel;

class Answer extends AbstractModel
{
    private ?int $resultId = null;
    private ?int $fieldId = null;
    private ?string $sid = null;
    private ?string $title = null;
    private ?int $answerId = null;
    private ?string $userText = null;
    private ?int $userFileId = null;
    private ?string $fieldType = null;

    public static function fromArray(array $data): self
    {
        $model = new static();

        if ($resultId = $data['RESULT_ID']) {
            $model->setResultId((int)$resultId);
        }

        if ($fieldId = $data['FIELD_ID']) {
            $model->setFieldId((int)$fieldId);
        }

        if ($sid = $data['SID']) {
            $model->setSid($sid);
        }

        if ($title = $data['TITLE']) {
            $model->setTitle($title);
        }

        if ($answerId = $data['ANSWER_ID']) {
            $model->setAnswerId((int)$answerId);
        }

        if ($userText = $data['USER_TEXT']) {
            $model->setUserText($userText);
        }

        if ($userFileId = $data['USER_FILE_ID']) {
            $model->setUserFileId((int)$userFileId);
        }

        if ($fieldType = $data['FIELD_TYPE']) {
            $model->setFieldType($fieldType);
        }

        return $model;
    }

    public function toArray(): array
    {
        return [
            'RESULT_ID' => $this->getResultId(),
            'FIELD_ID' => $this->getFieldId(),
            'SID' => $this->getSid(),
            'TITLE' => $this->getTitle(),
            'ANSWER_ID' => $this->getAnswerId(),
            'USER_TEXT' => $this->getUserText(),
            'USER_FILE_ID' => $this->getUserFileId(),
            'FIELD_TYPE' => $this->getFieldType(),
        ];
    }

    /**
     * Метод возвращает идентификатор результата.
     *
     * @return int|null
     */
    public function getResultId(): ?int
    {
        return $this->resultId;
    }

    /**
     * Метод устанавливает идентификатор результата.
     *
     * @param int|null $resultId
     * @return self
     */
    public function setResultId(?int $resultId): self
    {
        $this->resultId = $resultId;

        return $this;
    }

    /**
     * Метод возвращает идентификатор ответа.
     *
     * @return int|null
     */
    public function getFieldId(): ?int
    {
        return $this->fieldId;
    }

    /**
     * Метод устанавливает идентификатор ответа.
     *
     * @param int|null $fieldId
     * @return self
     */
    public function setFieldId(?int $fieldId): self
    {
        $this->fieldId = $fieldId;

        return $this;
    }

    /**
     * Метод возвращает символьный код поля.
     *
     * @return string|null
     */
    public function getSid(): ?string
    {
        return $this->sid;
    }

    /**
     * Метод устанавливает символьный код поля.
     *
     * @param string|null $sid
     * @return self
     */
    public function setSid(?string $sid): self
    {
        $this->sid = $sid;

        return $this;
    }

    /**
     * Метод возвращает заголовок поля.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Метод устанавливает заголовок поля.
     *
     * @param string|null $title
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Метод возвращает идентификатор ответа.
     *
     * @return int|null
     */
    public function getAnswerId(): ?int
    {
        return $this->answerId;
    }

    /**
     * Метод устанавливает идентификатор ответа.
     *
     * @param int|null $answerId
     * @return self
     */
    public function setAnswerId(?int $answerId): self
    {
        $this->answerId = $answerId;

        return $this;
    }

    /**
     * Метод возвращает текст, заполненный пользователем.
     *
     * @return string|null
     */
    public function getUserText(): ?string
    {
        return $this->userText;
    }

    /**
     * Метод устанавливает текст, заполненный пользователем.
     *
     * @param string|null $userText
     * @return self
     */
    public function setUserText(?string $userText): self
    {
        $this->userText = $userText;

        return $this;
    }

    /**
     * Метод возвращает идентификатор файла, загруженный пользователем.
     *
     * @return int|null
     */
    public function getUserFileId(): ?int
    {
        return $this->userFileId;
    }

    /**
     * Метод устанавливает идентификатор файла, загруженный пользователем.
     *
     * @param int|null $userFileId
     */
    public function setUserFileId(?int $userFileId): void
    {
        $this->userFileId = $userFileId;
    }

    /**
     * Метод возвращает тип поля.
     *
     * @return string|null
     */
    public function getFieldType(): ?string
    {
        return $this->fieldType;
    }

    /**
     * Метод устанавливает тип поля.
     *
     * @param string|null $fieldType
     * @return self
     */
    public function setFieldType(?string $fieldType): self
    {
        $this->fieldType = $fieldType;

        return $this;
    }
}
