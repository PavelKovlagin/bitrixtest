<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Answer;

use Bitrix\Main\ArgumentException;

class AnswerRepository
{
    public function __construct()
    {
        \CModule::IncludeModule("form");
    }

    /**
     * Метод возвращает поля ответов на вопрос веб-формы.
     *
     * @param array $parameters
     * @return array
     * @throws ArgumentException
     */
    public function get(array $parameters): array
    {
        if (empty($parameters['field_id'])) {
            throw new ArgumentException('Идентификатор поля не указан.');
        }

        if (empty($parameters['by'])) {
            $parameters['by'] = 's_sort';
        }

        if (!empty($parameters['order'])) {
            $parameters['order'] = 'asc';
        }

        if (!empty($parameters['is_filtered'])) {
            $parameters['is_filtered'] = false;
        }

        $answers = \CFormAnswer::GetList(
            (int)$parameters['id'],
            $parameters['by'],
            $parameters['order'],
            (array)$parameters['filter'],
            $parameters['is_filtered']
        );
        $answersArray = [];

        while ($answer = $answers->Fetch()) {
            $answersArray[] = $answer;
        }

        return $answersArray;
    }

    /**
     * Метод возвращает поля ответов на вопрос веб-формы по идентификатору вопроса.
     *
     * @param int $id
     * @return array
     * @throws ArgumentException
     */
    public function getByFieldId(int $id): array
    {
        return $this->get(['field_id' => $id]);
    }
}
