<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Answer;

class NullAnswer extends Answer
{
    public function isNull(): bool
    {
        return true;
    }
}
