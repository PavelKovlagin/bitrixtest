<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Field;

use Repo\Collection\Collection;
use Repo\Collection\CollectionInterface;
use Repo\Model\AbstractModel;
use Repo\Model\Form\Entity\Answer\Answer;

class Field extends AbstractModel
{
    private ?CollectionInterface $answers = null;

    public static function fromArray(array $data): self
    {
        $model = new static();
        $model->setAnswers($data);

        return $model;
    }

    public function toArray(): array
    {
        return [];
    }

    /**
     * Метод возвращает ответы на вопрос.
     *
     * @return Collection|CollectionInterface
     */
    public function getAnswers(): CollectionInterface
    {
        return $this->answers ?? new Collection();
    }

    /**
     * Метод возвращает ответ на вопрос.
     *
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answers->first();
    }

    /**
     * Метод устанавливает ответы на вопрос.
     *
     * @param array|null $answers
     * @return self
     */
    public function setAnswers(?array $answers): self
    {
        $answersCollection = new Collection();

        foreach ($answers as $answer) {
            $answersCollection->push(Answer::fromArray($answer));
        }

        $this->answers = $answersCollection;

        return $this;
    }
}
