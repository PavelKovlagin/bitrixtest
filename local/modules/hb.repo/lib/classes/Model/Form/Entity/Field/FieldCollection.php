<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Field;

use Repo\Collection\Collection;

class FieldCollection extends Collection
{
    /**
     * @param $key
     * @return Field|NullField
     */
    public function get($key)
    {
        return parent::get($key) ?? new NullField();
    }
}
