<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Field;

use Bitrix\Main\ArgumentException;

class FieldRepository
{
    /**
     * Метод возвращает поля вопросов веб-формы.
     *
     * @param array $parameters
     * @return array
     * @throws ArgumentException
     */
    public function get(array $parameters): array
    {
        \CModule::IncludeModule("form");
        if (empty($parameters['form_id'])) {
            throw new ArgumentException('Идентификатор формы не указан.');
        }

        if (empty($parameters['by'])) {
            $parameters['by'] = 's_sort';
        }

        if (!empty($parameters['order'])) {
            $parameters['order'] = 'asc';
        }

        if (!empty($parameters['is_filtered'])) {
            $parameters['is_filtered'] = false;
        }

        $fields = \CForm::GetFieldList(
            (int)$parameters['form_id'],
            'N',
            $parameters['by'],
            $parameters['order'],
            (array)$parameters['filter'],
            $parameters['is_filtered']
        );
        $fieldsArray = [];

        while ($field = $fields->Fetch()) {
            $fieldsArray[] = $field;
        }

        return $fieldsArray;
    }

    /**
     * Метод возвращает поля вопросов веб-формы по идентификатору формы.
     *
     * @param int $id
     * @return array
     * @throws ArgumentException
     */
    public function getByFormId(int $id): array
    {
        return $this->get(['form_id' => $id]);
    }
}
