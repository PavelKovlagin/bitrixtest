<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Field;

use Repo\Model\Form\Entity\Answer\NullAnswer;

class NullField extends Field
{
    public function isNull(): bool
    {
        return true;
    }

    public function getAnswer(): NullAnswer
    {
        return new NullAnswer();
    }
}
