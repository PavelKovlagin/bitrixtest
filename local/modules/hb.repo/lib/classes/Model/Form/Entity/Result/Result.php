<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Result;

use Repo\Model\AbstractModel;
use Repo\Model\Form\Entity\Field\Field;
use Repo\Model\Form\Entity\Field\FieldCollection;

class Result extends AbstractModel
{
    private ?int $id = null;
    private ?FieldCollection $fields = null;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        $model = new static();

        if ($id = $data['ID']) {
            $model->setId((int)$id);
        }

        if ($fields = $data['FIELDS']) {
            $model->setFields($fields);
        }

        return $model;
    }

    public function toArray(): array
    {
        return [];
    }

    /**
     * Метод возвращает идентификатор.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Метод устанавливает идентификатор.
     *
     * @param int|null $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Метод возвращает поля формы.
     *
     * @return FieldCollection|Field[]
     */
    public function getFields(): FieldCollection
    {
        return $this->fields;
    }

    /**
     * Метод устанавливает поля формы.
     *
     * @param FieldCollection|Field[]|null $fields
     * @return self
     */
    public function setFields(?FieldCollection $fields): self
    {
        $this->fields = $fields;

        return $this;
    }
}
