<?php

declare(strict_types=1);

namespace Repo\Model\Form\Entity\Result;

use Repo\Model\Form\Entity\Answer\AnswerRepository;
use Repo\Model\Form\Entity\Field\Field;
use Repo\Model\Form\Entity\Field\FieldCollection;
use Repo\Model\Form\Entity\Field\FieldRepository;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

class ResultRepository
{
    private AnswerRepository $answerRepository;
    private FieldRepository $fieldRepository;

    /**
     * @param AnswerRepository $answerRepository
     * @param FieldRepository $fieldRepository
     * @throws LoaderException
     */
    public function __construct()
    {
        Loader::includeModule('form');

        $this->answerRepository = new AnswerRepository();
        $this->fieldRepository = new FieldRepository();
    }

    /**
     * Метод возвращает результаты веб-формы.
     *
     * @param array $parameters
     * @return array
     * @throws ArgumentException
     */
    public function get(array $parameters): array
    {
        $parameters = $this->prepareParameters($parameters);
        $formId = $parameters['form_id'];

        $fieldsArray = $this->fieldRepository->getByFormId($formId);
        $resultsArray = [];

        foreach ($this->getResults($parameters) as $result) {
            $answersArray = [];

            foreach ($fieldsArray as $field) {
                // Получение ответа на вопрос.
                $answersArray[] = array_merge(...$this->answerRepository->getByFieldId((int)$field['ID']));
            }

            // Объединение вопросов и ответов результата.
            $dataArray = \CFormResult::GetDataByID(
                $result['ID'],
                [],
                $result,
                $answersArray
            );
            $dataCollection = new FieldCollection();

            foreach ($dataArray as $code => $dataArrayItem) {
                $dataCollection[$code] = Field::fromArray($dataArrayItem);
            }

            $resultsArray[] = [
                'ID' => $result['ID'],
                'FIELDS' => $dataCollection,
            ];
        }

        return $resultsArray;
    }

    /**
     * Метод возвращает поля результатов веб-формы.
     *
     * @param array $parameters
     * @return array
     */
    private function getResults(array $parameters): array
    {
        $results = \CFormResult::GetList(
            $parameters['form_id'],
            $by,
            $order,
            (array)$parameters['filter'],
            $isFiltered,
            'N',
            $parameters['limit'] ?? false
        );
        $resultsArray = [];

        while ($result = $results->Fetch()) {
            $resultsArray[] = $result;
        }

        return $resultsArray;
    }

    /**
     * Метод выполняет проверку параметров.
     *
     * @param array $parameters
     * @return array
     * @throws ArgumentException
     */
    private function prepareParameters(array $parameters): array
    {
        if (empty($parameters['form_id'])) {
            throw new ArgumentException('Form id is not specified.');
        }

        if ((int)$parameters['form_id'] < 1) {
            throw new ArgumentException('Form id is must not be zero or less.');
        }

        return $parameters;
    }
}
