<?php

declare(strict_types=1);

namespace Repo\Model\HL\Entity\Element;

class Element
{
    private ?int $id = null;
    private ?string $name = null;
    private ?string $xmlId = null;

    public static function fromArray(array $data): self
    {
        $element = new static();

        if ($id = $data['ID']) {
            $element->setId((int)$id);
        }
        if ($name = $data['UF_NAME']) {
            $element->setName((string)$name);
        }
        if ($xmlId = $data['UF_XML_ID']) {
            $element->setXmlId((string)$xmlId);
        }

        return $element;
    }

    public function toArray(): array
    {
        $element = [];
        
        $element['ID'] = $this->getId();
        $element['NAME'] = $this->getName();
        $element['XML_ID'] = $this->getXmlId();

        return $element;
    }

    /**
     * Метод возвращает идентификатор.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Метод устанавливает идентификатор.
     *
     * @param int|null $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Устанавливает название
     * 
     * @param string $name
     * 
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Возвращает название
     * 
     * @param string $name
     * 
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /** 
     * Устанавливает внешний код
     * 
     * @param string $xmlId
     * 
     * @return self
     */
    public function setXmlId(string $xmlId): self
    {
        $this->xmlId = $xmlId;
        return $this;
    }

    /**
     * Возвращает внешний код
     * 
     * @return string|null
     */
    public function getXmlId(): ?string
    {
        return $this->xmlId;
    }
}
