<?php

declare(strict_types=1);

namespace Repo\Model\HL\Entity\Element;

use Repo\Collection\{Collection, CollectionInterface};
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\{Loader, LoaderException, SystemException};
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Entity\{AddResult, DataManager, DeleteResult, UpdateResult};

class ElementRepository
{
    /**
     * @var string|DataManager
     */
    private string $entity;

    /**
     * @throws LoaderException
     */
    public function __construct()
    {
        Loader::includeModule('highloadblock');
    }

    /**
     * @param $entity
     * @return $this
     */
    public function setEntity($entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Метод создает объект для работы с сущностью.
     *
     * @param string $name
     * @return mixed
     * @throws ArgumentException | SystemException
     */
    public static function createEntity(string $name)
    {
        $highloadBlock = HighloadBlockTable::getList([
            'filter' => [
                '=NAME' => $name,
            ],
        ])->fetch();

        /** @var string $dataClass */
        $dataClass = HighloadBlockTable::compileEntity($highloadBlock)->getDataClass();

        return (new static())->setEntity($dataClass);
    }

    /**
     * Метод добавляет запись в HL-блок.
     *
     * @param array $parameters
     * @return AddResult
     * @throws \Exception
     */
    public function add(array $parameters): AddResult
    {
        return $this->entity::add($parameters);
    }

    /**
     * Обновление записи
     * 
     * @param int $id
     * @param array $parameters
     * 
     * @return UpdateResult
     */
    public function update(int $id, array $parameters): UpdateResult
    {
        return $this->entity::update($id, $parameters);
    }

    /**
     * Метод удаляет запись из HL-блока.
     *
     * @param int $id
     * @return DeleteResult
     * @throws \Exception
     */
    public function delete(int $id): DeleteResult
    {
        return $this->entity::delete($id);
    }

    /**
     * Метод возвращает коллекцию записей HL-блока.
     *
     * @param array $parameters
     * @return Collection|CollectionInterface
     * @throws ArgumentException
     */
    public function get(array $parameters): CollectionInterface
    {
        $elementsCollection = new Collection();
        $elementsArray = $this->getArray($parameters);

        foreach ($elementsArray as $element) {
            $elementsCollection->push(Element::fromArray($element));
        }

        return $elementsCollection;
    }

    /**
     * Метод возвращает массив записей HL-блока.
     *
     * @param array $parameters
     * @return array
     * @throws ArgumentException
     */
    public function getArray(array $parameters): array
    {
        return $this->entity::getList($parameters)->fetchAll();
    }

    /**
     * Метод возвращает количество записей HL-блока.
     *
     * @param array $parameters
     * @return int
     * @throws ArgumentException
     */
    public function getCount(array $parameters): int
    {
        return $this->entity::getList($parameters)->getSelectedRowsCount();
    }
}
