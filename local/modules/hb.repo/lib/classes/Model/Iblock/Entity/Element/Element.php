<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Element;

use Repo\Model\AbstractModel;
use Repo\Model\File\Entity\File\Image;
use Repo\Model\File\Entity\File\NullImage;
use Repo\Model\Iblock\Entity\Property\NullProperty;
use Repo\Model\Iblock\Entity\Property\Property;
use Repo\Model\Iblock\Entity\Property\PropertyCollection;
use Repo\Model\Iblock\Entity\Seo\Seo;
use Bitrix\Main\ArgumentNullException;

class Element extends AbstractModel
{
    private ?int $id = null;
    private ?int $iblockId = null;
    private ?string $xmlId = null;
    private ?string $code = null;
    private ?string $name = null;
    private ?string $active = null;
    private ?string $dateCreate = null;
    private ?string $dateActiveFrom = null;
    private ?string $dateActiveTo = null;
    private ?int $sort = null;
    private ?string $url = null;
    private ?int $sectionId = null;
    private ?array $sectionsIds = null;
    private ?Image $previewPicture = null;
    private ?Image $detailPicture = null;
    private ?string $previewText = null;
    private ?string $previewTextType = null;
    private ?string $detailText = null;
    private ?string $detailTextType = null;
    private ?string $search = null; //строка поиска
    private ?int $type = null;
    private ?PropertyCollection $properties = null;
    private ?Seo $seo = null;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        $element = new static();

        if (!empty($data['ID'])) {
            $element->setId((int)$data['ID']);
        }

        if (!empty($data['IBLOCK_ID'])) {
            $element->setIblockId((int)$data['IBLOCK_ID']);
        }

        if (!empty($data['XML_ID'])) {
            $element->setXmlId($data['XML_ID']);
        }

        if (!empty($data['CODE'])) {
            $element->setCode($data['CODE']);
        }

        if (!empty($data['NAME'])) {
            $element->setName($data['NAME']);
        }

        if (!empty($data['ACTIVE'])) {
            $element->setActive($data['ACTIVE']);
        }

        if (!empty($data['SORT'])) {
            $element->setSort((int)$data['SORT']);
        }

        if (!empty($data['DETAIL_PAGE_URL'])) {
            $element->setUrl(
                \CIBlock::ReplaceDetailUrl($data['DETAIL_PAGE_URL'], $data, false, 'E')
            );
        }

        if (!empty($data['IBLOCK_SECTION_ID'])) {
            $element->setSectionId((int)$data['IBLOCK_SECTION_ID']);
        }

        if (!empty($data['SECTIONS'])) {
            $element->setSectionsIds($data['SECTIONS']);
        }

        if (!empty($data['PREVIEW_PICTURE']) && $data['PREVIEW_PICTURE'] instanceof Image) {
            $element->setPreviewPicture($data['PREVIEW_PICTURE']);
        }

        if (!empty($data['DETAIL_PICTURE']) && $data['DETAIL_PICTURE'] instanceof Image) {
            $element->setDetailPicture($data['DETAIL_PICTURE']);
        }

        if (!empty($data['PREVIEW_TEXT'])) {
            $element->setPreviewText($data['PREVIEW_TEXT']);
        }

        if (!empty($data['PREVIEW_TEXT_TYPE'])) {
            $element->setPreviewTextType($data['PREVIEW_TEXT_TYPE']);
        }

        if (!empty($data['DETAIL_TEXT'])) {
            $element->setDetailText($data['DETAIL_TEXT']);
        }

        if (!empty($data['DETAIL_TEXT_TYPE'])) {
            $element->setDetailTextType($data['DETAIL_TEXT_TYPE']);
        }

        if (!empty($data['TYPE'])) {
            $element->setType((int)$data['TYPE']);
        }

        if (!empty($data['DATE_CREATE'])) {
            $element->setDateCreate((string)$data['DATE_CREATE']);
        }

        if (!empty($data['DATE_ACTIVE_FROM'])) {
            $element->setDateActiveFrom((string)$data['DATE_ACTIVE_FROM']);
        }

        if (!empty($data['DATE_ACTIVE_TO'])) {
            $element->setDateActiveTo((string)$data['DATE_ACTIVE_TO']);
        }

        $element->setProperties($data['PROPERTIES'] ?? []);

        $element->setSeo($data['SEO'] ?? []);

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        $element = [];

        if ($id = $this->getId()) {
            $element['ID'] = $id;
        }

        if ($iblockId = $this->getIblockId()) {
            $element['IBLOCK_ID'] = $iblockId;
        }

        if ($xmlId = $this->getXmlId()) {
            $element['XML_ID'] = $xmlId;
        }

        if ($code = $this->getCode()) {
            $element['CODE'] = $code;
        }

        if ($name = $this->getName()) {
            $element['NAME'] = $name;
        }

        if ($active = $this->getActive()) {
            $element['ACTIVE'] = $active;
        }

        if ($sort = $this->getSort()) {
            $element['SORT'] = $sort;
        }

        if ($url = $this->getUrl()) {
            $element['DETAIL_PAGE_URL'] = $url;
        }

        if ($sectionId = $this->getSectionId()) {
            $element['IBLOCK_SECTION_ID'] = $sectionId;
        }

        if ($sections = $this->getSectionsIds()) {
            $element['SECTIONS'] = $sections;
        }

        if ($previewPicture = $this->getPreviewPicture()) {
            $element['PREVIEW_PICTURE'] = $previewPicture;
        }

        if ($detailPicture = $this->getDetailPicture()) {
            $element['DETAIL_PICTURE'] = $detailPicture;
        }

        if ($previewText = $this->getPreviewText()) {
            $element['PREVIEW_TEXT'] = $previewText;
        }

        if ($previewTextType = $this->getPreviewTextType()) {
            $element['PREVIEW_TEXT_TYPE'] = $previewTextType;
        }

        if ($detailText = $this->getDetailText()) {
            $element['DETAIL_TEXT'] = $detailText;
        }

        if ($detailTextType = $this->getDetailTextType()) {
            $element['DETAIL_TEXT_TYPE'] = $detailTextType;
        }

        if ($properties = $this->getProperties()) {
            $element['PROPERTIES'] = $properties;
        }

        if ($type = $this->getType()) {
            $element['TYPE'] = $type;
        }

        return $element;
    }

    /**
     * Возвращает дату создания элемента
     * 
     * @return string|null
     */
    public function getDateCreate(): ?string
    {
        return $this->dateCreate;
    }

    /**
     * Возвращает дату создания в формате
     * 
     * @return string|null
     */
    public function getDateCreateFormat(): ?string
    {
        return ($date = $this->getDateCreate()) ? FormatDate('j F Y', MakeTimeStamp($date)) : null;
    }

    /**
     * Устанавливает дату создания элемента
     * 
     * @param string $date
     * 
     * @return self
     */
    public function setDateCreate(string $date): self
    {
        $this->dateCreate = $date;
        return $this;
    }

    /**
     * Устанавливает начальную дату активности
     * 
     * @param string $date
     * 
     * @return self
     */
    public function setDateActiveFrom(string $date): self
    {
        $this->dateActiveFrom = $date;
        return $this;
    }

    /**
     * Возвращает конечную дату активности
     * 
     * @return string|null
     */
    public function getDateActiveFrom(): ?string
    {
        if ($date = $this->dateActiveFrom) {
            return  FormatDate('j F Y', MakeTimeStamp($date));
        }
        return null;        
    }

    /**
     * Устанавливает конечную дату активности
     * 
     * @param string $date
     * 
     * @return self
     */
    public function setDateActiveTo(string $date): self
    {
        $this->dateActiveTo = $date;
        return $this;
    }

    /**
     * Возвращает конечную дату активности
     * 
     * @return string|null
     */
    public function getDateActiveTo(): ?string
    {
        if ($date = $this->dateActiveTo) {
            return  FormatDate('j F Y', MakeTimeStamp($date));
        }
        return null; 
    }

    /**
     * Возвращает тип
     * 
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * Устанавилвает тип
     * 
     * @param int $type
     * 
     * @return self
     */
    public function setType(int $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIblockId(): ?int
    {
        return $this->iblockId;
    }

    /**
     * @param int $iblockId
     * @return $this
     */
    public function setIblockId(int $iblockId): self
    {
        $this->iblockId = $iblockId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getXmlId(): ?string
    {
        return $this->xmlId;
    }

    /**
     * @param string|null $xmlId
     * @return $this
     */
    public function setXmlId(?string $xmlId): self
    {
        $this->xmlId = $xmlId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return $this
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getActive(): ?string
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active === 'Y';
    }

    /**
     * @param string $active
     * @return self
     */
    public function setActive(string $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return $this
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return $this
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSectionId(): ?int
    {
        return $this->sectionId;
    }

    /**
     * @param int|null $sectionId
     * @return $this
     */
    public function setSectionId(?int $sectionId): self
    {
        $this->sectionId = (int)$sectionId;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getSectionsIds(): ?array
    {
        return $this->sectionsIds;
    }

    /**
     * @param array|null $sectionsIds
     * @return $this
     */
    public function setSectionsIds(?array $sectionsIds): self
    {
        $this->sectionsIds = $sectionsIds;

        return $this;
    }

    /**
     * @return Image|NullImage
     */
    public function getPreviewPicture()
    {
        return $this->previewPicture ?? new NullImage();
    }

    /**
     * Возваращает превью элемента
     * 
     * @return array|null
     */
    public function getPreviewPictureResize(array $size = []): ?array
    {
        if (($picture = $this->getPreviewPicture()) && ($pictureId = $picture->getId())) {
            return $size ? $picture->getResizedImage($size) : $picture->getResizedImage(Image::SIZE_STOCK_PREVIEW);
        }
        return null;
    }

    /**
     * @param Image $previewPicture
     * @return $this
     */
    public function setPreviewPicture(Image $previewPicture): self
    {
        $this->previewPicture = $previewPicture;

        return $this;
    }

    /**
     * @return Image|NullImage
     */
    public function getDetailPicture()
    {
        return $this->detailPicture ?? new NullImage();
    }

    /**
     * Возвращает детальное изображение элемента
     * 
     * @return array|null
     */
    public function getDetailPictureResize(): ?array
    {
        if (($picture = $this->getDetailPicture()) && ($pictureId = $picture->getId())) {
            return $picture->getResizedImage(Image::SIZE_STOCK_PREVIEW);
        }
        return null;
    }

    /**
     * @param Image $detailPicture
     * @return $this
     */
    public function setDetailPicture(Image $detailPicture): self
    {
        $this->detailPicture = $detailPicture;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPreviewText(): ?string
    {
        return $this->previewText;
    }

    /**
     * @param string|null $previewText
     * @return $this
     */
    public function setPreviewText(?string $previewText): self
    {
        $this->previewText = $previewText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPreviewTextType(): ?string
    {
        return $this->previewTextType;
    }

    /**
     * @param string|null $previewTextType
     * @return $this
     */
    public function setPreviewTextType(?string $previewTextType): self
    {
        $this->previewTextType = $previewTextType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetailText(): ?string
    {
        return $this->detailText;
    }

    /**
     * Устанавливает фразу для поиска
     * 
     * @param string $search
     * 
     * @return self
     */
    public function setSearch(string $search): self
    {
        $this->search = $search;
        return $this;
    }

    /**
     * Возвращает фразу для поиска
     * 
     * @return string|null
     */
    public function getSearch(): ?string
    {
        return $this->search;
    }

    /**
     * Возвращает детальный текст для поиска
     * 
     * @param string $search
     * 
     * @return string|null
     */
    public function getSearchDetailText(): ?string
    {
        if (!$text = $this->getDetailText()) {
            return null;
        }
        $text = strip_tags($text);
        if ($text && ($search = $this->getSearch())) {
            $pos = mb_strpos($text, $search);
            if ($pos) {
                return htmlspecialchars('...' . mb_substr($text, $pos - 200, 400) . '...');
            }          
        }
        return htmlspecialchars(substr($text, 1, 400) . '...');
    }

    /**
     * @param string|null $detailText
     * @return $this
     */
    public function setDetailText(?string $detailText): self
    {
        $this->detailText = $detailText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetailTextType(): ?string
    {
        return $this->detailTextType;
    }

    /**
     * @param string|null $detailTextType
     * @return $this
     */
    public function setDetailTextType(?string $detailTextType): self
    {
        $this->detailTextType = $detailTextType;

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    public function getProperties(): PropertyCollection
    {
        return $this->properties;
    }

    /**
     * Возвращает seo элемента
     * 
     * @return Seo|null
     */
    public function getSeo(): ?Seo
    {
        return $this->seo;
    }

    /**
     * @param array $properties
     * @return $this
     * @throws ArgumentNullException
     */
    public function setProperties(array $properties): self
    {
        $collection = new PropertyCollection();

        foreach ($properties as $property) {
            if (empty($property['CODE'])) {
                throw new ArgumentNullException($property['CODE']);
            }

            $collection[$property['CODE']] = Property::fromArray($property);
        }

        $this->properties = $collection;

        return $this;
    }

    /**
     * Возвращает свойство по коду
     * 
     * @param string $code
     * 
     * @return [type]
     */
    public function getPropertyByCode(string $code)
    {
        $prop = $this->properties->findByCode($code);
        if (!($prop instanceof NullProperty) && $prop && $prop->getValues()->first()) {
            return $prop;
        }
        return null;
    }

    /**
     * Устанавливает SEO для элемента инфоблока
     * 
     * @param array $seo
     * @return $this
     */
    public function setSeo(array $seo): self
    {
        $this->seo = Seo::fromArray($seo);

        return $this;
    }
}