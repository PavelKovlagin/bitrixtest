<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Element;

use Repo\Collection\Collection;
use Repo\Collection\CollectionInterface;
use Repo\Model\File\Entity\File\ImageRepository;
use Repo\Model\Iblock\Entity\Property\PropertyRepository;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\{Application, ArgumentException, Loader};
use Bitrix\Main\Data\Cache;
use Bitrix\Iblock\InheritedProperty\ElementValues;
use Repo\Model\Iblock\Helper\IblockHelper;

class ElementRepository
{
    private PropertyRepository $propertyRepository;
    private ImageRepository $imageRepository;
    protected IblockHelper $iblockHelper;
    private ?int $iblockId;

    public function __construct()
    {
        $this->iblockHelper = new IblockHelper();
        $this->propertyRepository = new PropertyRepository();
        $this->imageRepository = new ImageRepository();
    }

    /**
     * <p><b>Метод возвращает коллекцию элементов инфоблока.</b></p>
     *
     * <p><b>Является оберткой над \CIBlockElement::GetList, включает в себя следующие основные улучшения:</b></p>
     * <ul style="margin-top: 0;">
     * <li>исправлены проблемы с выборкой множественных свойств;</li>
     * <li>элементы, свойства и значения свойства сохраняются в соответствующие коллекции;</li>
     * <li>в целом выборка более оптимизированная.</li>
     * </ul>
     *
     * <p><b>Допустимые ключи:</b></p>
     * <ul style="margin-top: 0;">
     * <li>array select - поля в части SELECT запроса</li>
     * <li>array select_properties - выборка свойств</li>
     * <li>array filter - фильтры в части WHERE запроса</li>
     * <li>array group - поля в части GROUP BY запроса</li>
     * <li>array order - поля в части ORDER BY запроса</li>
     * <li>int limit - максимальное число строк в выборке</li>
     * <li>int page - номер страницы</li>
     * </ul>
     *
     * @param array $parameters
     * @return Collection
     * @throws ArgumentException
     */
    public function get(array $parameters): CollectionInterface
    {
        $elementsArray = $this->getArray($parameters);

        $collection = new Collection();

        foreach ($elementsArray as $element) {
            $collection[$element['ID']] = Element::fromArray($element);
        }

        return $collection;
    }

    /**
     * Метод возвращает массив элементов инфоблока.
     *
     * @param array $parameters
     * @param bool $caching - нужно ли кэшировать
     * @return array
     * @throws ArgumentException
     * @see ElementRepository::get()
     */
    public function getArray(array $parameters, bool $caching = true): array
    {
        $parameters = $this->prepareParameters($parameters);

        global $USER;
        if ($USER->IsAdmin() && $_GET['clear_cache'] === 'Y') {
            $this->clearCache($parameters);
        }

        if ($caching) {
            $elementsArray = $this->managedCache($parameters);
        } else {
            $elementsArray = $this->getList($parameters);
        }

        return $elementsArray;
    }

    /**
     * Метод возвращает результат запроса
     * 
     * @param array $parameters
     * 
     * @return array
     */
    public function getList(array $parameters): array
    {
        $parameters['select'][] = 'ID';
        $elements = \CIBlockElement::GetList(
            (array)$parameters['order'],
            (array)$parameters['filter'],
            $parameters['group'],
            $parameters['navStart'],
            (array)$parameters['select']
        );
        $elementsArray = [];

        while ($element = $elements->Fetch()) {
            $elementsArray[$element['ID']] = $element;
        }

        // Получение изображений элементов инфоблока.
        $elementsArray = $this->getPictures($elementsArray);

        // Получение разделов элементов инфоблока.
        $elementsArray = $this->getSections($elementsArray);

        // Получение свойств элементов инфоблока.
        if (!empty($parameters['select_properties']) && is_array($parameters['select_properties'])) {
            $elementsArray = $this->getProperties($elementsArray, $parameters['select_properties']);
        }

        //получение seo данных
        if ($parameters['seo']) {
            $elementsArray = $this->getSeo($elementsArray);
        }

        return $elementsArray;
    }

    /**
     * Метод возвращает количество элементов инфоблока.
     * Является оберткой над D7-методами getList и getSelectedRowsCount.
     *
     * @param array $filter
     * @return int
     * @throws ArgumentException
     */
    public function getCount(array $filter): int
    {
        $elements = ElementTable::getList(['filter' => $filter]);

        return $elements->getSelectedRowsCount();
    }

    /**
     * Метод получает изображения элементов инфоблока.
     *
     * @param array $elements
     * @return array
     * @throws ArgumentException
     */
    private function getPictures(array $elements): array
    {
        $detailPictures = [];
        $previewPictures = [];

        foreach ($elements as $element) {
            if (!empty($element['DETAIL_PICTURE'])) {
                $detailPictures[$element['ID']] = $element['DETAIL_PICTURE'];
            }

            if (!empty($element['PREVIEW_PICTURE'])) {
                $previewPictures[$element['ID']] = $element['PREVIEW_PICTURE'];
            }
        }

        $detailPictures = $this->imageRepository->getByIds($detailPictures);
        $previewPictures = $this->imageRepository->getByIds($previewPictures);

        foreach ($detailPictures as $elementId => $detailPicture) {
            $elements[$elementId]['DETAIL_PICTURE'] = $detailPicture;
        }

        foreach ($previewPictures as $elementId => $previewPicture) {
            $elements[$elementId]['PREVIEW_PICTURE'] = $previewPicture;
        }

        return $elements;
    }

    /**
     * Метод получает идентификаторы разделов элементов инфоблока.
     *
     * @param array $elements
     * @return array
     */
    private function getSections(array $elements): array
    {
        $elementsIds = [];

        foreach ($elements as $element) {
            if (!empty($element['IBLOCK_SECTION_ID'])) {
                $elementsIds[] = $element['ID'];
            }
        }

        if (empty($elementsIds)) {
            return $elements;
        }

        $sqlSections = implode(', ', $elementsIds);
        $sql = "
            SELECT *
            FROM `b_iblock_section_element`
            WHERE `IBLOCK_ELEMENT_ID` IN ({$sqlSections})
        ";

        $sections = Application::getConnection()->query($sql)->fetchAll();

        foreach ($sections as $section) {
            $elements[$section['IBLOCK_ELEMENT_ID']]['SECTIONS'][] = (int)$section['IBLOCK_SECTION_ID'];
        }

        return $elements;
    }

    /**
     * Метод получает свойства элементов инфоблока.
     *
     * @param array $elements
     * @param array $properties
     * @return array
     * @throws ArgumentException
     */
    private function getProperties(array $elements, array $properties): array
    {
        $properties = $this->propertyRepository->get(
            $this->iblockId,
            array_keys($elements),
            $properties
        );

        foreach ($properties as $elementId => $elementProperties) {
            $elements[$elementId]['PROPERTIES'] = $elementProperties;
        }

        return $elements;
    }

    /**
     * Метод получает SEO элементов инфоблока.
     * 
     * @param array $elements
     * 
     * @return array
     */
    private function getSeo(array $elements): array
    {
        foreach ($elements as $elementId => $element) {
            $ipropElementValues = new ElementValues($this->iblockId, $element['ID']);
            $elements[$elementId]['SEO'] = $ipropElementValues->getValues();
        }

        return $elements;
    }

    /**
     * Метод выполняет проверку параметров.
     *
     * @param array $parameters
     * @return array
     * @throws ArgumentException
     */
    private function prepareParameters(array $parameters): array
    {
        /**
         * Замены фильтра "IBLOCK_ID" на "=IBLOCK_ID" в следующих целях:
         * - улучшения производительности выборки;
         * - последующего сохранения фильтруемого идентификатора инфоблока в переменную $iblockId.
         */
        if (isset($parameters['filter']['IBLOCK_ID'])) {
            $parameters['filter']['=IBLOCK_ID'] = $parameters['filter']['IBLOCK_ID'];
            unset($parameters['filter']['IBLOCK_ID']);
        }

        // Получение идентификатора инфоблока в фильтре.
        $iblockId = $parameters['filter']['=IBLOCK_ID'] ?? null;
        $this->iblockId = ($iblockId && is_numeric($iblockId) && $iblockId > 0) ? (int)$iblockId : null;

        /**
         * Выборка свойств производится не в \CIBlockElement::GetList, а самописными запросами в базу данных.
         * Сделано для исправления проблемы с выборкой множественных свойств.
         */
        foreach ($parameters['select'] as $parameter) {
            if (strpos($parameter, 'PROPERTY') === 0) {
                throw new ArgumentException('Свойства необходимо указывать в ключе \'select_properties\'');
            }
        }

        $parameters['group'] = $parameters['group'] ?? false;
        $parameters['navStart'] = [];

        if (!empty($parameters['page'])) {
            $parameters['navStart'] = [
                'checkOutOfRange' => true,
                'iNumPage' => (int)$parameters['page'],
            ];
        }

        if (!empty($parameters['limit'])) {
            $limit = (int)$parameters['limit'];
            if (!empty($parameters['page'])) {
                $parameters['navStart']['nPageSize'] = $limit;
            } else {
                $parameters['navStart']['nTopCount'] = $limit;
            }
        }

        return $parameters;
    }

    /**
     * Обновляет значения свойств для элемента
     * 
     * @param mixed $id
     * @param array $props
     * 
     * @return [type]
     */
    public function updateProp(int $id, int $iblock, array $props)
    {
        $res = \CIBlockElement::SetPropertyValuesEx(
            $id,
            $iblock,
            $props
        );

        return $res;
    }

    /**
     * Работа с кешем
     * 
     * @param array $parameters - параметры запроса
     * @param int $ttl - время жизни кеша
     * 
     * @return [type]
     */
    public function cache(array $parameters, int $ttl = 86400)
    {
        $cache = Cache::createInstance();
        $cacheId = md5(serialize($parameters));
        $elementsArray = [];

        if ($cache->initCache($ttl, $cacheId)) {
            $elementsArray = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $elementsArray = $this->getList($parameters);
            $cache->endDataCache($elementsArray);
        }
        return $elementsArray;
    }

    /**
     * Работа с управляемым кешем
     * 
     * @param array $parameters - параметры запроса
     * @param int $ttl - время жизни кеша
     * 
     * @return [type]
     */
    public function managedCache(array $parameters, int $ttl = 86400)
    {
        $cache = Application::getInstance()->getManagedCache();
        $cacheId = md5(serialize($parameters));
        if ($cache->read($ttl, $cacheId)) {
            $elementsArray = $cache->get($cacheId); // достаем переменные из кеша
        } else {
            $elementsArray = $this->getList($parameters);
            $cache->set($cacheId, $elementsArray); // записываем в кеш
        }
        return $elementsArray;
    }

    /**
     * Удаляет кеш по параметрам
     * 
     * @param array $parameters - параметры запроса
     * @param int $ttl - время жизни кеша
     * 
     * @return [type]
     */
    public function clearCache(array $parameters, int $ttl = 86400)
    {
        $parameters = $this->prepareParameters($parameters);
        $cacheId = md5(serialize($parameters));
        $cache = Application::getInstance()->getManagedCache();
        if ($cache->read($ttl, $cacheId)) {
            $cache->clean($cacheId);
            return true;
        }
        return false;
    }
}
