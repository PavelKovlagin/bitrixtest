<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Property;

use Repo\Collection\CollectionInterface;
use Repo\Model\Iblock\Entity\Value\ValueCollection;

class NullProperty extends Property
{
    public function isNull(): bool
    {
        return true;
    }

    public function getValues(): CollectionInterface
    {
        return new ValueCollection();
    }
}
