<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Property;

use Repo\Collection\CollectionInterface;
use Repo\Model\AbstractModel;
use Repo\Model\Iblock\Entity\Value\ValueCollection;
use Repo\Model\Iblock\Entity\Value\ValueFactory;

class Property extends AbstractModel
{
    public const TYPE_STRING = 'S';
    public const TYPE_NUMBER = 'N';
    public const TYPE_LIST = 'L';
    public const TYPE_FILE = 'F';
    public const TYPE_ELEMENT = 'E';
    public const TYPE_SECTION = 'G';

    private int $id;
    private int $iblockId;
    private ?string $xmlId = null;
    private string $code;
    private string $name;
    private string $multiple = 'N';
    private ?string $type = null;
    private ?string $userType = null;
    private ?string $hint = null;
    private int $sort = 500;
    private ValueCollection $values;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        $property = new static();

        if (!empty($data['ID'])) {
            $property->setId((int)$data['ID']);
        }

        if (!empty($data['IBLOCK_ID'])) {
            $property->setIblockId((int)$data['IBLOCK_ID']);
        }

        if (!empty($data['XML_ID'])) {
            $property->setXmlId($data['XML_ID']);
        }

        if (!empty($data['CODE'])) {
            $property->setCode($data['CODE']);
        }

        if (!empty($data['NAME'])) {
            $property->setName($data['NAME']);
        }

        if (!empty($data['MULTIPLE'])) {
            $property->setMultiple($data['MULTIPLE']);
        }

        if (!empty($data['PROPERTY_TYPE'])) {
            $property->setType($data['PROPERTY_TYPE']);
        }

        if (!empty($data['USER_TYPE'])) {
            $property->setUserType($data['USER_TYPE']);
        }

        if (!empty($data['HINT'])) {
            $property->setHint($data['HINT']);
        }

        if (!empty($data['SORT'])) {
            $property->setSort((int)$data['SORT']);
        }

        if (!empty($data['VALUE'])) {
            $property->setValues($data['VALUE']);
        }

        $property->setValues($data['VALUE'] ?? []);

        return $property;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            'ID' => $this->getId(),
            'IBLOCK_ID' => $this->getIblockId(),
            'XML_ID' => $this->getXmlId(),
            'CODE' => $this->getCode(),
            'NAME' => $this->getName(),
            'MULTIPLE' => $this->getMultiple(),
            'TYPE' => $this->getType(),
            'USER_TYPE' => $this->getUserType(),
            'HINT' => $this->getHint(),
            'SORT' => $this->getSort(),
            'VALUE' => $this->getValues(),
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getIblockId(): int
    {
        return $this->iblockId;
    }

    /**
     * @param int $iblockId
     * @return $this
     */
    public function setIblockId(int $iblockId): self
    {
        $this->iblockId = $iblockId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getXmlId(): ?string
    {
        return $this->xmlId;
    }

    /**
     * @param string|null $xmlId
     * @return $this
     */
    public function setXmlId(?string $xmlId): self
    {
        $this->xmlId = $xmlId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getMultiple(): string
    {
        return $this->multiple;
    }

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->multiple === 'Y';
    }

    /**
     * @param string $multiple
     * @return $this
     */
    public function setMultiple(string $multiple = 'N'): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserType(): ?string
    {
        return $this->userType;
    }

    /**
     * @param string|null $userType
     * @return $this
     */
    public function setUserType(?string $userType): self
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHint(): ?string
    {
        return $this->hint;
    }

    /**
     * @param string|null $hint
     * @return $this
     */
    public function setHint(?string $hint): self
    {
        $this->hint = $hint;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return $this
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return ValueCollection
     */
    public function getValues(): CollectionInterface
    {
        return $this->values;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        if ($this->values && $this->values->first() && $value = $this->values->first()->getValue()) {
            return $value;
        }

        return null;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function setValues(array $values): self
    {
        $collection = new ValueCollection();

        foreach ($values as $value) {
            $collection->push(ValueFactory::fromArray($value, $this->getType()));
        }

        $this->values = $collection;

        return $this;
    }
}
