<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Property;

use Repo\Collection\Collection;

class PropertyCollection extends Collection
{
    /**
     * Метод возвращает свойство по символьному коду.
     *
     * @param string $code
     * @return Property|NullProperty
     */
    public function findByCode(string $code)
    {
        return $this[$code] ?? new NullProperty();
    }

    /**
     * Возвращает идентификаторы свойств
     * 
     * @param string $code
     * 
     * @return array|NullProperty
     */
    public function getIdsByCode(string $code)
    {
        if ($this[$code]) {
            foreach ($this[$code]->getValues() as $item) {
                $array[] = $item->getId();
            }
            return $array;
        }        
        return new NullProperty();
    }
}
