<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Property;

use Repo\Model\Iblock\Helper\IblockHelper;
use Repo\Model\Iblock\Service\PropertyHandlerExecutor;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;

class PropertyRepository
{
    private IblockHelper $iblockHelper;
    private PropertyHandlerExecutor $propertyHandlerExecutor;
    private int $iblockId;

    public function __construct()
    {
        $this->iblockHelper = new IblockHelper();
        $this->propertyHandlerExecutor = new PropertyHandlerExecutor();
    }

    /**
     * Метод для получения полей и значений свойств элементов.
     * Возвращает массив свойств, ключами элементов которых являются идентификаторы элементов.
     *
     * @param int $iblockId
     * @param array $elementsIds
     * @param array $propertiesCodes
     * @return array
     * @throws ArgumentException
     */
    public function get(int $iblockId, array $elementsIds, array $propertiesCodes): array
    {
        if (!$elementsIds) {
            return [];
        }

        $this->iblockId = $iblockId;

        $propertiesFields = $this->getFields($propertiesCodes);
        $propertiesValues = $this->getValues($elementsIds, $propertiesFields);

        $properties = [];

        foreach ($elementsIds as $elementId) {
            $elementProperties = $propertiesFields;

            foreach ($elementProperties as &$property) {
                if (!empty($propertiesValues[$elementId]['VALUE'][$property['ID']])) {
                    $property['VALUE'] = $propertiesValues[$elementId]['VALUE'][$property['ID']];
                }
            }

            unset($property);

            $properties[$elementId] = array_combine(
                array_column($elementProperties, 'CODE'),
                $elementProperties
            );
        }

        // Выполнение хендлеров свойств.
        $properties = $this->propertyHandlerExecutor->execute($properties);

        return $properties;
    }

    /**
     * <p><b>Метод для получения полей свойств.</b></p>
     * <p>Возвращает массив свойств без значений.</p>
     *
     * <p>Элементы массива содержат значения следующих полей:</p>
     * <ul style="margin-top: 0;">
     * <li>ID</li>
     * <li>IBLOCK_ID</li>
     * <li>XML_ID</li>
     * <li>CODE</li>
     * <li>PROPERTY_TYPE</li>
     * <li>USER_TYPE</li>
     * <li>ACTIVE</li>
     * <li>NAME</li>
     * <li>SORT</li>
     * <li>MULTIPLE</li>
     * <li>IS_REQUIRED</li>
     * <li>HINT</li>
     * <li>VALUE</li>
     * </ul>
     *
     * @param array $propertiesCodes
     * @return array
     * @throws ArgumentException
     */
    private function getFields(array $propertiesCodes): array
    {
        $propertiesArray = [];
        $properties = PropertyTable::getList([
            'filter' => [
                'IBLOCK_ID' => $this->iblockId,
                'CODE' => $propertiesCodes,
            ],
            'select' => [
                'ID',
                'IBLOCK_ID',
                'XML_ID',
                'CODE',
                'PROPERTY_TYPE',
                'USER_TYPE',
                'ACTIVE',
                'NAME',
                'SORT',
                'MULTIPLE',
                'HINT',
            ],
        ]);

        foreach ($properties->fetchAll() as $property) {
            $property['VALUE'] = [];
            $propertiesArray[$property['ID']] = $property;
        }

        return $propertiesArray;
    }

    /**
     * Метод для получения свойств элементов инфоблока.
     *
     * @param array $elementsIds
     * @param array $propertiesFields
     * @return array
     * @throws ArgumentException
     */
    private function getValues(array $elementsIds, array $propertiesFields): array
    {
        $iblockVersion = $this->iblockHelper->getVersion($this->iblockId);

        if ($iblockVersion === 1) {
            return $this->getValuesForIblockV1(
                $elementsIds,
                array_column($propertiesFields, 'ID')
            );
        }

        if ($iblockVersion === 2) {
            return $this->getValuesForIblockV2(
                $elementsIds,
                $propertiesFields
            );
        }

        throw new ArgumentException("Неизвестная версия инфоблока: {$this->iblockId}", $iblockVersion);
    }

    /**
     * Метод для получения значений свойств элементов инфоблоков первой версии.
     * Возвращает массив свойств, ключами элементов которых являются идентификаторы элементов.
     *
     * @param array $elementsIds
     * @param array $propertiesIds
     * @return array
     */
    private function getValuesForIblockV1(array $elementsIds, array $propertiesIds): array
    {
        $sqlElements = implode(',', $elementsIds);
        $sqlProperties = '';

        if ($propertiesIds) {
            $sqlProperties = implode('\', \'', $propertiesIds);
            $sqlProperties = "AND `IBLOCK_PROPERTY_ID` IN ('$sqlProperties')";
        }

        $sqlWhere = "
            `IBLOCK_ELEMENT_ID` IN ({$sqlElements})
            {$sqlProperties}
        ";

        $sql = "
            SELECT *
            FROM `b_iblock_element_property`
            WHERE {$sqlWhere}
        ";

        $valuesArray = [];
        $values = Application::getConnection()->query($sql)->fetchAll();

        foreach ($values as $value) {
            if (!isset($valuesArray[$value['IBLOCK_ELEMENT_ID']])) {
                $valuesArray[$value['IBLOCK_ELEMENT_ID']] = [
                    'ELEMENT_ID' => $value['IBLOCK_ELEMENT_ID'],
                    'VALUE' => [],
                ];
            }

            $valuesArray[$value['IBLOCK_ELEMENT_ID']]['VALUE'][$value['IBLOCK_PROPERTY_ID']][] = $value;
        }

        return $valuesArray;
    }

    /**
     * Метод для получения значений свойств элементов инфоблоков первой версии.
     * Возвращает массив свойств, ключами элементов которых являются идентификаторы элементов.
     *
     * @param $elementsIds
     * @param $properties
     * @return array
     */
    private function getValuesForIblockV2($elementsIds, $properties): array
    {
        $sqlElementsIds = implode(', ', $elementsIds);
        $singleProperties = $multipleProperties = [];
        $iblockId = $this->iblockId;
        $valuesArray = [];

        foreach ($properties as $property) {
            if ($property['MULTIPLE'] === 'N') {
                $singleProperties[] = "PROPERTY_{$property['ID']}";
                $singleProperties[] = "DESCRIPTION_{$property['ID']}";
            } elseif ($property['MULTIPLE'] === 'Y') {
                $multipleProperties[] = $property['ID'];
            }
        }

        if ($singleProperties) {
            $singleColumns = Application::getConnection()->query("SHOW COLUMNS FROM `b_iblock_element_prop_s{$iblockId}`")->fetchAll();
            $singleColumns = array_column($singleColumns, 'Field');

            $singleProperties = array_filter($singleProperties, static function ($column) use ($singleColumns) {
                return in_array($column, $singleColumns, true);
            });
        }

        if ($singleProperties) {
            $sqlSingleProperties = implode('`, `', $singleProperties);
            $sqlQuerySingleProperties = "
                SELECT `IBLOCK_ELEMENT_ID`, `{$sqlSingleProperties}`
                FROM `b_iblock_element_prop_s{$iblockId}`
                WHERE `IBLOCK_ELEMENT_ID`
                IN ({$sqlElementsIds})
            ";

            $singleValues = Application::getConnection()->query($sqlQuerySingleProperties)->fetchAll();

            foreach ($singleValues as $singleValue) {
                foreach ($singleValue as $propertyKey => $propertyValue) {
                    if (strpos($propertyKey, 'PROPERTY_') !== 0) {
                        continue;
                    }

                    $propertyId = str_replace('PROPERTY_', '', $propertyKey);

                    if (!isset($valuesArray[$singleValue['IBLOCK_ELEMENT_ID']])) {
                        $valuesArray[$singleValue['IBLOCK_ELEMENT_ID']] = [
                            'ELEMENT_ID' => $singleValue['IBLOCK_ELEMENT_ID'],
                            'VALUE' => [],
                        ];
                    }

                    $valuesArray[$singleValue['IBLOCK_ELEMENT_ID']]['VALUE'][$propertyId][] = [
                        'VALUE' => $propertyValue,
                        'DESCRIPTION' => $singleValue["DESCRIPTION_{$propertyId}"],
                    ];
                }
            }
        }

        if ($multipleProperties) {
            $sqlMultipleProperties = implode(', ', $multipleProperties);

            $sqlQueryMultiple = "
                SELECT *
                FROM `b_iblock_element_prop_m{$iblockId}`
                WHERE `IBLOCK_ELEMENT_ID`
                IN ({$sqlElementsIds})
                AND `IBLOCK_PROPERTY_ID`
                IN ({$sqlMultipleProperties})
            ";

            $multipleValues = Application::getConnection()->query($sqlQueryMultiple)->fetchAll();

            foreach ($multipleValues as $multipleValue) {
                if (!isset($valuesArray[$multipleValue['IBLOCK_ELEMENT_ID']])) {
                    $valuesArray[$multipleValue['IBLOCK_ELEMENT_ID']] = [
                        'ELEMENT_ID' => $multipleValue['IBLOCK_ELEMENT_ID'],
                        'VALUE' => [],
                    ];
                }

                $valuesArray[$multipleValue['IBLOCK_ELEMENT_ID']]['VALUE'][$multipleValue['IBLOCK_PROPERTY_ID']][] = $multipleValue;
            }
        }

        return $valuesArray;
    }
}
