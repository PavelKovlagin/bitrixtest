## Класс SectionRepository

Запрашивает разделы инфоблока. Возвращает Collection разделов

### Пример запроса к разделам инфоблока
```php
\Bitrix\Main\Loader::includeModule('hb.repo');
$iblockHelper = new Repo\Model\Iblock\Helper\IblockHelper();
$rep = (new Repo\Model\Iblock\Entity\Section\SectionRepository())->get([
	'filter' => [ //фильтр
		'DEPTH_LEVEL' => 1, //уровень раздела
		'IBLOCK_ID' => $iblockHelper->getIdByCode('infoportal_news_s1') //получение идентификатора инфоблока по символьному коду
	],
	'order' => [ //сортировка
		'NAME' => 'ASC', //сортировка по названию по возрастанию
	],
	'select' => [ //поля
		'UF_*' //получение всех пользовательских полей
	],
    'limit' => 3, //ограничение записей
	'page' => 2 //страница
]);
foreach ($rep as $item) {
	echo '<pre>';
	print_r([
		'NAME' => $item->getName(),
		'CUSTOM' => $item->getUserField('UF_CUSTOM')
	]);
	echo '</pre>';
}
```
Для более детальной настройки рекомендуется отнаследоваться от баховых классов:
- SectionRepository - репозиторий для запроса из инфоблока;
- Collection - коллекция, которая хранит список элементов;
- Section - раздел инфоблока;

### Класс Section

Содержит информацию об одном разделе инфоблока