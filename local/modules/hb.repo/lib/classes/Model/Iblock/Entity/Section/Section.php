<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Section;

use Repo\Model\AbstractModel;
use Repo\Model\File\Entity\File\Image;
use Repo\Model\Iblock\Entity\Seo\Seo;

class Section extends AbstractModel
{
    private ?int $id = null;
    private ?int $updatedAt = null;
    private ?int $updatedBy = null;
    private ?int $createdAt = null;
    private ?int $createdBy = null;
    private ?int $iblockId = null;
    private ?int $sectionId = null;
    private ?string $active = null;
    private ?int $sort = null;
    private ?string $url = null;
    private ?string $name = null;
    private ?string $code = null;
    private ?string $xmlId = null;
    private ?int $leftMargin = null;
    private ?int $rightMargin = null;
    private ?int $depthLevel = null;
    private ?string $description = null;
    private ?string $descriptionType = null;
    private ?Image $picture = null;
    private ?Image $detailPicture = null;
    private ?array $userFields = null;
    private ?Seo $seo = null;

    public static function fromArray(array $data): Section
    {
        $section = new static();

        if (!empty($data['ID'])) {
            $section->setId((int)$data['ID']);
        }

        if (!empty($data['TIMESTAMP_X'])) {
            $section->setUpdatedAt((int)$data['TIMESTAMP_X']);
        }

        if (!empty($data['MODIFIED_BY'])) {
            $section->setUpdatedBy((int)$data['MODIFIED_BY']);
        }

        if (!empty($data['DATE_CREATE'])) {
            $section->setCreatedAt((int)$data['DATE_CREATE']);
        }

        if (!empty($data['CREATED_BY'])) {
            $section->setCreatedBy((int)$data['CREATED_BY']);
        }

        if (!empty($data['IBLOCK_ID'])) {
            $section->setIblockId((int)$data['IBLOCK_ID']);
        }

        if (!empty($data['IBLOCK_SECTION_ID'])) {
            $section->setSectionId((int)$data['IBLOCK_SECTION_ID']);
        }

        if (!empty($data['ACTIVE'])) {
            $section->setActive($data['ACTIVE']);
        }

        if (!empty($data['SORT'])) {
            $section->setSort((int)$data['SORT']);
        }

        if (!empty($data['SECTION_PAGE_URL'])) {
            $section->setUrl(
                \CIBlock::ReplaceSectionUrl($data['SECTION_PAGE_URL'], $data, false, 'S')
            );
        }

        if (!empty($data['NAME'])) {
            $section->setName($data['NAME']);
        }

        if (!empty($data['CODE'])) {
            $section->setCode($data['CODE']);
        }

        if (!empty($data['XML_ID'])) {
            $section->setXmlId($data['XML_ID']);
        }

        if (!empty($data['LEFT_MARGIN'])) {
            $section->setLeftMargin((int)$data['LEFT_MARGIN']);
        }

        if (!empty($data['RIGHT_MARGIN'])) {
            $section->setRightMargin((int)$data['RIGHT_MARGIN']);
        }

        if (!empty($data['DEPTH_LEVEL'])) {
            $section->setDepthLevel((int)$data['DEPTH_LEVEL']);
        }

        if (!empty($data['DESCRIPTION'])) {
            $section->setDescription($data['DESCRIPTION']);
        }

        if (!empty($data['DESCRIPTION_TYPE'])) {
            $section->setDescriptionType($data['DESCRIPTION_TYPE']);
        }

        if (!empty($data['PICTURE'])) {
            $section->setPicture($data['PICTURE']);
        }

        if (!empty($data['PICTURE'])) {
            $section->setDetailPicture($data['PICTURE']);
        }

        if (!empty($data['USER_FIELDS'])) {
            $section->setUserFields($data['USER_FIELDS']);
        }

        $section->setSeo($data['SEO'] ?? []);

        return $section;
    }

    public function toArray(): array
    {
        return [
            'ID' => $this->getId(),
            'TIMESTAMP_X' => $this->getUpdatedAt(),
            'MODIFIED_BY' => $this->getUpdatedBy(),
            'DATE_CREATE' => $this->getCreatedAt(),
            'CREATED_BY' => $this->getCreatedBy(),
            'IBLOCK_ID' => $this->getIblockId(),
            'IBLOCK_SECTION_ID' => $this->getIblockId(),
            'ACTIVE' => $this->isActive(),
            'SORT' => $this->getSort(),
            'NAME' => $this->getName(),
            'CODE' => $this->getCode(),
            'XML_ID' => $this->getXmlId(),
            'LEFT_MARGIN' => $this->getLeftMargin(),
            'RIGHT_MARGIN' => $this->getRightMargin(),
            'DEPTH_LEVEL' => $this->getDepthLevel(),
            'DESCRIPTION' => $this->getDescription(),
            'DESCRIPTION_TYPE' => $this->getDescriptionType(),
            'PICTURE' => $this->getPicture(),
            'DETAIL_PICTURE' => $this->getDetailPicture(),
            'USER_FIELDS' => $this->getUserFields()
        ];
    }

    /**
     * Добавляет массив USER_FIELDS
     * 
     * @param array $userFields
     * 
     * @return self
     */
    public function setUserFields(array $userFields): self
    {
        foreach ($userFields as $key => $field) {
            $this->userFields[$key] = $field;
        }
        return $this;
    }

    /**
     * Возвращает массив USER_FIELDS
     * 
     * @return array|null
     */
    public function getUserFields(): ?array
    {
        return $this->userFields;
    }

    /**
     * Возвращает field из массива USER_FIELDS по коду
     * 
     * @param string $code
     * 
     * @return [type]
     */
    public function getUserField(string $code)
    {
        if ($userField = $this->userFields[$code]) {
            return $userField;
        }
        return null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUpdatedAt(): ?int
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?int $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?int $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getIblockId(): ?int
    {
        return $this->iblockId;
    }

    public function setIblockId(?int $iblockId): self
    {
        $this->iblockId = $iblockId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSectionId(): ?int
    {
        return $this->sectionId;
    }

    public function setSectionId(?int $sectionId): self
    {
        $this->sectionId = $sectionId;

        return $this;
    }

    public function getActive(): ?string
    {
        return $this->active;
    }

    public function isActive(): bool
    {
        return $this->active === 'Y';
    }

    public function setActive(string $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(?int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getXmlId(): ?string
    {
        return $this->xmlId;
    }

    public function setXmlId(?string $xmlId): self
    {
        $this->xmlId = $xmlId;

        return $this;
    }

    public function getLeftMargin(): ?int
    {
        return $this->leftMargin;
    }

    public function setLeftMargin(?int $leftMargin): self
    {
        $this->leftMargin = $leftMargin;

        return $this;
    }

    public function getRightMargin(): ?int
    {
        return $this->rightMargin;
    }

    public function setRightMargin(?int $rightMargin): self
    {
        $this->rightMargin = $rightMargin;

        return $this;
    }

    public function getDepthLevel(): ?int
    {
        return $this->depthLevel;
    }

    public function setDepthLevel(?int $depthLevel): self
    {
        $this->depthLevel = $depthLevel;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescriptionType(): ?string
    {
        return $this->descriptionType;
    }

    public function setDescriptionType(?string $descriptionType): self
    {
        $this->descriptionType = $descriptionType;

        return $this;
    }

    /**
     * Возвращает изображение
     * 
     * @return array|null
     */
    public function getPictureResize(): ?array
    {
        if (($picture = $this->getPicture()) && $picture->getId()) {
            return $picture->getResizedImage(Image::SIZE_STOCK_PREVIEW);
        }
        return null;
    }

    /**
     * Возвращает детальное изображение
     * 
     * @return array|null
     */
    public function getDetailPictureResized(array $size = null): ?array
    {
        if (($picture = $this->getDetailPicture()) && $picture->getId()) {
            return $picture->getResizedImage($size ?? Image::SIZE_STOCK_PREVIEW);
        }
        return null;
    }
        

    public function getPicture(): ?Image
    {
        return $this->picture;
    }

    public function setPicture(?Image $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getDetailPicture(): ?Image
    {
        return $this->detailPicture;
    }

    public function setDetailPicture(?Image $detailPicture): self
    {
        $this->detailPicture = $detailPicture;

        return $this;
    }

    /**
     * Возвращает SEO для раздела
     * 
     * @return Seo|null
     */
    public function getSeo(): ?Seo
    {
        return $this->seo;
    }

    /**
     * Устанавливает SEO для раздела
     * 
     * @param array $seo
     * @return $this
     */
    public function setSeo(array $seo): self
    {
        $this->seo = Seo::fromArray($seo);

        return $this;
    }
}
