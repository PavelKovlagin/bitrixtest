<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Section;

use Repo\Collection\Collection;
use Repo\Model\File\Entity\File\ImageRepository;
use Bitrix\Main\Data\Cache;
use Bitrix\Iblock\InheritedProperty\SectionValues;
use Repo\Helper\Logging;

class SectionRepository
{
    private ?int $iblockId;

    private ImageRepository $imageRepository;

    public function __construct()
    {
        $this->imageRepository = new ImageRepository();
    }

    /**
     * Метод возвращает коллекцию разделов инфоблока.
     *
     * @param array $parameters
     * @return Collection
     */
    public function get(array $parameters): Collection
    {
        $sectionsArray = $this->getArray($parameters);

        $collection = new Collection();

        foreach ($sectionsArray as $section) {
            $collection[$section['ID']] = Section::fromArray($section);
        }

        Logging::log(['hello' => 'world'], get_class(), __FILE__);

        return $collection;
    }

    /**
     * Метод возвращает массив разделов инфоблока.
     *
     * @param array $parameters
     * @return array
     */
    public function getArray(array $parameters, bool $caching = true): array
    {
        $parameters = $this->prepareParameters($parameters);

        if ($caching) {
            $cacheId = md5(serialize($parameters));
            $cache = Cache::createInstance();
            $sectionsArray = [];

            if ($cache->initCache(86400, $cacheId)) {
                $sectionsArray = $cache->getVars();
            } elseif ($cache->startDataCache()) {
                $sectionsArray = $this->getList($parameters);
                $cache->endDataCache($sectionsArray);
            }        
        } else {
            $sectionsArray = $this->getList($parameters);
        }

        return $sectionsArray;
    }

    /**
     * Метод возвращает результат запроса
     * 
     * @param array $parameters
     * 
     * @return array
     */
    public function getList(array $parameters): array
    {
        $sections = \CIBlockSection::GetList(
            (array)$parameters['order'],
            (array)$parameters['filter'],
            (bool)$parameters['bIncCount'],
            (array)$parameters['select'],
            $parameters['navStart']
        );
        $sectionsArray = [];

        while ($section = $sections->Fetch()) {
            $sectionsArray[$section['ID']] = $section;
        }

        // Получение изображений разделов инфоблока.
        $sectionsArray = $this->getPictures($sectionsArray);

        // Получение пользовательских полей разделов инфоблока.
        $sectionsArray = $this->getUserFields($sectionsArray);
        
        if ($parameters['seo']) {
            $sectionsArray = $this->getSeo($sectionsArray);
        }

        return $sectionsArray;
    }



    /**
     * Метод получает SEO раздлелов инфоблока
     * 
     * @param array $elements
     * 
     * @return array
     */
    private function getSeo(array $elements): array
    {
        foreach ($elements as $elementId => $element) {
            $ipropSectionValues = new SectionValues($this->iblockId, $element['ID']);
            $elements[$elementId]['SEO'] = $ipropSectionValues->getValues();
        }

        return $elements;
    }

    /**
     * Метод получает изображения разделов инфоблока.
     *
     * @param array $sections
     * @return array
     */
    private function getPictures(array $sections): array
    {
        $pictures = [];

        foreach ($sections as $section) {
            if (!empty($section['PICTURE'])) {
                $pictures[$section['ID']] = $section['PICTURE'];
            }
        }

        $pictures = $this->imageRepository->getByIds($pictures);

        foreach ($pictures as $sectionId => $picture) {
            $sections[$sectionId]['PICTURE'] = $picture;
        }

        return $sections;
    }

    public function getUserFields(array $sections): array
    {
        foreach ($sections as $sectionId => $section) {
            foreach ($section as $field => $value) {
                if (strpos($field, 'UF_') === 0) {
                    $sections[$sectionId]['USER_FIELDS'][$field] = $value;
                    unset($sections[$sectionId][$field]);
                }
            }
        }

        return $sections;
    }

    /**
     * Метод выполняет проверку параметров.
     *
     * @param array $parameters
     * @return array
     */
    private function prepareParameters(array $parameters): array
    {
        $parameters['navStart'] = [];

        if (isset($parameters['filter']['IBLOCK_ID'])) {
            $parameters['filter']['=IBLOCK_ID'] = $parameters['filter']['IBLOCK_ID'];
        }

        $iblockId = $parameters['filter']['=IBLOCK_ID'] ?? null;
        $this->iblockId = ($iblockId && is_numeric($iblockId) && $iblockId > 0) ? (int)$iblockId : null;

        if (!empty($parameters['page'])) {
            $parameters['navStart'] = [
                'checkOutOfRange' => true,
                'iNumPage' => (int)$parameters['page'],
            ];
        }

        if (!empty($parameters['limit'])) {
            $limit = (int)$parameters['limit'];
            if (!empty($parameters['page'])) {
                $parameters['navStart']['nPageSize'] = $limit;
            } else {
                $parameters['navStart']['nTopCount'] = $limit;
            }
        }

        return $parameters;
    }
}
