<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Seo;

use Repo\Model\AbstractModel;

class Seo extends AbstractModel
{
    private ?string $metaTitle = null; //Шаблон META TITLE
    private ?string $metaDescription = null; //Шаблон META DESCRIPTION
    private ?string $metaKeywords = null; //Шаблон META KEYWORDS
    private ?string $pageTitle = null; //Заголовок элемента
    private ?string $previewPictureFileAlt = null; // Шаблон ALT анонса картинки
    private ?string $previewPictureFileTitle = null; //Шаблон TITLE анонса картинки
    private ?string $previewPictureFileName = null; //Шаблон имени файла анонса картинки
    private ?string $detailPictureFileAlt = null; // Шаблон ALT детальной картинки
    private ?string $detailPictureFileTitle = null; //Шаблон TITLE детальной картинки
    private ?string $detailPictureFileName = null; //Шаблон имени файла детальной картинки
    private ?string $sectionMetaTitle = null;
    private ?string $sectionMetaKeywords = null;
    private ?string $sectionMetaDescription = null;
    private ?string $sectionPageTitle = null;

    public static function fromArray(array $data): self
    {
        $element = new static();

        if (!empty($data['ELEMENT_META_TITLE'])) {
            $element->setMetaTitle((string)$data['ELEMENT_META_TITLE']);
        }

        if (!empty($data['ELEMENT_META_DESCRIPTION'])) {
            $element->setMetaDescription((string)$data['ELEMENT_META_DESCRIPTION']);
        }

        if (!empty($data['ELEMENT_META_KEYWORDS'])) {
            $element->setMetaKeywords((string)$data['ELEMENT_META_KEYWORDS']);
        }

        if (!empty($data['ELEMENT_PAGE_TITLE'])) {
            $element->setPageTitle((string)$data['ELEMENT_PAGE_TITLE']);
        }

        if (!empty($data['SECTION_META_TITLE'])) {
            $element->setSectionMetaTitle((string)$data['SECTION_META_TITLE']);
        }

        if (!empty($data['SECTION_META_KEYWORDS'])) {
            $element->setSectionMetaKeywords((string)$data['SECTION_META_KEYWORDS']);
        }

        if (!empty($data['SECTION_META_DESCRIPTION'])) {
            $element->setSectionMetaDescription((string)$data['SECTION_META_DESCRIPTION']);
        }

        if (!empty($data['SECTION_PAGE_TITLE'])) {
            $element->setSectionPageTitle((string)$data['SECTION_PAGE_TITLE']);
        }

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [];
    }

    
    /**
     * Устанавливает шаблон META TITLE
     * 
     * @param string $metaTitle
     * 
     * @return self
     */
    public function setMetaTitle(string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Возвращает шаблон META TITLE
     * 
     * @return string|null
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * Устанавливает шаблон META DESCRIPTION SEO
     * 
     * @param string $metaDescription
     * 
     * @return self
     */
    public function setMetaDescription(string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Возвращает шаблон META DESCRIPTION SEO
     * 
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * Устанавливает заголовок элемента SEO
     * 
     * @param string $pageTitle
     * 
     * @return self
     */
    public function setPageTitle(string $pageTitle): self
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    /**
     * Возвращает заголовок элемента SEO
     * 
     * @return string|null
     */
    public function getPageTitle(): ?string
    {
        return $this->pageTitle;
    }

    /**
     * Устанавливает шаблон META KEYWORDS SEO
     * 
     * @param string $metaKeywords
     * 
     * @return self
     */
    public function setMetaKeywords(string $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Возвращает шаблон META KEYWORDS SEO
     * 
     * @return string|null
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * Устанавливает шаблон ALT анонса картинки SEO
     * 
     * @param string $previewPictureFileAlt
     * 
     * @return self
     */
    public function setPriviewPictureFileAlt(string $previewPictureFileAlt): self
    {
        $this->previewPictureFileAlt = $previewPictureFileAlt;

        return $this;
    }  

    /**
     * Возвращает шаблон ALT анонса картинки SEO
     * 
     * @return string|null
     */
    public function getPreviewPictureFileAlt(): ?string
    {
        return $this->previewPictureFileAlt;
    }

    /**
     * Устанавливает шаблон TITLE анонса картинки SEO 
     * 
     * @param string $previewPictureFileTitle
     * 
     * @return self
     */
    public function setPreviewPictureFileTitle(string $previewPictureFileTitle): self
    {
        $this->previewPictureFileTitle = $previewPictureFileTitle;

        return $this;
    }

    /**
     * Возвращает шаблон TITLE анонса картинки SEO
     * 
     * @return string|null
     */
    public function getPreviewPictureFileTitle(): ?string
    {
        return $this->previewPictureFileTitle;
    }

    /**
     * Устанавливает шаблон имени файла анонса картинки SEO
     * 
     * @param string $previewPictureFileName
     * 
     * @return self
     */
    public function setPreviewPictureFileName(string $previewPictureFileName): self
    {
        $this->previewPictureFileName = $previewPictureFileName;

        return $this;
    }

    /**
     * Возвращает шаблон имени файла анонса картинки SEO
     * 
     * @return string|null
     */
    public function getPreviewPictureFileName(): ?string
    {
        return $this->previewPictureFileName;
    }
    
    /**
     * Устанавливает шаблон ALT детальной картинки SEO
     * 
     * @param string $detailPictureFileAlt
     * 
     * @return self
     */
    public function setDetailPictureFileAlt(string $detailPictureFileAlt): self
    {
        $this->detailPictureFileAlt = $detailPictureFileAlt;

        return $this;
    }

    /**
     * Возвращает шаблон ALT детальной картинки SEO
     * 
     * @return string|null
     */
    public function getDetailPictureFileAlt(): ?string
    {
        return $this->detailPictureFileAlt;
    }

    /**
     * 
     * Устанавливает шаблон TITLE детальной картинки SEO
     * 
     * @param string $detailPictureFileTitle
     * 
     * @return self
     */
    public function setDetailPictureFileTitle(string $detailPictureFileTitle): self
    {
        $this->detailPictureFileTitle = $detailPictureFileTitle;

        return $this;
    }

    /**
     * Возвращает шаблон TITLE детальной картинки SEO
     * 
     * @return string|null
     */
    public function getDetailPictureFileTitle(): ?string
    {
        return $this->detailPictureFileTitle;
    }

    /**
     * Устанавливает шаблон имени файла детальной картинки SEO
     * 
     * @param string $detailPictureFileName
     * 
     * @return self
     */
    public function setDetailPictureFileName(string $detailPictureFileName): self
    {
        $this->detailPictureFileName = $detailPictureFileName;

        return $this;
    }

    /**
     * Возвращает шаблон имени файла детальной картинки SEO
     * 
     * @return string|null
     */
    public function getDetailPictureFileName(): ?string
    {
        return $this->detailPictureFileName;
    }

    /**
     * Устанавливает шаблон META TITLE для раздела SEO
     * 
     * @param string $sectionMetaTitle
     * 
     * @return self
     */
    public function setSectionMetaTitle(string $sectionMetaTitle): self
    {
        $this->sectionMetaTitle = $sectionMetaTitle;

        return $this;
    }

    /**
     * Возвращает шаблон META TITLE для раздела SEO
     * 
     * @return string|null
     */
    public function getSectionMetaTitle(): ?string
    {
        return $this->sectionMetaTitle;
    }

    /**
     * Устанавливает шаблон META KEYWORDS для раздела SEO
     * 
     * @param string $sectionMetaKeywords
     * 
     * @return self
     */
    public function setSectionMetaKeywords(string $sectionMetaKeywords): self
    {
        $this->sectionMetaKeywords = $sectionMetaKeywords;

        return $this;
    }

    /**
     * Возвращает шаблон META KEYWORDS для раздела SEO
     * 
     * @return string|null
     */
    public function getSectionMetaKeywords(): ?string
    {
        return $this->sectionMetaKeywords;
    }

    /**
     * Устанавливает шаблон META DESCRIPTION для раздела SEO
     * 
     * @param string $sectionMetaDescription
     * 
     * @return self
     */
    public function setSectionMetaDescription(string $sectionMetaDescription): self
    {
        $this->sectionMetaDescription = $sectionMetaDescription;

        return $this;
    }

    /**
     * Возвращает шаблон META DESCRIPTION для раздела SEO
     * 
     * @return string|null
     */
    public function getSectionMetaDescription(): ?string
    {
        return $this->sectionMetaDescription;
    }

    /**
     * Устанавливает заголовок раздела
     * 
     * @param string $sectionPageTitle
     * 
     * @return self
     */
    public function setSectionPageTitle(string $sectionPageTitle): self
    {
        $this->sectionPageTitle = $sectionPageTitle;

        return $this;
    }

    /**
     * Возвращает заголовок раздела
     * 
     * @return string|null
     */
    public function getSectionPageTitle(): ?string
    {
        return $this->sectionPageTitle;
    }
}
