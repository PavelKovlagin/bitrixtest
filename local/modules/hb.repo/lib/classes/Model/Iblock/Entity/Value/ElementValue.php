<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Value;

class ElementValue extends Value
{
    private int $iblockId;
    private ?string $xmlId = null;
    private ?string $code = null;
    private string $name;
    private int $sort = 500;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        /** @var static $value */
        $value = parent::fromArray($data);

        if (!empty($data['IBLOCK_ID'])) {
            $value->setIblockId((int)$data['IBLOCK_ID']);
        }

        if (!empty($data['XML_ID'])) {
            $value->setXmlId($data['XML_ID']);
        }

        if (!empty($data['CODE'])) {
            $value->setCode($data['CODE']);
        }

        if (!empty($data['NAME'])) {
            $value->setName($data['NAME']);
        }

        if (!empty($data['SORT'])) {
            $value->setSort((int)$data['SORT']);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        $value = parent::toArray();

        $value['IBLOCK_ID'] = $this->getIblockId();
        $value['NAME'] = $this->getName();
        $value['CODE'] = $this->getCode();
        $value['XML_ID'] = $this->getXmlId();
        $value['SORT'] = $this->getSort();

        return $value;
    }

    /**
     * @return int
     */
    public function getIblockId(): int
    {
        return $this->iblockId;
    }

    /**
     * @param int $iblockId
     * @return self
     */
    public function setIblockId(int $iblockId): self
    {
        $this->iblockId = $iblockId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getXmlId(): ?string
    {
        return $this->xmlId;
    }

    /**
     * @param string|null $xmlId
     * @return self
     */
    public function setXmlId(?string $xmlId): self
    {
        $this->xmlId = $xmlId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return self
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return self
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }
}
