<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Value;

class EnumValue extends Value
{
    private ?string $xmlId = null;
    private int $sort = 500;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        /** @var static $value */
        $value = parent::fromArray($data);

        if (!empty($data['XML_ID'])) {
            $value->setXmlId($data['XML_ID']);
        }

        if (!empty($data['SORT'])) {
            $value->setSort((int)$data['SORT']);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        $value = parent::toArray();

        $value['XML_ID'] = $this->getXmlId();
        $value['SORT'] = $this->getSort();

        return $value;
    }

    /**
     * @return string|null
     */
    public function getXmlId(): ?string
    {
        return $this->xmlId;
    }

    /**
     * @param string|null $xmlId
     * @return self
     */
    public function setXmlId(?string $xmlId): self
    {
        $this->xmlId = $xmlId;

        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return self
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }
}
