<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Value;

use Bitrix\Main\Type\DateTime;

class FileValue extends Value
{
    private ?DateTime $timestamp = null;
    private ?string $moduleId = null;
    private ?int $height = null;
    private ?int $width = null;
    private ?int $fileSize = null;
    private ?string $contentType = null;
    private ?string $subDir = null;
    private ?string $fileName = null;
    private ?string $originalName = null;
    private ?string $handlerId = null;
    private ?string $externalId = null;
    private ?string $src = null;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        /** @var static $value */
        $value = parent::fromArray($data);

        if (!empty($data['TIMESTAMP_X'])) {
            $value->setTimestamp($data['TIMESTAMP_X']);
        }

        if (!empty($data['MODULE_ID'])) {
            $value->setModuleId($data['MODULE_ID']);
        }

        if (!empty($data['HEIGHT'])) {
            $value->setHeight((int)$data['HEIGHT']);
        }

        if (!empty($data['WIDTH'])) {
            $value->setWidth((int)$data['WIDTH']);
        }

        if (!empty($data['FILE_SIZE'])) {
            $value->setFileSize((int)$data['FILE_SIZE']);
        }

        if (!empty($data['CONTENT_TYPE'])) {
            $value->setContentType($data['CONTENT_TYPE']);
        }

        if (!empty($data['SUBDIR'])) {
            $value->setSubDir($data['SUBDIR']);
        }

        if (!empty($data['FILE_NAME'])) {
            $value->setFileName($data['FILE_NAME']);
        }

        if (!empty($data['ORIGINAL_NAME'])) {
            $value->setOriginalName($data['ORIGINAL_NAME']);
        }

        if (!empty($data['HANDLER_ID'])) {
            $value->setHandlerId($data['HANDLER_ID']);
        }

        if (!empty($data['EXTERNAL_ID'])) {
            $value->setExternalId($data['EXTERNAL_ID']);
        }

        if (!empty($data['SRC'])) {
            $value->setSrc($data['SRC']);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            'TIMESTAMP_X' => $this->getTimestamp(),
            'MODULE_ID' => $this->getModuleId(),
            'HEIGHT' => $this->getHeight(),
            'WIDTH' => $this->getWidth(),
            'FILE_SIZE' => $this->getFileSize(),
            'CONTENT_TYPE' => $this->getContentType(),
            'SUBDIR' => $this->getSubDir(),
            'FILE_NAME' => $this->getFileName(),
            'ORIGINAL_NAME' => $this->getOriginalName(),
            'HANDLER_ID' => $this->getHandlerId(),
            'EXTERNAL_ID' => $this->getExternalId(),
            'SRC' => $this->getSrc(),
        ];
    }

    /**
     * @return DateTime|null
     */
    public function getTimestamp(): ?DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param DateTime|null $timestamp
     * @return self
     */
    public function setTimestamp(?DateTime $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuleId(): ?string
    {
        return $this->moduleId;
    }

    /**
     * @param string|null $moduleId
     * @return self
     */
    public function setModuleId(?string $moduleId): self
    {
        $this->moduleId = $moduleId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     * @return self
     */
    public function setHeight(?int $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @param int|null $width
     * @return self
     */
    public function setWidth(?int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getFileSize(): ?int
    {
        return $this->fileSize;
    }

    /**
     * @param int|null $fileSize
     * @return self
     */
    public function setFileSize(?int $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    /**
     * @param string|null $contentType
     * @return self
     */
    public function setContentType(?string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubDir(): ?string
    {
        return $this->subDir;
    }

    /**
     * @param string|null $subDir
     * @return self
     */
    public function setSubDir(?string $subDir): self
    {
        $this->subDir = $subDir;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string|null $fileName
     * @return self
     */
    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    /**
     * @param string|null $originalName
     * @return self
     */
    public function setOriginalName(?string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHandlerId(): ?string
    {
        return $this->handlerId;
    }

    /**
     * @param string|null $handlerId
     * @return self
     */
    public function setHandlerId(?string $handlerId): self
    {
        $this->handlerId = $handlerId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     * @return self
     */
    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSrc(): ?string
    {
        return $this->src;
    }

    /**
     * @param string|null $src
     * @return self
     */
    public function setSrc(?string $src): self
    {
        $this->src = $src;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->getSrc();
    }

    /**
     * @return string
     */
    public function getValueType(): string
    {
        return self::DEFAULT_VALUE_TYPE;
    }
}
