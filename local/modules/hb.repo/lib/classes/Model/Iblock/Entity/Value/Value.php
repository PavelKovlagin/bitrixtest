<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Value;

use Repo\Model\AbstractModel;

class Value extends AbstractModel
{
    private ?int $id = null;
    private $value;
    private $valueType;
    private ?string $description = null;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        $value = new static();

        if (!empty($data['ID'])) {
            $value->setId((int)$data['ID']);
        }

        if (!empty($data['VALUE'])) {
            $value->setValue($data['VALUE']);
        }

        if (!empty($data['VALUE_TYPE'])) {
            $value->setValueType($data['VALUE_TYPE']);
        }

        if (!empty($data['DESCRIPTION'])) {
            $value->setDescription($data['DESCRIPTION']);
        }

        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            'ID' => $this->getId(),
            'VALUE' => $this->getValue(),
            'VALUE_TYPE' => $this->getValueType(),
            'DESCRIPTION' => $this->getDescription(),
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return self
     */
    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueType()
    {
        return $this->valueType;
    }

    /**
     * @param $valueType
     * @return self
     */
    public function setValueType($valueType): self
    {
        $this->valueType = $valueType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->value;
    }
}