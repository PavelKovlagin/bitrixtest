<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Value;

use Repo\Collection\Collection;
use Bitrix\Main\ArgumentException;

class ValueCollection extends Collection
{
    /**
     * @param $offset
     * @param $value
     * @throws ArgumentException
     */
    public function offsetSet($offset, $value): void
    {
        if ($value instanceof Value) {
            parent::offsetSet($offset, $value);
        } else {
            throw new ArgumentException('Variable $offset does not implement ValueInterface.');
        }
    }

    /**
     * Возвращает массив идентификаторов свойства
     * 
     * @return array|null
     */
    public function getIds(): ?array
    {
        foreach ($this->container as $item) {
            $ids[] = $item->getId();
        }
        return $ids;
    }
}
