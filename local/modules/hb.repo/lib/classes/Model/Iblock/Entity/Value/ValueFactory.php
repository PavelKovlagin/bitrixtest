<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Entity\Value;

use Repo\Model\Iblock\Entity\Property\Property;

class ValueFactory
{
    public static function fromArray(array $data, string $type = null): Value
    {
        switch ($type) {
            case Property::TYPE_LIST:
                return static::createEnumValue($data);
            case Property::TYPE_FILE:
                return static::createFileValue($data);
            case Property::TYPE_ELEMENT:
                return static::createElementValue($data);
            default:
                return static::createSimpleValue($data);
        }
    }

    public static function createEnumValue(array $data): Value
    {
        return EnumValue::fromArray($data);
    }

    public static function createFileValue(array $data): Value
    {
        return FileValue::fromArray($data);
    }

    public static function createElementValue(array $data): Value
    {
        return ElementValue::fromArray($data);
    }

    public static function createSimpleValue(array $data): Value
    {
        return Value::fromArray($data);
    }
}
