<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Handler\Property\Element;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\ArgumentException;

class Handler
{
    /**
     * Метод для получения полей свойств типа "Привязка к элементам".
     *
     * @param array $properties
     * @return array
     * @throws ArgumentException
     */
    public function handle(array $properties): array
    {
        $elementsIds = $elementsArray = [];

        foreach ($properties as $elementId => $elementProperties) {
            foreach ($elementProperties as $property) {
                if ($property['PROPERTY_TYPE'] === 'E' && !empty($property['VALUE'])) {
                    $elementsIds[] = array_column($property['VALUE'] ?? [], 'VALUE');
                }
            }
        }

        $elementsIds = array_merge([], ... $elementsIds);

        if (!$elementsIds) {
            return $properties;
        }

        $elements = ElementTable::getList([
            'select' => [
                'ID',
                'IBLOCK_ID',
                'XML_ID',
                'NAME',
                'CODE',
                'SORT',
            ],
            'filter' => [
                '=ACTIVE' => 'Y',
                '=ID' => $elementsIds,
            ],
        ]);

        while ($element = $elements->fetch()) {
            $elementsArray[$element['ID']] = $element;
        }

        foreach ($properties as $elementId => &$elementProperties) {
            foreach ($elementProperties as &$property) {
                if ($property['PROPERTY_TYPE'] !== 'E') {
                    continue;
                }

                $values = [];

                foreach ($property['VALUE'] as $value) {
                    $elementId = (int)$value['VALUE'];

                    if (!isset($elementsArray[$elementId])) {
                        continue;
                    }

                    $newValue = $elementsArray[$elementId];
                    $newValue['DESCRIPTION'] = $value['DESCRIPTION'] ?? null;
                    $values[] = $newValue;
                }

                $property['VALUE'] = $values;
            }
        }

        return $properties;
    }
}
