<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Handler\Property\Enum;

use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\ArgumentException;

class Handler
{
    /**
     * Метод для получения полей свойств типа "Список".
     *
     * @param array $properties
     * @return array
     * @throws ArgumentException
     */
    public function handle(array $properties): array
    {
        $listsIds = $listsArray = [];

        foreach ($properties as $elementId => $elementProperties) {
            foreach ($elementProperties as $property) {
                if ($property['PROPERTY_TYPE'] === 'L' && !empty($property['VALUE'])) {
                    $listsIds[] = array_column($property['VALUE'] ?? [], 'VALUE');
                }
            }
        }

        $listsIds = array_merge([], ...$listsIds);

        if (!$listsIds) {
            return $properties;
        }

        $lists = PropertyEnumerationTable::getList([
            'select' => [
                '*',
            ],
            'filter' => [
                '=ID' => $listsIds,
            ],
            'order' => [
                'SORT' => 'ASC',
                'VALUE' => 'ASC',
            ],
        ]);

        while ($list = $lists->fetch()) {
            $listsArray[$list['ID']] = $list;
        }

        foreach ($properties as $elementId => &$elementProperties) {
            foreach ($elementProperties as &$property) {
                if ($property['PROPERTY_TYPE'] !== 'L') {
                    continue;
                }

                $values = [];

                foreach ($property['VALUE'] as $value) {
                    if (!isset($listsArray[$value['VALUE']])) {
                        continue;
                    }

                    $newValue = $listsArray[$value['VALUE']];
                    $newValue['DESCRIPTION'] = $value['DESCRIPTION'] ?? null;
                    $values[] = $newValue;
                }

                $property['VALUE'] = $values;
            }
        }

        return $properties;
    }
}
