<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Handler\Property\File;

use Repo\Model\File\Entity\File\FileTable;
use Bitrix\Main\ArgumentException;

class Handler
{
    /**
     * Метод для получения полей свойств типа "Файл".
     *
     * @param array $properties
     * @return array
     * @throws ArgumentException
     */
    public function handle(array $properties): array
    {
        $filesIds = $filesArray = [];

        foreach ($properties as $elementId => $elementProperties) {
            foreach ($elementProperties as $property) {
                if ($property['PROPERTY_TYPE'] === 'F' && !empty($property['VALUE'])) {
                    $filesIds[] = array_column($property['VALUE'] ?? [], 'VALUE');
                }
            }
        }

        $filesIds = array_merge([], ...$filesIds);

        if (!$filesIds) {
            return $properties;
        }

        $files = FileTable::getList([
            'filter' => [
                '=ID' => $filesIds,
            ],
            'select' => [
                '*',
                'SRC',
            ],
        ]);

        while ($file = $files->fetch()) {
            $filesArray[$file['ID']] = $file;
        }

        foreach ($properties as $elementId => &$elementProperties) {
            foreach ($elementProperties as &$property) {
                if ($property['PROPERTY_TYPE'] !== 'F') {
                    continue;
                }

                $values = [];

                foreach ($property['VALUE'] as $value) {
                    $fileId = (int)$value['VALUE'];

                    if (!isset($filesArray[$fileId])) {
                        continue;
                    }

                    $newValue = $filesArray[$fileId];
                    $newValue['VALUE'] = $newValue['SRC'];
                    $newValue['DESCRIPTION'] = $newValue['DESCRIPTION'] ?? null;

                    $values[] = $newValue;
                }

                $property['VALUE'] = $values;
            }
        }

        return $properties;
    }
}
