<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Handler\Property\HtmlText;

class Handler
{
    /**
     * Метод для десериализации и получения значений полей свойств типа "HTML/текст".
     *
     * @param array $properties
     * @return array
     */
    public function handle(array $properties): array
    {
        foreach ($properties as $elementId => &$elementProperties) {
            foreach ($elementProperties as &$property) {
                if ($property['PROPERTY_TYPE'] === 'S' && $property['USER_TYPE'] === 'HTML' && !empty($property['VALUE'])) {
                    foreach ($property['VALUE'] as &$value) {
                        if (!empty($value['VALUE']) && $newValue = unserialize($value['VALUE'], ['allowed_classes' => false])) {
                            $value['VALUE'] = $newValue['TEXT'];
                            $value['VALUE_TYPE'] = strtolower($newValue['TYPE']);
                        }
                    }
                }
            }
        }

        return $properties;
    }
}
