<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Helper;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\ArgumentException;

class IblockHelper
{
    /**
     * @param int $iblockId
     * @param bool $staticCache
     * @return array|null
     * @throws ArgumentException
     */
    public function getData(int $iblockId, bool $staticCache = true): ?array
    {
        static $data = [];

        if ($iblockId < 1) {
            throw new ArgumentException('Iblock id is not specified', $iblockId);
        }

        if (!$staticCache || empty($data[$iblockId])) {
            $iblock = IblockTable::getRow([
                'filter' => [
                    '=ID' => $iblockId,
                ],
                'limit' => 1,
            ]);

            if ((int)$iblock['ID'] < 1) {
                throw new \RuntimeException("Iblock {$iblockId} not found.");
            }

            $data[$iblockId] = $iblock;
        }

        return $data[$iblockId];
    }

    /**
     * @param int $iblockId
     * @param bool $staticCache
     * @return int
     * @throws ArgumentException
     */
    public function getVersion(int $iblockId, $staticCache = true): int
    {
        return (int)$this->getData($iblockId, $staticCache)['VERSION'];
    }

    /**
     * @param string $code
     * @param bool $staticCache
     * @return array|false|mixed
     * @throws ArgumentException
     */
    public function getDataByCode(string $code, $staticCache = true)
    {
        static $data = [];

        if (!trim($code)) {
            throw new ArgumentException('Iblock code is not specified.');
        }

        if (!$staticCache || empty($data[$code])) {
            $iblock = IblockTable::getList([
                'filter' => [
                    '=CODE' => $code,
                ],
                'limit' => 1,
            ])->fetch();

            if ((int)$iblock['ID'] < 1) {
                throw new \RuntimeException("Iblock {$code} not found.");
            }

            $data[$code] = $iblock;
        }

        return $data[$code];
    }

    /**
     * @param string $code
     * @return int
     */
    public function getIdByCode(string $code): int
    {
        return (int)$this->getDataByCode($code)['ID'];
    }
}
