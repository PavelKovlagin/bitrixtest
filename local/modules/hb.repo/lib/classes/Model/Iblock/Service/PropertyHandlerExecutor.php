<?php

declare(strict_types=1);

namespace Repo\Model\Iblock\Service;

use Repo\Collection\Collection;
use Repo\Model\Iblock\Handler\Property;

class PropertyHandlerExecutor
{
    private Collection $handlers;

    public function __construct()
    {
        $this->handlers = (new Collection())
            ->push(new Property\Element\Handler())
            ->push(new Property\Enum\Handler())
            ->push(new Property\File\Handler())
            ->push(new Property\HtmlText\Handler());
    }

    /**
     * Метод выполняет хендлеры свойств элементов инфоблока.
     *
     * @param array $properties Массив свойств элементов инфоблока.
     * @return array
     */
    public function execute(array $properties): array
    {
        foreach ($this->handlers as $handler) {
            $properties = $handler->handle($properties);
        }

        return $properties;
    }
}
