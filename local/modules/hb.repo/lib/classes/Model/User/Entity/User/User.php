<?php

declare(strict_types=1);

namespace Repo\Model\User\Entity\User;

use Repo\Collection\CollectionInterface;
use Repo\Model\AbstractModel;

class User extends AbstractModel
{
    public const ID = 'ID';
    public const LOGIN = 'LOGIN';
    public const EMAIL = 'EMAIL';
    public const ACTIVE = 'ACTIVE';
    public const FIRST_NAME = 'NAME';
    public const SECOND_NAME = 'SECOND_NAME';
    public const LAST_NAME = 'LAST_NAME';
    public const FULL_NAME = 'FULL_NAME';
    public const PHONE = 'PERSONAL_MOBILE';
    public const STREET = 'PERSONAL_STREET';
    public const CITY = 'PERSONAL_CITY';
    public const REGION = 'PERSONAL_STATE';
    public const ZIP = 'PERSONAL_ZIP';

    private ?int $id = null;
    private ?string $login = null;
    private ?string $email = null;
    private ?string $active = null;
    private ?string $firstName = null;
    private ?string $secondName = null;
    private ?string $lastName = null;
    private ?string $fullName = null;
    private ?string $phone = null;
    private ?string $street = null;
    private ?string $city = null;
    private ?string $region = null;
    private ?string $zip = null;
    private ?array $group = [];
    private CollectionInterface $userFields;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        $user = new static();

        if (!empty($data['ID'])) {
            $user->setId((int)$data['ID']);
        }

        if (!empty($data['LOGIN'])) {
            $user->setLogin($data['LOGIN']);
        }

        if (!empty($data['EMAIL'])) {
            $user->setEmail($data['EMAIL']);
        }

        if (!empty($data['ACTIVE'])) {
            $user->setActive($data['ACTIVE']);
        }

        if (!empty($data['FIRST_NAME'])) {
            $user->setFirstName($data['FIRST_NAME']);
        }

        if (!empty($data['SECOND_NAME'])) {
            $user->setSecondName($data['SECOND_NAME']);
        }

        if (!empty($data['LAST_NAME'])) {
            $user->setLastName($data['LAST_NAME']);
        }

        if (!empty($data['FULL_NAME'])) {
            $user->setFullName($data['FULL_NAME']);
        }

        if (!empty($data['PHONE'])) {
            $user->setPhone($data['PHONE']);
        }

        if (!empty($data['STREET'])) {
            $user->setStreet($data['STREET']);
        }

        if (!empty($data['CITY'])) {
            $user->setCity($data['CITY']);
        }

        if (!empty($data['REGION'])) {
            $user->setRegion($data['REGION']);
        }

        if (!empty($data['ZIP'])) {
            $user->setZip($data['ZIP']);
        }

        if (!empty($data['USER_FIELDS'])) {
            $user->setUserFields($data['USER_FIELDS']);
        }

        return $user;
    }

    public function toArray(): array
    {
        $element = [];

        $element['ID'] = $this->getId();
        $element['LOGIN'] = $this->getLogin();
        $elemtnt['EMAIL'] = $this->getEmail();
        $element['FIRST_NAME'] = $this->getFirstName();
        $element['SECOND_NAME'] = $this->getSecondName();
        $element['LAST_NAME'] = $this->getLastName();  
        $element['FULL_NAME'] = $this->getFullName();   
        $element['PHONE'] = $this->getPhone();
        $element['STREET'] = $this->getStreet();
        $element['CITY'] = $this->getCity();
        $elememt['REGION'] = $this->getRegion();
        $element['ZIP'] = $this->getZip();
        $element['USER_FIELDS'] = $this->getUserFields(); 

        return $element;
    }

    /**
     * Устанавливает группы
     * 
     * @param array $group
     * 
     * @return [type]
     */
    public function setGroup(array $group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * Возвращает группы пользователя
     * 
     * @return array|null
     */
    public function getGroup(): ?array
    {
        return $this->group;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     * @return self
     */
    public function setLogin(?string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return self
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getActive(): ?string
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active === 'Y';
    }

    /**
     * @param string|null $active
     * @return $this
     */
    public function setActive(?string $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return self
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    /**
     * @param string|null $secondName
     * @return self
     */
    public function setSecondName(?string $secondName): self
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return self
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string|null $fullName
     * @return User
     */
    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return self
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return self
     */
    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return self
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     * @return self
     */
    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string|null $zip
     * @return self
     */
    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return CollectionInterface
     */
    public function getUserFields(): CollectionInterface
    {
        return $this->userFields;
    }

    /**
     * @param CollectionInterface $userFields
     * @return self
     */
    public function setUserFields(CollectionInterface $userFields): self
    {
        $this->userFields = $userFields;

        return $this;
    }
}
