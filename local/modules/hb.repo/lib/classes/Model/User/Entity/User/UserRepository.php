<?php

declare(strict_types=1);

namespace Repo\Model\User\Entity\User;

use Repo\Collection\Collection;
use Repo\Collection\CollectionInterface;
use Repo\Model\User\Service\UserChecker;
use Repo\Model\UserField\Entity\UserField\UserFieldRepository;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Engine\CurrentUser;
use Bitrix\Main\ObjectNotFoundException;

class UserRepository
{
    private UserFieldRepository $userFieldRepository;

    public function __construct(UserFieldRepository $userFieldRepository)
    {
        $this->userFieldRepository = $userFieldRepository;
    }

    /**
     * Добавляет группы пользователя
     * 
     * @param User $user
     * 
     * @return [type]
     */
    private function addGroup(User &$user)
    {
        if ($userId = $user->getId()) {
            $user->setGroup(\CUser::GetUserGroup($userId));
        }
    }

    /**
     * Метод для получения пользователей.
     *
     * @param array $parameters
     * @return CollectionInterface
     * @throws ArgumentException
     */
    public function get(array $parameters): CollectionInterface
    {
        if (!empty($parameters['select_user_fields']) && is_array($parameters['select_user_fields'])) {
            $selectUserFields = $parameters['select_user_fields'];
            unset($parameters['select_user_fields']);
        }

        $users = UserTable::getList($parameters);
        $usersArray = [];

        while ($user = $users->fetch()) {
            if (!empty($selectUserFields)) {
                $user['USER_FIELDS'] = $this->userFieldRepository->getByFields($selectUserFields);
            }
            $usersArray[$user['ID']] = $user;
        }

        $collection = new Collection();

        foreach ($usersArray as $user) {
            $collection[$user['ID']] = User::fromArray($user);
        }

        return $collection;
    }

    /**
     * Ищет пользователя по телефону
     * 
     * @param string $phone
     * 
     * @return User|null
     */
    public function getByPhone(string $phone): ?User
    {
        $user = $this->get([
            'filter' => [
                'PERSONAL_PHONE' => $phone,
            ],
            'select' => [
                'ID' => User::ID,
                'LOGIN' => User::LOGIN,
                'EMAIL' => User::EMAIL,
                'ACTIVE' => User::ACTIVE,
                'FIRST_NAME' => User::FIRST_NAME,
                'SECOND_NAME' => User::SECOND_NAME,
                'LAST_NAME' => User::LAST_NAME,
                'FULL_NAME' => USER::FULL_NAME,
                'PHONE' => User::PHONE,
                'STREET' => User::STREET,
                'CITY' => User::CITY,
                'REGION' => User::REGION,
                'ZIP' => User::ZIP,
            ],
            'select_user_fields' => [
                'UF_*',
            ],
        ])->first();

        if ($user === null) {
            return null;
        }

        return $user;
    }

    /**
     * Возвращает пользоателя по логину
     * 
     * @param string $login
     * 
     * @return User|null
     */
    public function getByLogin(string $login): ?User
    {
        $user = $this->get([
            'filter' => [
                'LOGIN' => $login,
            ],
            'select' => [
                'ID' => User::ID,
                'LOGIN' => User::LOGIN,
                'EMAIL' => User::EMAIL,
                'ACTIVE' => User::ACTIVE,
                'FIRST_NAME' => User::FIRST_NAME,
                'SECOND_NAME' => User::SECOND_NAME,
                'LAST_NAME' => User::LAST_NAME,
                'FULL_NAME' => USER::FULL_NAME,
                'PHONE' => User::PHONE,
                'STREET' => User::STREET,
                'CITY' => User::CITY,
                'REGION' => User::REGION,
                'ZIP' => User::ZIP,
            ],
            'select_user_fields' => [
                'UF_*',
            ],
        ])->first();

        if ($user === null) {
            return null;
        }

        return $user;
    }


    /**
     * Метод для получения профиля пользователя.
     *
     * @param int|null $id
     * @return User
     * @throws ArgumentException|ObjectNotFoundException
     */
    public function getForProfile(?int $id = null): ?User
    {
        if (($id === null) && !$id = CurrentUser::get()->getId()) {
            return null;
        }

        $user = $this->get([
            'filter' => [
                '=ID' => $id,
            ],
            'select' => [
                'ID' => User::ID,
                'LOGIN' => User::LOGIN,
                'EMAIL' => User::EMAIL,
                'ACTIVE' => User::ACTIVE,
                'FIRST_NAME' => User::FIRST_NAME,
                'SECOND_NAME' => User::SECOND_NAME,
                'LAST_NAME' => User::LAST_NAME,
                'FULL_NAME' => USER::FULL_NAME,
                'PHONE' => User::PHONE,
                'STREET' => User::STREET,
                'CITY' => User::CITY,
                'REGION' => User::REGION,
                'ZIP' => User::ZIP,
            ],
            'select_user_fields' => [
                'UF_*',
            ],
        ])->first();

        if ($user === null) {
            throw new ObjectNotFoundException('Пользователь не найден.');
        }

        $this->addGroup($user);

        return $user;
    }

    /**
     * Возвращает группы пользователя
     * 
     * @return User|null
     */
    public function getGroup(): ?User
    {
        global $USER;
        if (!UserChecker::checkAuthorized()) {
            return null;
        }

        $user = $this->get([
            'filter' => [
                '=ID' => $USER->getId(),
            ],
            'select' => [
                'ID' => User::ID,
            ]
        ])->first();

        if ($user === null) {
            throw new ObjectNotFoundException('Пользователь не найден.');
        }

        $this->addGroup($user);

        return $user;
    }

    /**
     * Метод регистрирует пользователя 
     * 
     * @param string $email
     * @param string $password
     * @param string $confirmPassword
     * @param string|null $phone
     * @param string|null $lastName
     * @param string|null $name
     * @param string|null $secondName
     * 
     * @return array|null
     */
    public function register(
        string $email, 
        string $password, 
        string $confirmPassword, 
        string $phone = null,
        string $lastName = null, 
        string $name = null, 
        string $secondName = null
    ): ?array 
    {
        $user = new \CUser;
        $arFields = array(
            'EMAIL' => $email,
            'PERSONAL_PHONE' => $phone,
            'LAST_NAME' => $lastName,
            'NAME' => $name,
            'SECOND_NAME' => $secondName,
            'LOGIN' => $email,
            'ACTIVE' => 'Y',
            'PASSWORD' => $password,
            'CONFIRM_PASSWORD'  => $confirmPassword,
        );

        $id = $user->Add($arFields);
        if ((int)$id > 0) {
            $user->Login($email, $password);
            return [
                'id' => (int)$id,
                'success' => true,
                'message' => 'Пользователь успешно добавлен',
            ];
        } else {
            return [
                'success' => false,
                'message' => $user->LAST_ERROR,
            ];
        }
    }
}
