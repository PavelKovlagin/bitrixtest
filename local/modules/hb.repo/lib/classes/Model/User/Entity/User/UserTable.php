<?php

declare(strict_types=1);

namespace Repo\Model\User\Entity\User;

use Bitrix\Main;

class UserTable extends Main\UserTable
{
    public static function getMap(): array
    {
        $map = parent::getMap();

        $map['FULL_NAME'] = new Main\Entity\ExpressionField(
            'FULL_NAME',
            'CONCAT_WS(" ", %1$s, %2$s, %3$s)',
            ['LAST_NAME', 'NAME', 'SECOND_NAME']
        );

        return $map;
    }
}