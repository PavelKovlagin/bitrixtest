<?php

declare(strict_types=1);

namespace Repo\Model\User\Handler\Login;

class Command
{
    public string $email;
    public string $password;
    public bool $remember;

    public function __construct(string $email, string $password, bool $remember)
    {
        $this->email = $email;
        $this->password = $password;
        $this->remember = $remember;
    }
}
