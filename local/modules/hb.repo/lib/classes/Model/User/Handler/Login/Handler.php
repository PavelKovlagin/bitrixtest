<?php

declare(strict_types=1);

namespace Repo\Model\User\Handler\Login;

use Bitrix\Main\Engine\CurrentUser;

class Handler
{
    /**
     * Хендлер авторизации пользователя.
     *
     * @param Command $command
     * @return array|bool[]|false[]
     */
    public function handle(Command $command): array
    {
        global $USER;

        $result = $USER->Login($command->email, $command->password, $command->remember ? 'Y' : 'N');

        if ($result === true && $currentUser = CurrentUser::get()) {
            return [
                'success' => true,
                'user' => $currentUser->getFirstName() ?: $currentUser->getLogin(),
            ];
        }

        if ($result['MESSAGE']) {
            return [
                'success' => false,
                'message' => 'Неверный логин или пароль.',
            ];
        }

        return [
            'success' => false,
            'message' => 'Не удалось авторизоваться.',
        ];
    }
}
