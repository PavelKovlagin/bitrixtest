<?php

declare(strict_types=1);

namespace Repo\Model\User\Handler\Register;

class Command
{
    public ?string $fio = null;
    public ?string $phone = null;
    public ?string $email = null;
    public ?string $password = null;
    public ?string $confirmPassword = null;

    public function __construct(array $request)
    {
        $this->fio = $request['fio'];
        $this->phone = $request['phone'];
        $this->email = $request['email'];
        $this->password = $request['password'];
        $this->confirmPassword = $request['confirm_password'];
    }
}
