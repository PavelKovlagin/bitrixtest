<?php

declare(strict_types=1);

namespace Repo\Model\User\Handler\Register;

use Repo\Model\User\Entity\User\UserRepository;

class Handler
{

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Хендлер регисрации пользователя.
     *
     * @param Command $command
     * @return array|bool[]|false[]
     */
    public function handle(Command $command): array
    {
        if (!($email = $command->email)) {
            return [
                'success' => false,
                'message' => 'Не заполнен email',
            ];
        }

        if (($password = $command->password) !== ($confirmPassword =  $command->confirmPassword)) {
            return [
                'success' => false,
                'message' => 'Пароли не совпадают',
            ];
        }

        if ($fio = $command->fio) {
            $arFio = explode(' ', trim($fio));
            switch (count($arFio)) {
                case 1:
                    $name = $arFio[0];
                break;
                case 2:
                    $name = $arFio[0];
                    $secondName = $arFio[1];
                break;
                case 3:
                    $lastName = $arFio[0];
                    $name = $arFio[1];
                    $secondName = $arFio[2];
                break;
            }
        }

        $res = $this->userRepository->register(
            $email,
            $password,
            $confirmPassword,
            null,
            $lastName,
            $name,
            $secondName,            
        );
        if ($res['success']) {
            global $USER;
            $USER->Login($email, $password);
        }
        
        return $res;
        
    }
}
