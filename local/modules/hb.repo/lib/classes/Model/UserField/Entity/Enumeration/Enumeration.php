<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\Enumeration;

use Repo\Model\AbstractModel;

class Enumeration extends AbstractModel
{
    private ?int $id = null;
    private ?int $fieldId = null;
    private ?string $value = null;
    private ?string $def = null;
    private ?int $sort = 500;
    private ?string $xmlId = null;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): Enumeration
    {
        $enumeration = new static();

        if (!empty($data['ID'])) {
            $enumeration->setId((int)$data['ID']);
        }

        if (!empty($data['USER_FIELD_ID'])) {
            $enumeration->setFieldId((int)$data['USER_FIELD_ID']);
        }

        if (!empty($data['VALUE'])) {
            $enumeration->setValue($data['VALUE']);
        }

        if (!empty($data['DEF'])) {
            $enumeration->setDef($data['DEF']);
        }

        if (!empty($data['SORT'])) {
            $enumeration->setSort((int)$data['SORT']);
        }

        if (!empty($data['XML_ID'])) {
            $enumeration->setXmlId($data['XML_ID']);
        }

        return $enumeration;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            'ID' => $this->getId(),
            'USER_FIELD_ID' => $this->getFieldId(),
            'VALUE' => $this->getValue(),
            'DEF' => $this->getDef(),
            'SORT' => $this->getSort(),
            'XML_ID' => $this->getXmlId(),
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getFieldId(): ?int
    {
        return $this->fieldId;
    }

    /**
     * @param int|null $fieldId
     * @return self
     */
    public function setFieldId(?int $fieldId): self
    {
        $this->fieldId = $fieldId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return self
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDef(): ?string
    {
        return $this->def;
    }

    /**
     * @param string|null $def
     * @return self
     */
    public function setDef(?string $def): self
    {
        $this->def = $def;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }

    /**
     * @param int|null $sort
     * @return self
     */
    public function setSort(?int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getXmlId(): ?string
    {
        return $this->xmlId;
    }

    /**
     * @param string|null $xmlId
     * @return self
     */
    public function setXmlId(?string $xmlId): self
    {
        $this->xmlId = $xmlId;

        return $this;
    }
}
