<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\Enumeration;

use Repo\Collection\Collection;

class EnumerationRepository
{
    public function get(array $parameters): Collection
    {
        $enumerations = EnumerationTable::getList($parameters);
        $enumerationsArray = [];

        while ($enumeration = $enumerations->fetch()) {
            $enumerationsArray[$enumeration['ID']] = $enumeration;
        }

        $collection = new Collection();

        foreach ($enumerationsArray as $enumeration) {
            $collection[$enumeration['ID']] = Enumeration::fromArray($enumeration);
        }

        return $collection;
    }

    public function getByIds(array $ids): Collection
    {
        return $this->get([
            'filter' => [
                '=USER_FIELD_ID' => $ids,
            ],
            'order' => [
                'sort' => 'asc',
                'id' => 'asc',
            ],
        ]);
    }
}
