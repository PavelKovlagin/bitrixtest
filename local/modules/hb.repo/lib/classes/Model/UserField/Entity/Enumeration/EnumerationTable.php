<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\Enumeration;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;

class EnumerationTable extends DataManager
{
    public static function getTableName(): string
    {
        return 'b_user_field_enum';
    }

    public static function getMap(): array
    {
        return [
            new IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new IntegerField('USER_FIELD_ID'),
            new StringField('VALUE'),
            new StringField('DEF'),
            new IntegerField('SORT'),
            new StringField('XML_ID'),
        ];
    }
}
