<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\UserField;

use Repo\Collection\CollectionInterface;

class EnumerationUserField extends UserField
{
    private CollectionInterface $enumerations;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        /** @var static $userField */
        $userField = parent::fromArray($data);

        if (!empty($data['ENUMERATIONS'])) {
            $userField->setEnumerations($data['ENUMERATIONS']);
        }

        return $userField;
    }

    public function toArray(): array
    {
        $data = parent::toArray();
        $data['ENUMERATIONS'] = $this->getEnumerations();

        return $data;
    }

    public function getEnumerations(): CollectionInterface
    {
        return $this->enumerations;
    }

    public function setEnumerations(CollectionInterface $enumerations): self
    {
        $this->enumerations = $enumerations;

        return $this;
    }
}
