<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\UserField;

use Repo\Model\AbstractModel;

class UserField extends AbstractModel
{
    private ?string $type = null;
    private ?string $name = null;
    private ?string $title = null;
    private ?string $placeholder = null;
    private ?string $multiple = 'N';
    private $value;

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $data): self
    {
        $userField = new static();

        if (!empty($data['USER_TYPE_ID'])) {
            $userField->setType($data['USER_TYPE_ID']);
        }

        if (!empty($data['FIELD_NAME'])) {
            $userField->setName($data['FIELD_NAME']);
        }

        if (!empty($data['TITLE'])) {
            $userField->setTitle($data['TITLE']);
        }

        if (!empty($data['PLACEHOLDER'])) {
            $userField->setPlaceholder($data['PLACEHOLDER']);
        }

        if (!empty($data['VALUE'])) {
            $userField->setValue($data['VALUE']);
        }

        return $userField;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return [
            'TYPE' => $this->getType(),
            'NAME' => $this->getName(),
            'TITLE' => $this->getTitle(),
            'PLACEHOLDER' => $this->getPlaceholder(),
            'MULTIPLE' => $this->getMultiple() === 'Y' ?? true,
            'VALUE' => $this->getValue(),
        ];
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceholder(): ?string
    {
        return $this->placeholder;
    }

    /**
     * @param string $placeholder
     * @return self
     */
    public function setPlaceholder(string $placeholder): self
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * @return string
     */
    public function getMultiple(): string
    {
        return $this->multiple;
    }

    /**
     * @param string $multiple
     * @return self
     */
    public function setMultiple(string $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return self
     */
    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }
}
