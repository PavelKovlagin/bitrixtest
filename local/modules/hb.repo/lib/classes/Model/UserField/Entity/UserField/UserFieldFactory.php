<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\UserField;

class UserFieldFactory
{
    public static function fromArray(array $data, string $type = null)
    {
        if ($type === 'enumeration') {
            return static::createEnumerationUserField($data);
        }

        return static::createSimpleUserField($data);
    }

    public static function createEnumerationUserField(array $data): EnumerationUserField
    {
        return EnumerationUserField::fromArray($data);
    }

    public static function createSimpleUserField(array $data): UserField
    {
        return UserField::fromArray($data);
    }
}
