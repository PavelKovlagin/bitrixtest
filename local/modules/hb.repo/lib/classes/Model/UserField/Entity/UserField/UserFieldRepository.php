<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\UserField;

use Repo\Collection\Collection;
use Repo\Collection\CollectionInterface;
use Repo\Model\UserField\Entity\Enumeration\EnumerationRepository;
use Bitrix\Main\ArgumentException;

class UserFieldRepository
{
    private EnumerationRepository $enumerationRepository;

    public function __construct(EnumerationRepository $enumerationRepository)
    {
        $this->enumerationRepository = $enumerationRepository;
    }

    /**
     * @param array $parameters
     * @return Collection
     * @throws ArgumentException
     */
    public function get(array $parameters): CollectionInterface
    {
        $userFields = UserFieldTable::getList($parameters);
        $userFieldsArray = [];
        $userFieldsEnumerationsIds = [];

        while ($userField = $userFields->fetch()) {
            $userFieldsArray[$userField['ID']] = $userField;

            if ($userField['USER_TYPE_ID'] === 'enumeration') {
                $userFieldsEnumerationsIds[] = $userField['ID'];
            }
        }

        if ($userFieldsEnumerationsIds) {
            $enumerations = $this->enumerationRepository->getByIds($userFieldsEnumerationsIds);

            foreach ($enumerations as $enumeration) {
                if (!isset($userFieldsArray[$enumeration->getFieldId()]['ENUMERATIONS'])) {
                    $userFieldsArray[$enumeration->getFieldId()]['ENUMERATIONS'] = (new Collection())->push($enumeration);
                } else {
                    $userFieldsArray[$enumeration->getFieldId()]['ENUMERATIONS']->push($enumeration);
                }
            }
        }

        $collection = new Collection();

        foreach ($userFieldsArray as $userField) {
            $collection[$userField['ID']] = UserFieldFactory::fromArray($userField, $userField['USER_TYPE_ID']);
        }

        return $collection;
    }

    /**
     * @param array $fields
     * @return CollectionInterface
     * @throws ArgumentException
     */
    public function getByFields(array $fields): CollectionInterface
    {
        return $this->get([
            'filter' => [
                '=LANG.LANGUAGE_ID' => 'ru',
                '=ENTITY_ID' => 'USER',
                '=FIELD_NAME' => $fields,
            ],
            'select' => [
                'ID',
                'ENTITY_ID',
                'FIELD_NAME',
                'USER_TYPE_ID',
                'MULTIPLE',
                'MANDATORY',
                'SORT',
                'TITLE' => 'LANG.EDIT_FORM_LABEL',
            ],
            'order' => [
                'SORT' => 'ASC',
                'ID' => 'ASC',
            ],
        ]);
    }
}
