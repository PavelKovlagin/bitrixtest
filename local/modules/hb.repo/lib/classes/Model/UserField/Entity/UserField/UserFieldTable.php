<?php

declare(strict_types=1);

namespace Repo\Model\UserField\Entity\UserField;

use Bitrix\Main;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\UserFieldLangTable;

class UserFieldTable extends Main\UserFieldTable
{
    public static function getTableName(): string
    {
        return 'b_user_field';
    }

    public static function getMap()
    {
        $map = parent::getMap();

        $map['LANG'] = new ReferenceField(
            'LANG',
            UserFieldLangTable::class,
            ['=this.ID' => 'ref.USER_FIELD_ID'],
            ['join_type' => 'INNER']
        );

        return $map;
    }
}
