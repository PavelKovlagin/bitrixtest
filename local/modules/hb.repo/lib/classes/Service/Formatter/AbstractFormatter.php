<?php

declare(strict_types=1);

namespace Repo\Service\Formatter;

use Bitrix\Main\ArgumentException;

abstract class AbstractFormatter
{
    private $value;
    protected array $options = [];

    /**
     * Конструктор.
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = $options;
    }

    /**
     * Метод возвращает объект форматирования.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Метод устанавливает объект форматирования.
     *
     * @param mixed $value
     * @return self
     * @throws ArgumentException
     */
    public function setValue($value): self
    {
        if (($type = $this->getTypeObject()) && !($value instanceof $type)) {
            throw new ArgumentException('Не валидный тип входящего объекта');
        }

        $this->value = $value;

        return $this;
    }

    /**
     * Метод форматирует данные.
     *
     * @param $value
     * @return mixed
     * @throws ArgumentException
     */
    final public function format($value)
    {
        return $this->setValue($value)->getFormatted();
    }

    /**
     * Метод создает класс форматтера.
     *
     * @param $value
     * @param array $options
     * @return static
     * @throws ArgumentException
     */
    public static function create($value, array $options = []): AbstractFormatter
    {
        $object = new static($options);
        $object->setValue($value);

        return $object;
    }

    /**
     * Метод формирует класс форматтера и производит форматирование данных.
     *
     * @param $value
     * @param array $options
     * @return mixed
     * @throws ArgumentException
     */
    public function createFormatted($value, array $options = [])
    {
        return static::create($value, $options)->getFormatted();
    }

    /**
     * Метод возвращает тип объекта, который необходимо форматировать.
     *
     * @return mixed
     */
    public function getTypeObject()
    {
        return null;
    }
}
