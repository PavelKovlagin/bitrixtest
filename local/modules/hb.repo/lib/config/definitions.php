<?php
/**
 * DI definitions.
 */

return [
    'CollectionInterface' => DI\create(Repo\Collection\Collection::class),
    'ElementRepository' => DI\autowire(Repo\Model\Iblock\Entity\Element\ElementRepository::class),
    'PropertyRepository' => DI\autowire(Repo\Model\Iblock\Entity\Property\PropertyRepository::class),
    'SectionRepository' => DI\autowire(Repo\Model\Iblock\Entity\Section\SectionRepository::class),
    'UserRepository' => DI\autowire(Repo\Model\User\Entity\User\UserRepository::class),
];
