<?
IncludeModuleLangFile(__FILE__);

Class hb_site extends CModule
{
    var $MODULE_ID = "hb.site";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $errors;

    function __construct()
    {
        $this->MODULE_VERSION = "1.0.0";
        $this->MODULE_VERSION_DATE = "2018-06-11";
        $this->MODULE_NAME = "HB модуль для сайта";
        $this->MODULE_DESCRIPTION = "Модуль для сайта";
    }

    function DoInstall()
    {
        global $APPLICATION;
        
        \Bitrix\Main\ModuleManager::RegisterModule($this->MODULE_ID);

        //Регистрация событий
        // RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd","hb.site", "HB\\handlerevent\\EntityCatalogEvent","changeIblockElement");


		$APPLICATION->IncludeAdminFile("Установка модуля ", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/step.php");
        return true;
    }

    function DoUninstall()
    {
		global $APPLICATION;
		
        \Bitrix\Main\ModuleManager::UnRegisterModule($this->MODULE_ID);
        
        //Отвязка от событий
        // UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd","hb.site", "HB\\handlerevent\\EntityCatalogEvent","changeIblockElement");

		$APPLICATION->IncludeAdminFile("Деинсталляция модуля " . $this->MODULE_ID . "", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/unstep.php");
        return true;
    }
}