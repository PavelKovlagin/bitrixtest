<?
namespace HB\Calculator;

class Calculator{
    
    /**
     * Возвращает сумму x и y
     * 
     * @param int $x
     * @param int $y
     * 
     * @return int|null
     */
    public static function sum(int $x, int $y): ?int
    {
        return $x + $y;
    }
}
?>