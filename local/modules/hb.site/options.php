<?
if(!$USER->IsAdmin())
    return;

preg_match("/modules\/([a-zA-Z0-9_\.]+)\//", __FILE__, $find_module_id);
$moduleId = $find_module_id[1];

if (CModule::IncludeModule("hb.site") && isset($_GET['x']) && isset($_GET['y'])) {
    $result = \HB\Calculator\Calculator::sum((int)$_GET['x'], (int)$_GET['y']);
}
?>
<h1><?= $result ? 'X + Y = ' . $result : 'Введите  X и Y' ?></h1>
<form>
<input type='hidden' name='lang' value='ru'>
<input type='hidden' name='mid' value='hb.site'>
<input type='hidden' name='mid_menu' value='1'>
<p>X = <input type='text' name='x'></p>
<p>Y = <input type='text' name='y'></p>
<button type='submit'>Calculate</button>
</form>