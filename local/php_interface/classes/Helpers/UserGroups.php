<?php

namespace App\Helpers;

class UserGroups
{
    /**
     * Возвращает идентификатор группы по коду
     * 
     * @param string $code
     * 
     * @return int|null
     */
    public static function getGroupIdByCode(string $code)
    {
        $rsGroups = \CGroup::GetList(
            $by = "c_sort",
            $order = "asc",
            [
                "STRING_ID" => $code
            ]
        );
        $group = $rsGroups->Fetch();
        if ($id = $group['ID']) {
            return $id;
        }
        return null;
    }
}