<?php

namespace App\Helpers;

class Users
{
    /**
     * Возвращает пользователя по идентификатору группы и логину
     * 
     * @param string $login
     * @param int $groupId
     * 
     * @return array|null
     */
    public static function getUserByGroup(string $login, int $groupId): ?array
    {
        $rsUsers = \CUser::GetList(
            ($by = "personal_country"),
            ($order = "desc"),
            [
                'LOGIN_EQUAL' => $login,
                'GROUPS_ID' => [
                    $groupId
                ]
            ],
            [
                'SELECT' => [
                    'UF_*'
                ]
            ]
        );
        if ($user = $rsUsers->Fetch()) {
            return $user;
        }
        return null;;
    }

    /**
     * Возвращает список пользователей по идентификатору группы
     * 
     * @param int $groupId
     * 
     * @return array|null
     */
    public static function getUsersByGroup(int $groupId): ?array
    {
        $usersArray = [];
        $rsUsers = \CUser::GetList(
            ($by = "personal_country"),
            ($order = "desc"),
            [
                'GROUPS_ID' => [
                    $groupId
                ]
            ],
            [
                'SELECT' => [
                    'UF_*'
                ]
            ]
        );
        while ($user = $rsUsers->Fetch()) {
            $usersArray[] = $user;
        }
        return $usersArray;
    }

    /**
     * Устанавливает новую строку избранных товаров для пользовтеля
     * 
     * @param int $userId
     * @param string $favorites
     * 
     * @return [type]
     */
    public static function updateFavoritesForUser(int $userId, string $favorites)
    {
        $user = new \CUser;
        $user->Update(
            $userId, 
            [
                "UF_FAVORITES" => $favorites
            ]
        );
    }

    /**
     * Удаление избранных товаров для пользователя
     * 
     * @param int $userId
     * 
     * @return [type]
     */
    public static function removeFavoritesForUser(int $userId)
    {
        self::updateFavoritesForUser($userId, '');
    }

    /**
     * Устанавливает новую строку избранных аптек для пользовтеля
     * 
     * @param int $userId
     * @param string $pharmFavorites
     * 
     * @return [type]
     */
    public static function updatePharmFavoritesForUser(int $userId, string $pharmFavorites)
    {
        $user = new \CUser;
        $user->Update(
            $userId, 
            [
                "UF_PHARM_FAVORITES" => $pharmFavorites
            ]
        );
    }

    /**
     * Удаление избранных аптек для пользователя
     * 
     * @param int $userId
     * 
     * @return [type]
     */
    public static function removePharmFavoritesForUser(int $userId)
    {
        self::updatePharmFavoritesForUser($userId, '');
    }

    /**
     * Возвращает идентификаторы пользователей
     * 
     * @param array $users
     * 
     * @return [type]
     */
    public static function getUsersIds(array $users)
    {
        $ids = [];
        foreach ($users as $user) {
            $ids[] = $user['ID'];
        }
        return $ids;
    }
}
