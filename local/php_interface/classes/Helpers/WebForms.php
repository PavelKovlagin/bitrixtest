<?php

namespace App\Helpers;

use \Bitrix\Main\Loader;

Loader::includeModule("form");

class WebForms
{
    /**
     * Возвращает информацию о веб форме по символьному коду
     * 
     * @param string $code
     * 
     * @return [type]
     */
    public static function getByCode(string $code)
    {
        $rsForm = \CForm::GetBySID($code);
        $arForm = $rsForm->Fetch();
        return $arForm;
    }

    /**
     * Возвращает результаты по идентификатору веб-формы и фильтру 
     * 
     * @param int $formId
     * @param array $fields => [
     *      [
     *          'CODE' - символьный код вопроса,
     *          'VALUE' - значение?
     *          'FILTER_TYPE' - тип значениея,
     *          'EXACT_MATCH' - Y - точное совпадение, N - вхождение
     *      ]
     * ]
     * 
     * @return [type]
     */
    public static function getResultsByReviewValue(int $formId, array $fields)
    {
        $arRes = [];
        $result = \CFormResult::GetList(
            $formId,
            ($by = "s_timestamp"),
            ($order = "desc"),
            [
                'FIELDS' => $fields
            ]
        );
        while ($res = $result->Fetch()) {
            $arRes[$res['ID']] = $res;
        }
        return $arRes;
    }
}
