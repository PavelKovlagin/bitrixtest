<?php

namespace App\Models\Manager\Entity;

use Repo\DI\Container;
use Repo\Model\Iblock\Entity\Element\ElementRepository;
use Repo\Collection\CollectionInterface;
use Repo\Model\User\Entity\User\UserRepository;

class ManagerRepository extends ElementRepository
{
    /**
     * Добавляет пользователя
     * 
     * @param Manager $manager
     * 
     * @return [type]
     */
    private function addUser(Manager &$manager)
    {
        if ($id = $manager->getUserId()) {
            $user = Container::get(UserRepository::class)->getForProfile($id);
            $manager->setUser($user);
        }
    }

    public function get(array $parameters): CollectionInterface
    {
        $elementsArray = $this->getArray($parameters);

        $collection = new ManagerCollection();

        foreach ($elementsArray as $element) {
            $collection[$element['ID']] = Manager::fromArray($element);
        }

        return $collection;
    }

    /**
     * Возвращает всех менеджеров
     * 
     * @return [type]
     */
    public function getAll(): CollectionInterface
    {
        $collection = $this->get([
            'select' => [
                '*'
            ],
            'select_properties' => [
                'USER'
            ],
            'filter' => [
                'IBLOCK_ID' => $this->iblockHelper->getIdByCode('managers')
            ]
        ]);

        foreach ($collection as $item) {
            $this->addUser($item);
        }

        return $collection;
    }

    /**
     * Возвращает менеджера по идентификатору
     * 
     * @param int $id
     * 
     * @return CollectionInterface
     */
    public function getById(int $id): CollectionInterface
    {
        $collection = $this->get([
            'select' => [
                '*'
            ],
            'select_properties' => [
                'USER'
            ],
            'filter' => [
                'ID' => $id,
                'IBLOCK_ID' => $this->iblockHelper->getIdByCode('managers')
            ]
        ]);

        foreach ($collection as $item) {
            $this->addUser($item);
        }

        return $collection;
    }

    /**
     * Возвращает менеджеров по идентификаторам
     * 
     * @param array $ids
     * 
     * @return CollectionInterface
     */
    public function getByIds(array $ids): CollectionInterface
    {
        $collection = $this->get([
            'select' => [
                '*'
            ],
            'select_properties' => [
                'USER'
            ],
            'filter' => [
                '=ID' => $ids,
                'IBLOCK_ID' => $this->iblockHelper->getIdByCode('managers')
            ]
        ]);

        foreach ($collection as $item) {
            $this->addUser($item);
        }

        return $collection;
    }
}