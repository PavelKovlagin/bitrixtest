<?php

namespace App\Models\Program\Entity;

use App\Models\Manager\Entity\ManagerCollection;
use App\Models\Volunteer\Entity\VolunteerCollection;
use Repo\Model\Iblock\Entity\Element\Element;

class Program extends Element
{
    private ?VolunteerCollection $volunteers = null;
    private ?ManagerCollection $managers = null;

    public function toArray(): array
    {
        $element = parent::toArray();

        return $element;
    }

    /**
     * Возвращает идентификаторы волонтеров
     * 
     * @return array|null
     */
    public function getVolunteersIds(): ?array
    {
        if ($volunteers = $this->getPropertyByCode('VOLUNTEERS')) {
            $array = [];
            foreach ($volunteers->getValues() as $volunteer) {
                $array[] = $volunteer->getId();
            }
            return $array;
        }
        return null;
    }

    /**
     * Устанавливает волонтеров
     * 
     * @param VolunteerCollection $volunteers
     * 
     * @return self
     */
    public function setVolunteers(VolunteerCollection $volunteers): self
    {
        $this->volunteers = $volunteers;

        return $this;
    }

    /**
     * Возвращает волонтеров
     * 
     * @return VolunteerCollection|null
     */
    public function getVolunteers(): ?VolunteerCollection
    {
        return $this->volunteers;
    }

    /**
     * Возвращает идентификаторы руководителей
     * 
     * @return array|null
     */
    public function getManagersIds(): ?array
    {
        if ($managers = $this->getPropertyByCode('MANAGERS')) {
            $array = [];
            foreach ($managers->getValues() as $manager) {
                $array[] = $manager->getId();
            }
            return $array;
        }
        return null;
    }

    /**
     * Устанавливает руководителей
     * 
     * @param ManagerCollection $managers
     * 
     * @return self
     */
    public function setManagers(ManagerCollection $managers): self
    {
        $this->managers = $managers;

        return $this;
    }

    /**
     * Возвращает руководителей
     * 
     * @return ManagerCollection|null
     */
    public function getManagers(): ?ManagerCollection
    {
        return $this->managers;
    }
}