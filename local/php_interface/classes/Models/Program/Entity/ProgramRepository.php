<?php

namespace App\Models\Program\Entity;

use App\Models\Manager\Entity\ManagerRepository;
use App\Models\Volunteer\Entity\VolunteerRepository;
use Repo\Model\Iblock\Entity\Element\ElementRepository;
use Repo\Collection\CollectionInterface;

class ProgramRepository extends ElementRepository
{
    private function addVolunteers(Program &$item)
    {
        if ($ids = $item->getVolunteersIds()) {
            $volunteers = (new VolunteerRepository())->getByIds($ids);
            $item->setVolunteers($volunteers);
        }
    }

    private function addManagers(Program $item) {
        if ($ids = $item->getManagersIds()) {
            $managers = (new ManagerRepository())->getByIds($ids);
            $item->setManagers($managers);
        }
    }

    public function get(array $parameters): CollectionInterface
    {
        $elementsArray = $this->getArray($parameters);

        $collection = new ProgramCollection();

        foreach ($elementsArray as $element) {
            $collection[$element['ID']] = Program::fromArray($element);
        }

        return $collection;
    }

    /**
     * Возвращает первого
     * 
     * @return [type]
     */
    public function getAll(): ProgramCollection
    {
        $collection = $this->get([
            'select' => [
                '*'
            ],
            'select_properties' => [
                'VOLUNTEERS',
                'MANAGERS'
            ],
            'filter' => [
                'IBLOCK_ID' => $this->iblockHelper->getIdByCode('programs')
            ]
        ]);

        foreach ($collection as $item) {
            $this->addVolunteers($item);
            $this->addManagers($item);
        }

        return $collection;
    }
}