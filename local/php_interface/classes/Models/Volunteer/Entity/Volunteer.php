<?php

namespace App\Models\Volunteer\Entity;

use Repo\Model\Iblock\Entity\Element\Element;
use Repo\Model\User\Entity\User\User;

class Volunteer extends Element
{
    private ?User $user = null;

    public function toArray(): array
    {
        $element = parent::toArray();

        $element['USER_ID'] = $this->getUserId();

        return $element;
    }

    public function getUserId(): ?int
    {
        if ($userId = $this->getPropertyByCode('USER')) {
            return $userId->getValue();
        }
        return null;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }
}