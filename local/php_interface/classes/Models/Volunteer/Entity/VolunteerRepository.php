<?php

namespace App\Models\Volunteer\Entity;

use Repo\Model\Iblock\Entity\Element\ElementRepository;
use Repo\Collection\CollectionInterface;
use Repo\Model\User\Entity\User\UserRepository;

class VolunteerRepository extends ElementRepository
{
    /**
     * Добавляет пользователя
     * 
     * @param Volunteer $volunteer
     * 
     * @return [type]
     */
    private function addUser(Volunteer &$volunteer)
    {
        if ($id = $volunteer->getUserId()) {
            $user = (new UserRepository())->getForProfile($id);
            $volunteer->setUser($user);
        }
    }

    public function get(array $parameters): CollectionInterface
    {
        $elementsArray = $this->getArray($parameters);

        $collection = new VolunteerCollection();

        foreach ($elementsArray as $element) {
            $collection[$element['ID']] = Volunteer::fromArray($element);
        }

        return $collection;
    }

    /**
     * Возвращает всех волонтеров
     * 
     * @return [type]
     */
    public function getAll(): CollectionInterface
    {
        $collection = $this->get([
            'select' => [
                '*'
            ],
            'select_properties' => [
                'USER'
            ],
            'filter' => [
                'IBLOCK_ID' => $this->iblockHelper->getIdByCode('volunteers')
            ]
        ]);

        foreach ($collection as $item) {
            $this->addUser($item);
        }

        return $collection;
    }

    /**
     * Возвращает волонтеры по идентификатору
     * 
     * @param int $id
     * 
     * @return VolunteerCollection
     */
    public function getById(int $id): CollectionInterface
    {
        $collection = $this->get([
            'select' => [
                '*'
            ],
            'select_properties' => [
                'USER'
            ],
            'filter' => [
                '=ID' => $id,
                'IBLOCK_ID' => $this->iblockHelper->getIdByCode('volunteers')
            ],
            'limit' => 1
        ]);

        foreach ($collection as $item) {
            $this->addUser($item);
        }

        return $collection;
    }

    /**
     * Возвращает волонтеров по идентификаторам
     * 
     * @param array $ids
     * 
     * @return VolunteerCollection
     */
    public function getByIds(array $ids): CollectionInterface
    {
        $collection = $this->get([
            'select' => [
                '*'
            ],
            'select_properties' => [
                'USER'
            ],
            'filter' => [
                '=ID' => $ids,
                'IBLOCK_ID' => $this->iblockHelper->getIdByCode('volunteers')
            ],
            'limit' => 1
        ]);

        foreach ($collection as $item) {
            $this->addUser($item);
        }

        return $collection;
    }
}