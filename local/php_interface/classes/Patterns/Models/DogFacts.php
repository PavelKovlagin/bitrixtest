<?php

namespace App\Patterns\Models;

use App\Exceptions\MyException;

class DogFacts
{
    private ?array $fact = null;

    /**
     * Возвращает факт
     * 
     * @return array|null
     */
    public function get(): ?array
    {
        if (!$this->fact) {
            $this->fact = json_decode(file_get_contents('https://some-random-api.ml/facts/dog'), true);
        }
        if ((string)strlen($this->fact['fact']) < 60) {
            throw new MyException('Нужно больше букоф');
        }
        
        return $this->fact;
    }
}