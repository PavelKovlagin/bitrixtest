<?php

namespace App\Patterns\Models;

use App\Patterns\Patterns\Factory;

class ElementFromArray extends Factory
{
    public function __construct(array $data)
    {
        if ($data['ID']) {
            $this->setId((int)$data['ID']);
        }

        if ($data['NAME']) {
            $this->setName((string)$data['NAME']);
        }
        
        if ($data['DESCRIPTION']) {
            $this->setDescription((string)$data['DESCRIPTION']);
        }
    }
}