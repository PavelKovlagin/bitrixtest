<?php

namespace App\Patterns\Models;

use App\Patterns\Patterns\Factory;

class ElementsContainer
{
    private array $elements = [];

    public function addElement(Factory $factory): self
    {
        $this->elements[$factory->getId()] = $factory;

        return $this;
    }

    public function getElements(): ?array
    {
        return $this->elements;
    }
}