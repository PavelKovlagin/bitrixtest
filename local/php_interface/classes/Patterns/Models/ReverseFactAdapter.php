<?php

namespace App\Patterns\Models;

class ReverseFactAdapter
{
    private ?DogFacts $dogFacts = null;
    
    public function __construct(DogFacts $dogFacts)
    {
        $this->dogFacts = $dogFacts;
    }

    /**
     * Возвращает обратный факт
     * 
     * @return string|null
     */
    public function get(): ?string
    {
        $reverse = '';
        $fact = $this->dogFacts->get()['fact'];
        for($i = strlen($fact) - 1; $i >= 0; $i--) {
            $reverse .= $fact[$i];
         }
        return $reverse;
    }
}