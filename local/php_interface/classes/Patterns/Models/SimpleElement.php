<?php

namespace App\Patterns\Models;

use App\Patterns\Patterns\Factory;

class SimpleElement extends Factory
{
    public function __construct(int $id, string $name, string $description)
    {
        $this->setId($id);
        $this->setName($name);
        $this->setDescription($description);
    }
}