<?php

namespace App\Patterns\Models;

use App\Patterns\Patterns\Singleton;

class SingleUser extends Singleton
{
    private ?string $name = null;

    public function getName(): ?string
    {
        return (string)$this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}