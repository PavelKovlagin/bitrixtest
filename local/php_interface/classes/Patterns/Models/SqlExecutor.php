<?php

namespace App\Patterns\Models;

use SplObserver;

class SqlExecutor implements \SplSubject
{
    private $observers = [];

    public function __construct()
    {
        $this->observers['*'] = [];       
    }   

    /**
     * Инициализация подписчиков
     * 
     * @param string $event - событие
     * 
     * @return void
     */
    private function initEventGroup(string $event = '*'): void
    {
        if (!isset($this->observers[$event])) {
            $this->observers[$event] = [];
        }
    }

    /**
     * Получение подписчиков
     * 
     * @param string $event - событие
     * 
     * @return array
     */
    private function getEventObservers(string $event = '*'): array
    {
        $this->initEventGroup($event);
        $group = $this->observers[$event];
        $all = $this->observers['*'];

        return array_merge($group, $all);
    }

    /**
     * Подписка на событие
     * 
     * @param \SplObserver $observer - подписчик
     * @param string $event - событие
     * 
     * @return void
     */
    public function attach(\SplObserver $observer, string $event = '*'): void
    {
        $eventLabel = $event === '*' ? 'все события' : 'событие ' . $event;
        echo '+++ Класс ' . get_class($observer) . ' подписался на ' . $eventLabel . ' <br>';

        $this->observers[$event][] = $observer;
    }

    /**
     * Отписака от события
     * 
     * @param SplObserver $observer - подписчик
     * @param string $event - событие
     * 
     * @return void
     */
    public function detach(SplObserver $observer, string $event = '*'): void
    {
        foreach ($this->getEventObservers($event) as $key => $s) {
            if ($s === $observer) {
                unset($this->observers[$event][$key]);
            }
        }
    }

    /**
     * Уведомление
     * 
     * @param string $event - событие
     * @param string $message - дополнительное сообщение
     * 
     * @return void
     */
    public function notify(string $event = '*', string $message = 'ничего'): void
    {
        foreach ($this->getEventObservers($event) as $observer) {
            $observer->update($this, $message);
        }
    }

    /**
     * @param string $select
     * 
     * @return void
     */
    public function select(string $select): void
    {
        echo 'SqlExecutor select: ' . $select . "<br>";
        $this->notify('select', 'просмотр ' . $select);
    }

    /**
     * @param string $update
     * 
     * @return void
     */
    public function update(string $update): void
    {
        echo 'SqlExecutor select: ' . $update . "<br>";
        $this->notify('update', 'обновление ' . $update);
    }

    /**
     * @param string $delete
     * 
     * @return void
     */
    public function delete(string $delete): void
    {
        echo 'SqlExecutor delete: ' . $delete . "<br>";
        $this->notify('delete', 'удаление ' . $delete);
    }

    /**
     * @param string $insert
     * 
     * @return void
     */
    public function insert(string $insert): void
    {
        echo 'SqlExecutor insert: ' . $insert . "<br>";
        $this->notify('insert', 'добавление ' . $insert);
    }
}