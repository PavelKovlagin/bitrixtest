<?php

namespace App\Patterns\Models;

use SplSubject;

class SqlViewer implements \SplObserver
{
    /**
     * @param SplSubject $subject - подписчик
     * @param string|null $message - дополнительное событие
     * 
     * @return void
     */
    public function update(SplSubject $subject, string $message = null): void
    {
        echo '*** Я слежу за тем как ты выполняешь ' . $message . '<br>';
    }
}