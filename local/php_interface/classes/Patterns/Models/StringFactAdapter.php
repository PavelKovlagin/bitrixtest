<?php

namespace App\Patterns\Models;

class StringFactAdapter
{
    private ?DogFacts $dogFacts = null; 

    public function __construct(DogFacts $dogFacts)
    {
        $this->dogFacts = $dogFacts;   
    }

    /**
     * Возвращает строку
     * 
     * @return string|null
     */
    public function get(): ?string
    {
        return (string)$this->dogFacts->get()['fact'];
    }
}