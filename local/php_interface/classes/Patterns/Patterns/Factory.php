<?php

namespace App\Patterns\Patterns;

class Factory implements IFactory
{
    private ?int $id = null;
    private ?string $name = null;
    private ?string $description = null;

    /**
     * Устанавливает идентификатор
     * 
     * @param int $id
     * 
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Возвращает идентификатор
     * 
     * @return int|null
     */
    public function getId(): ?int
    {
        return (int)$this->id;
    }

    /**
     * Устанавливает название
     * 
     * @param string $name
     * 
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Возвращает название
     * 
     * @return string|null
     */
    public function getName(): ?string
    {
        return (string)$this->name;
    }

    /**
     * Устанавливает описание
     * 
     * @param string $description
     * 
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Возвращает описание
     * 
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return (string)$this->description;
    }

    /**
     * Возвращает идентификато, название и описание при попытке преобразовать класс в строку
     * 
     * @return string
     */
    public function __toString()
    {
        return 'ID: ' . $this->getId() . ', NAME: ' . $this->getName() . ', DESCRIPTION: ' . $this->getDescription();
    }
}