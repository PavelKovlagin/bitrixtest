<?php

namespace App\Patterns\Patterns;

interface IFactory
{
    public function setId(int $id): self;
    public function getId(): ?int;
    public function setName(string $name): self;
    public function getName(): ?string;
    public function setDescription(string $description): self;
    public function getDescription(): ?string;
}