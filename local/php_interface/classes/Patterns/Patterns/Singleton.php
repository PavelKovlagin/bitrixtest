<?php

namespace App\Patterns\Patterns;

use Exception;

class Singleton
{
    private static $instance = [];

    protected function __construct()
    {

    }

    protected function __clone()
    {

    }

    protected function __wakeup()
    {
        throw new Exception('Нельзя восстанавливать объект');
    }

    public static function getInstance(): Singleton
    {
        $cls = static::class;
        if (!isset(self::$instance[$cls])) {
            self::$instance[$cls] = new static();
        }

        return self::$instance[$cls];
    }
}