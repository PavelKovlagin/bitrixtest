<?php

namespace App\UserTypes;

class CUserTypeColor
{
    const CODE = 'NewField';

    public function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => "S", #-----один из стандартных типов
            "USER_TYPE" => "MYIDCODE", #-----идентификатор типа свойства
            "DESCRIPTION" => "Название свойства",
            "GetPropertyFieldHtml" => array("App\UserTypes\CUserTypeColor", "GetPropertyFieldHtml"),
            "ConvertToDB" => array("App\UserTypes\CUserTypeColor", "ConvertToDB")
        );
    }

    /*--------- вывод поля свойства на странице редактирования ---------*/
    public function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $uns = unserialize($value['DESCRIPTION']);
        echo "<script>console.log('" . __FILE__ . "'); console.log(" . json_encode($uns) .  ")</script>";
        return '
            <p>Значение: <input type="text" name="' . CODE . '_value" value="' . $value['VALUE'] . '"></p>
            <p>One: <input type="text" name="' . CODE . '_one" value="' . $uns['one'] . '"></p>
            <p>Two: <input type="text" name="' . CODE . '_two" value="' . $uns['two'] . '"></p>
            <p>Three: <input type="text" name="' . CODE . '_three" value="' . $uns['three'] . '"></p>
            <p>Four: <input type="text" name="' . CODE . '_four" value="' . $uns['four'] . '"></p>
            <p>Five: <input type="text" name="' . CODE . '_five" value="' . $uns['five'] . '"></p>
            <p>Six: <input type="text" name="' . CODE . '_six" value="' . $uns['six'] . '"></p>
        ';
    }

    function ConvertToDB($arProperty, $value)
    {
        $value['VALUE'] = $_REQUEST[CODE . '_value'];
        $value['DESCRIPTION'] = serialize(
            [
                'one' => $_REQUEST[CODE . '_one'],
                'two' => $_REQUEST[CODE . '_two'],
                'three' => $_REQUEST[CODE . '_three'],
                'four' => $_REQUEST[CODE . '_four'],
                'five' => $_REQUEST[CODE . '_five'],
                'six' => $_REQUEST[CODE . '_six'],
            ]
        );
        // $value['DESCRIPTION'] = 'asdasd';
        return $value;
    }
}
