<?php

use Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();

//Вешаем обработчик на событие создания списка пользовательских свойств OnUserTypeBuildList
$eventManager->addEventHandler('iblock', 'OnIBlockPropertyBuildList', ['App\UserTypes\CUserTypeColor', 'GetUserTypeDescription']);
$eventManager->addEventHandler('iblock', 'OnIBlockPropertyBuildList', ['App\UserTypes\ElementWithDescription', 'GetIBlockPropertyDescription']);