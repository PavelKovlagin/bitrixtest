<?php
use \Bitrix\Main\Loader;

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/vendor/autoload.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/vendor/autoload.php';
}

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/config/events.php')) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/config/events.php';
}

AddEventHandler("iblock", "OnAfterIBlockElementSetPropertyValuesEx", Array("MyClass", "OnAfterIBlockElementSetPropertyValuesExHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("MyClass", "OnBeforeIBlockElementUpdateHandler"));

class MyClass
{
    // создаем обработчик события "OnAfterIBlockElementUpdate"
    function OnAfterIBlockElementSetPropertyValuesExHandler($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUES, $FLAGS)
    {
        // \Bitrix\Main\Diag\Debug::writeToFile(
        //     [
        //         'ELEMENT_ID' => $ELEMENT_ID,
        //         'IBLOCK_ID',

        //     ],
        //     date('d.m.Y H:i:s') . ' | ' .  __FILE__,
        //    'log_blockprop.txt'
        // );
    }

    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        // if ((int)$arFields['IBLOCK_ID'] === 11) {
        //     $arFields['PROPERTY_VALUES'][59][311]['VALUE'] = '01.01.1999';
        //     \Bitrix\Main\Diag\Debug::writeToFile(
        //         [
        //             'fields' => $arFields
        //         ],
        //         date('d.m.Y H:i:s') . ' | ' .  __FILE__,
        //        'log_blockprop.txt'
        //     );
        // }
        
    }

    function OnBeforeIBlockPropertyUpdateHandler(&$arFields)
    {
        \Bitrix\Main\Diag\Debug::writeToFile(
            [
                'fields' => $arFields
            ],
            date('d.m.Y H:i:s') . ' | ' .  __FILE__,
            'log_blockprop.txt'
        );
    }
}


Loader::includeModule('hb.repo');