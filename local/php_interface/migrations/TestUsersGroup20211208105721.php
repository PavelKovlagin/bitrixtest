<?php

namespace Sprint\Migration;

use Repo\Helper\Csv;
use Repo\Helper\UserGroups;

class TestUsersGroup20211208105721 extends Version
{
    protected $description = "Тестовые пользователи";

    protected $moduleVersion = "3.14.3";

    const TESTUSERS_GROUP_CODE = 'testusers';
    const TESTUSERS_GROUP_NAME = 'Тестовые пользователи';
    const TESTUSERS_GROUP_DESCRIPTION = 'Тестовые пользователи';
    const TESTUSERS_GROUP_CSV = '/uploads/users.csv';

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $helper->UserGroup()->saveGroup(
            self::TESTUSERS_GROUP_CODE,
            [
                'ACTIVE' => 'Y',
                'C_SORT' => '100',
                'ANONYMOUS' => 'N',
                'NAME' => self::TESTUSERS_GROUP_NAME,
                'DESCRIPTION' => self::TESTUSERS_GROUP_DESCRIPTION,
                'SECURITY_POLICY' => [],
            ]
        );
        $groupId = UserGroups::getGroupIdByCode(self::TESTUSERS_GROUP_CODE);
        if ($array = Csv::getArrayFromCsv(self::TESTUSERS_GROUP_CSV)) {
            $user = new \CUser;
            foreach ($array as $item) {
                $ID = $user->Add([
                    'LOGIN' => trim($item['LOGIN']),
                    'NAME' => trim($item['NAME']),
                    'LAST_NAME' => trim($item['LAST_NAME']),
                    'EMAIL' => trim($item['EMAIL']),
                    'PASSWORD' => trim($item['PASSWORD']),
                    "GROUP_ID" => [$groupId],
                ]);
            }
        }
    }

    public function down()
    {
        $group = new \CGroup;
        if ($groupId = UserGroups::getGroupIdByCode(self::TESTUSERS_GROUP_CODE)) {
            $rsUsers = \CUser::GetList(
                ($by = "personal_country"),
                ($order = "desc"),
                [
                    'GROUPS_ID' => [
                        $groupId
                    ]
                ]
            );
            while ($user = $rsUsers->Fetch()) {
                \CUser::Delete($user['ID']);
            }
            $group->Delete($groupId);
        }
    }
}
