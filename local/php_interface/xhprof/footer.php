<?php
// stop profiler
if ($_GET['xhprof']) {
    $xhprof_data = xhprof_disable();

    $XHPROF_ROOT = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/xhprof';
    include($XHPROF_ROOT . "/utils/xhprof_lib.php");
    include($XHPROF_ROOT . "/utils/xhprof_runs.php");

    $namespace = 'test_bitrix';
    $xhprof_runs = new XHProfRuns_Default();
    $run_id = $xhprof_runs->save_run($xhprof_data, $namespace);
    header("XHprofLink: http://xhprof.localhost/index.php?run=" . $run_id . "&source=" . $namespace);
}
