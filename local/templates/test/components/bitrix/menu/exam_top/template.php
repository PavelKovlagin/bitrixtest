<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"])) {
	return;
}
?>

<nav class="nav">
	<div class="inner-wrap">
		<div class="menu-block popup-wrap">
			<a href="" class="btn-menu btn-toggle"></a>
			<div class="menu popup-block">
				<ul class="">
					<li class="main-page"><a href="/"><?= Loc::getMessage('MAIN_TEXT') ?></a></li>
					<?php foreach ($arResult['ALL_ITEMS_ID'] as $keyLvl1 => $itemLvl1) { ?>
						<?php if ($arResult['ALL_ITEMS'][$keyLvl1]['PERMISSION'] === 'D') continue; ?>
						<li>
							<a href="<?= $arResult['ALL_ITEMS'][$keyLvl1]['LINK'] ?>" class='color-<?= $arResult['ALL_ITEMS'][$keyLvl1]['SELECTED'] ? 'black' :  $arResult['ALL_ITEMS'][$keyLvl1]['PARAMS']['color'] ?>'><?= $arResult['ALL_ITEMS'][$keyLvl1]['TEXT'] ?></a>
							<ul>
								<?php if (($text = $arResult['ALL_ITEMS'][$keyLvl1]['PARAMS']['text']) && $arResult['ALL_ITEMS'][$keyLvl1]['IS_PARENT']) { ?>
									<div class="menu-text"><?= $text ?></div>
								<?php } ?>
								<?php foreach ($itemLvl1 as $keyLvl2 => $itemLvl2) { ?>
									<?php if ($arResult['ALL_ITEMS'][$keyLvl2]['PERMISSION'] === 'D') continue; ?>
									<li>
										<a href="<?= $arResult['ALL_ITEMS'][$keyLvl2]['LINK'] ?>"><?= $arResult['ALL_ITEMS'][$keyLvl2]['TEXT'] ?></a>
										<ul>
											<?php foreach ($itemLvl2 as $keyLvl3 => $itemLvl3) { ?>	
												<?php if ($arResult['ALL_ITEMS'][$itemLvl3]['PERMISSION'] === 'D') continue; ?>											
												<li><a href="<?= $arResult['ALL_ITEMS'][$itemLvl3]['LINK'] ?>"><?= $arResult['ALL_ITEMS'][$itemLvl3]['TEXT'] ?></a></li>
											<?php } ?>
										</ul>
									</li>
								<?php } ?>
							</ul>
						</li>
					<?php } ?>
					<a href="" class="btn-close"></a>
			</div>
			<div class="menu-overlay"></div>
		</div>
	</div>
</nav>