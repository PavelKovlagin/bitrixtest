<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$count = 0;
?>
<div class="item-wrap">
	<div class="rew-footer-carousel">
		<?php foreach ($arResult['ITEMS'] as $item) { ?>
			<?php if ($count >= $arParams['NEWS_COUNT']) continue?>
			<div class="item">
				<div class="side-block side-opin">
					<div class="inner-block">
						<div class="title">
							<div class="photo-block">
								<img src="<?= $item['PREVIEW_PICTURE'] ? $item['PREVIEW_PICTURE_RESIZED']['src'] : '/img/no_photo_left_block.jpg' ?>" alt="">
							</div>
							<div class="name-block"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a></div>
							<div class="pos-block"><?= $item['PROPERTIES']['COMPANY']['VALUE'] ?></div>
						</div>
						<div class="text-block"><?= $item['PREVIEW_TEXT'] ?></div>
					</div>
				</div>
			</div>
			<?php $count++ ?>
		<?php } ?>
	</div>
</div>