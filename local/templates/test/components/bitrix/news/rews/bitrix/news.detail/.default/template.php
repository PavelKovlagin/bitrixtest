<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;
$APPLICATION->SetPageProperty("h1", Loc::getMessage('REW_LABEL') . ' - ' . $arResult['NAME'] . ' - ' . $arResult['PROPERTIES']['COMPANY']['VALUE']);
$APPLICATION->SetPageProperty("title", Loc::getMessage('REW_LABEL') . ' - ' . $arResult['NAME']);
$this->setFrameMode(true);
?>
<div class="review-block">
	<div class="review-text">
		<div class="review-text-cont">
			<?= $arResult['DETAIL_TEXT'] ?>
		</div>
		<div class="review-autor">
			<?= $arResult['NAME'] ?>,
			<?= $arResult['ACTIVE_FROM'] ? DateTime::createFromFormat('d.m.Y H:i:s', $arResult['ACTIVE_FROM'])->format($arParams['ACTIVE_DATE_FORMAT']) : '' ?>,
			<?= $arResult['PROPERTIES']['POSITION']['VALUE'] ?>,
			<?= $arResult['PROPERTIES']['COMPANY']['VALUE'] ?>
		</div>
	</div>
	<div style="clear: both;" class="review-img-wrap"><img src="<?= $arResult['DETAIL_PICTURE'] ? $arResult['DETAIL_PICTURE']['SRC'] : '/img/rew/no_photo.jpg' ?>" alt="img"></div>
</div>
<div class="exam-review-doc">
	<?php if ($arResult['PROPERTIES']['DOCUMENTS']['VALUE']) { ?>
		<p>Документы:</p>
		<?php foreach ($arResult['PROPERTIES']['DOCUMENTS']['VALUE'] as $docId) { ?>
			<?php $document = CFile::GetByID($docId)->Fetch() ?>
			<div class="exam-review-item-doc">
				<img class="rew-doc-ico" src="/img/icons/pdf_ico_40.png">
				<a href="<?= CFile::getPath($docId) ?>"><?= $document['ORIGINAL_NAME'] ?></a>
			</div>
		<?php } ?>
	<?php } ?>
</div>