<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
foreach ($arResult['ITEMS'] as $item) { ?>
	<div class="review-block">
		<div class="review-text">
			<div class="review-block-title">
				<span class="review-block-name">
					<a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a>
				</span>
				<span class="review-block-description">
					<?= $item['ACTIVE_FROM'] ? DateTime::createFromFormat('d.m.Y H:i:s', $item['ACTIVE_FROM'])->format($arParams['ACTIVE_DATE_FORMAT']) : '' ?>, 
					<?= $item['PROPERTIES']['POSITION']['VALUE'] ?>,
					<?= $item['PROPERTIES']['COMPANY']['VALUE'] ?>
				</span>
			</div>
			<div class="review-text-cont">
				<?= $item['PREVIEW_TEXT'] ?>
			</div>
		</div>
		<div class="review-img-wrap">
			<a href=" <?= $item['DETAIL_PAGE_URL'] ?>"><img src="<?= $item['PREVIEW_PICTURE'] ? $item['PREVIEW_PICTURE']['SRC'] : '/img/rew/no_photo.jpg' ?>" alt="img"></a>
		</div>
	</div>
<? } ?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>