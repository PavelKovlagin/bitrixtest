<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
require($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/xhprof/header.php');
IncludeTemplateLangFile(__FILE__);

use \Bitrix\Main\Page\Asset;
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <?php
    $APPLICATION->ShowHead();
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/reset.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/style.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/owl.carousel.css');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/owl.carousel.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/scripts.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/vanillascript.js');
    Asset::getInstance()->addString('<link rel="shortcut icon" href="' . SITE_TEMPLATE_PATH . '/img/favicon.ico">');
    Asset::getInstance()->addString('<link rel="icon" type="image/vnd.microsoft.icon"  href="' . SITE_TEMPLATE_PATH . '/img/favicon.ico">');
    ?>
    <title><?= $APPLICATION->ShowTitle() ?></title>
</head>

<body>
    <? $APPLICATION->ShowPanel() ?>
    <!-- wrap -->
    <div class="wrap">
        <!-- header -->
        <header class="header">
            <div class="inner-wrap">
                <div class="logo-block"><a href="" class="logo">Мебельный магазин</a>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "/include/main.php",
                        "AREA_FILE_RECURSIVE" => "Y",
                        "EDIT_TEMPLATE" => "standard.php",
                        "COMPONENT_TEMPLATE" => ".default",
                        "PATH" => "/include/main.php"
                    ),
                    false
                ); ?>
                <div class="actions-block">
                    <form action="/" class="main-frm-search">
                        <input type="text" placeholder="Поиск">
                        <button type="submit"></button>
                    </form>
                    <nav class="menu-block">
                        <ul>
                            <?php
                            $APPLICATION->IncludeComponent(
	"bitrix:system.auth.form", 
	"demo", 
	array(
		"REGISTER_URL" => "register.php",
		"FORGOT_PASSWORD_URL" => "",
		"PROFILE_URL" => "profile.php",
		"SHOW_ERRORS" => "Y",
		"COMPONENT_TEMPLATE" => "demo"
	),
	false
);
                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <!-- /header -->
        <!-- nav -->
        <nav class="nav">
            <div class="inner-wrap">
                <div class="menu-block popup-wrap">
                    <a href="" class="btn-menu btn-toggle"></a>
                    <? $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"exam_top", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MAX_LEVEL" => "3",
		"USE_EXT" => "N",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "exam_top",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_THEME" => "site"
	),
	false
); ?>
                    <div class="menu-overlay"></div>
                </div>
            </div>
        </nav>
        <!-- /nav -->
        <? $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "exam",
            array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1",
                "COMPONENT_TEMPLATE" => "exam"
            ),
            false
        ); ?>
        <!-- page -->
        <div class="page">
            <!-- content box -->
            <div class="content-box">
                <div class="content">
                    <div class="cnt">
                        <?php if ($APPLICATION->GetCurDir() !== '/') { ?>
                            <header>
                                <h1><?= $APPLICATION->ShowProperty('h1') ?></h1>
                            </header>
                        <?php } ?>