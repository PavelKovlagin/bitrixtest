async function pFunc() {
    var promise = new Promise((resolve, reject) => {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/local/ajax/reviews.php', false);
        xhr.send();
        if (xhr.status != 200) {
            reject(xhr.status + ': ' + xhr.statusText);
        } else {
            resolve(xhr.responseText);
        }
    });
    return promise;
}

async function asyncAlert() {
    pFunc()
    .then(
        result => {
            console.log(result)
        },
        error => {
            //alert("Rejected: " + error.message)
            console.log("Rejected: " + error)
        }
    );
}

asyncAlert().then();