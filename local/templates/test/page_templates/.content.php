<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
IncludeTemplateLangFile(__FILE__);
$TEMPLATE["standard.php"] = [
    "name" => GetMessage("standart"), 
    "sort" => 1
];
$TEMPLATE["nonstandard.php"] = [
    "name" => GetMessage("nonstandart"), 
    "sort" => 2
];
?>