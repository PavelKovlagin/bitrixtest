<?

use App\Models\Manager\Entity\ManagerRepository;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Руководители");
$managers = (new ManagerRepository)->getAll();
?>

<?php foreach ($managers as $manager) { ?>
    <h1><?= $manager->getName() ?></h1>
    <?php if ($user = $manager->getUser()) { ?>
        <p><?= $user->getLogin() ?></p>
        <p><?= $user->getEmail() ?></p>
        <p><?= $user->getFullName() ?></p>
    <?php } ?>
<?php } ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>