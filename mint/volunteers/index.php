<?php

use App\Models\Volunteer\Entity\VolunteerRepository;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Волонтеры");
$volunteers = (new VolunteerRepository())->getAll();
?>
<?php foreach ($volunteers as $volunteer) { ?>
    <h1><?= $volunteer->getName() ?></h1>
    <?php if ($user = $volunteer->getUser()) { ?>
        <p>Login: <?= $user->getLogin() ?></p>
        <p>Email: <?= $user->getEmail() ?></p>
        <p>Fullname: <?= $user->getFullName() ?></p>
    <?php } ?>
<?php } ?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>