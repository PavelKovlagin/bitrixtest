<?php

use App\Helpers\UserGroups;
use App\Helpers\Users;

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
ini_set('session.save_path', "/tmp/ses/");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . '/../');

require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";

if ($argv[1] === 'delete') {
    $groupCode = 'testusers';
    if ($groupId = UserGroups::getGroupIdByCode('testusers')) {
        $users = Users::getUsersByGroup($groupId);
        foreach ($users as $user) {            
            Users::removeFavoritesForUser($user['ID']);
            echo 'Delete favorites products for user ' . $user['ID'] . "\n";
        }
    } else {
        echo 'Group ' . $groupCode . ' not found'. "\n\n";
    }
} else {
    echo "Required parameter is missed! \n\n";
}
