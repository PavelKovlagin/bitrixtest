<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);
ini_set('session.save_path', "/tmp/ses/");

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . '/../');

require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";

if ($argv[1] === 'delete') {
    if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
        global $DB;
        $nDays = 1; // сроком старше одного дня
        $nDays = IntVal($nDays);
        $strSql =
            "SELECT f.ID, f.DATE_UPDATE " .
            "FROM b_sale_fuser f " .
            "LEFT JOIN b_sale_order o ON (o.USER_ID = f.USER_ID) " .
            "WHERE " .
            "   TO_DAYS(f.DATE_UPDATE)<(TO_DAYS(NOW())-" . $nDays . ") " .
            "   AND o.ID is null " .
            "   AND f.USER_ID is null " .
            "LIMIT 3000";
        $dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        $iter = 0;
        while ($res = $dbRes->Fetch()) { 
            $iter++;
            // CSaleBasket::DeleteAll($ar_res["ID"], false);
            // CSaleUser::Delete($ar_res["ID"]);
            echo $res['ID'] . ' - ' . $res['DATE_UPDATE'] . ' - delete' . "\n";
        }
    }
} else {
    echo "Required parameter is missed! \n\n";
}
