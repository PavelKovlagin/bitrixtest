<?php

use App\Helpers\WebForms;

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . '/../');
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/csv_data.php");

if ($argv[1] == "delete") {
	$code = 'SIMPLE_FORM_2';
	$form = WebForms::getByCode($code);
	if ($form = WebForms::getByCode($code)) {
		$results = WebForms::getResultsByReviewValue(
			$form['ID'],
			[
				[
					'CODE' => 'REVIEW',
					'VALUE' => 'DELETE',
					'FILTER_TYPE' => 'text',
					'EXACT_MATCH' => 'N'
				]
			]
		);
		foreach ($results as $result) {
			if (CFormResult::Delete($result['ID'], 'N')) {
				echo 'delete result ' . $result['ID'] . " success \n";
			} else {
				echo 'delete result ' . $result['ID'] . " failed \n";
			}
		}
	} else {
		echo 'WebForm ' . $code . ' not found' . "\n\n";
	}	
} else {
	echo "Required parameter is missed! \n\n";
}
